package com.mythosstudios.CrateBreaker;

/**
 * ...
 * @author A.A. Grapsas
 */

import android.os.Bundle;
import android.view.WindowManager;
 
public class MainActivity  extends org.haxe.nme.GameActivity
{
	protected void onCreate(Bundle state)
	{
		super.onCreate(state);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		float brightness = 100.0f;
		
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.screenBrightness = brightness / 255.0f;
		
		getWindow().setAttributes(lp);
	}
}