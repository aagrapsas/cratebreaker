package game.gems;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.EventBus;
import engine.misc.ITickable;
import engine.misc.ITouchable;
import engine.render.particles.InWorldFXPlayer;
import engine.render.ResolutionConfig;
import engine.scene.Actor;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.fx.GemExplosionFX;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class Gem extends Actor, implements ITickable
{
	public static inline var STARTING_VELOCITY:Float = 100;
	public static inline var VELOCITY_STEP:Float = 800;
	public static inline var VELOCITY_DAMPENING:Float = 65;
	public static inline var STATIONARY_TIME:Float = 0.3;
	public static inline var KABOOM_DIST_SQRD:Float = 40000;	// let's assume this is world untis
	
	public var isAlive:Bool = true;
	
	public var velocityX:Float = 0;
	public var velocityY:Float = 0;
	
	public var stationaryAccumulator:Float = 0;
	public var lifeAccumulator:Float = 0;
	
	public var explosionAsset:String = "redparticle";
	
	public function new() 
	{
		super();
		
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.GAME_END, onGameEnd );
	}
	
	private function onGameEnd( data:Dynamic ):Void
	{
		kaboom();
	}
	
	private function kaboom():Void
	{
		InWorldFXPlayer.PTR.playFX( new GemExplosionFX( explosionAsset ), this.x, this.y );
				
		// Mark as collected
		if ( explosionAsset == "purpleparticle" )
		{
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.PURPLE_GEM_OBTAINED, null );
		}
		else if ( explosionAsset == "greenparticle" )
		{
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.GREEN_GEM_OBTAINED, null );
		}
		else
		{
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.RED_GEM_OBTAINED, null );
		}
		
		destroy();
	}
	
	public function tick( deltaTime:Float ):Void
	{
		lifeAccumulator += deltaTime;
		
		if ( GameConfig.PTR.currentLevel == null )
		{
			destroy();
			return;
		}
		
		// Screen coords
		var target:Point = GameConfig.PTR.currentLevel.getUIPosition( explosionAsset );
		
		var destX:Float = ResolutionConfig.PTR.convertScreenXToWorld( target.x );
		var destY:Float = ResolutionConfig.PTR.convertScreenYToWorld( target.y );
		
		target.x -= this.x;
		target.y -= this.y;
		
		target.normalize( 1 );
		
		velocityX += target.x * VELOCITY_STEP * deltaTime;
		velocityY += target.y * VELOCITY_STEP * deltaTime;
		
		this.x += velocityX * deltaTime;
		this.y += velocityY * deltaTime;
		
		if ( ( this.x - ( this.bitmap.originalWidth / 2 ) ) <= 0 )      // Too left
		{
			this.x = 0 + ( this.bitmap.originalWidth / 2 );
			this.velocityX *= -1;
		}
		else if ( this.x >= ( ResolutionConfig.PTR.expectedScreenWidth - ( this.bitmap.originalWidth / 2 ) ) )
		{
			this.x = ResolutionConfig.PTR.expectedScreenWidth - ( this.bitmap.originalWidth / 2 );
			this.velocityX *= -1;
		}
		
		if ( this.y <= 0 )
		{
			this.y = 0 + ( this.bitmap.originalHeight / 2 );
			this.velocityY *= -1;
		}
		else if ( this.y >= ( ResolutionConfig.PTR.expectedScreenHeight - ( this.bitmap.originalHeight / 2 ) ) )
		{
			this.y = ResolutionConfig.PTR.expectedScreenHeight - ( this.bitmap.originalHeight / 2 );
			this.velocityY *= -1;
		}
		
		// Check if close enough to target to go kaboom
		var distX:Float = destX - this.x;
		var distY:Float = destY - this.y;
		var distSqr:Float = ( distX * distX ) + ( distY * distY );
		
		if ( distSqr <= KABOOM_DIST_SQRD )
		{
			kaboom();
		}
	}
	
	public function handleTap():Void { }
	
	private function destroy():Void
	{
		isAlive = false;
		
		SceneManager.PTR.removeActor( this );
		SceneManager.PTR.removeTickable( this );
		
		GameConfig.PTR.gameplayEvents.unregister( GameEventTypes.GAME_END, onGameEnd );
	}
}