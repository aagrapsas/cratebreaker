package game.purchases;
import game.config.GameConfig;

/**
 * ...
 * @author A.A. Grapsas
 */
class Purchasable
{
	public var data:PurchaseData;
	
	public function new() 
	{
		
	}
	
	public function canExecute():Bool
	{
		if ( data.isCrateSet )
		{
			var found:Bool = false;
			
			for ( i in 0...GameConfig.PTR.userData.unlockedSets.length )
			{
				var set:String = GameConfig.PTR.userData.unlockedSets[ i ];
				
				if ( set == data.setKey )
				{
					found = true;
					break;
				}
			}
			
			// Do not allow purchase of already purchased item
			if ( found )
			{
				return false;
			}
		}
		
		// Purple gems
		if ( data.purpleCost > 0 && GameConfig.PTR.userData.purpleGems < data.purpleCost )
		{
			return false;
		}
		
		// Green cost
		if ( data.greenCost > 0 && GameConfig.PTR.userData.greenGems < data.greenCost )
		{
			return false;
		}
		
		// Red cost
		if ( data.redCost > 0 && GameConfig.PTR.userData.redGems < data.redCost )
		{
			return false;
		}
		
		return true;
	}
	
	public function isMonetary():Bool
	{
		return data.cashCost != 0;
	}
	
	public function execute():Void
	{
		if ( data.isCrateSet )
		{
			// Give set
			GameConfig.PTR.userData.unlockedSets.push( data.setKey );
		}
		else if ( data.resourceType != null )
		{
			// Give any resources
			if ( data.purpleToGive > 0 )
			{
				GameConfig.PTR.userData.purpleGems += data.purpleToGive;
			}
			
			if ( data.greenToGive > 0 )
			{
				GameConfig.PTR.userData.greenGems += data.greenToGive;
			}
			
			if ( data.redToGive > 0 )
			{
				GameConfig.PTR.userData.redGems += data.redToGive;
			}
		}
		
		// Subtract appropriate resources
		// Purple gems
		if ( data.purpleCost > 0 )
		{
			GameConfig.PTR.userData.purpleGems -= data.purpleCost;
		}
		
		// Green cost
		if ( data.greenCost > 0 )
		{
			GameConfig.PTR.userData.greenGems -= data.greenCost;
		}
		
		// Red cost
		if ( data.redCost > 0 )
		{
			GameConfig.PTR.userData.redGems -= data.redCost;
		}
		
		GameConfig.PTR.userData.save();
	}
}