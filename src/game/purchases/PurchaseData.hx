package game.purchases;

/**
 * ...
 * @author A.A. Grapsas
 */
class PurchaseData
{
	public var purpleCost:Int = 0;
	public var greenCost:Int = 0;
	public var redCost:Int = 0;
	
	public var cashCost:Float = 0;
	
	public var resourceType:String = null;
	public var resourceCount:Int = 0;
	
	public var isCrateSet:Bool = false;
	public var setKey:String = null;
	
	public var purpleToGive:Int = 0;
	public var greenToGive:Int = 0;
	public var redToGive:Int = 0;
	
	public function new() 
	{
		
	}
	
	public function setupCrateSet( key:String ):Void
	{
		isCrateSet = true;
		setKey = key;
	}
	
	public function setupResource( purple:Int, green:Int, red:Int ):Void
	{
		purpleToGive = purple;
		greenToGive = green;
		redToGive = red;
	}
	
	public function setupSoftCost( purple:Int, green:Int, red:Int ):Void
	{
		purpleCost = purple;
		greenCost = green;
		redCost = red;
	}
	
	public function setupHardCost( cost:Float ):Void
	{
		cashCost = cost;
	}
}