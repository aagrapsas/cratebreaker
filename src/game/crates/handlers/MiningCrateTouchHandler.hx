package game.crates.handlers;
import engine.collision.SceneNode;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.crates.Crate;
import game.crates.CrateFactory;

/**
 * ...
 * @author A.A. Grapsas
 */

class MiningCrateTouchHandler implements ICrateTouchHandler
{
	public function new() 
	{
		
	}
	
	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		if ( chain.length < 3 )
		{
			return true;
		}
		
		var visited:IntHash<Crate> = new IntHash<Crate>();
		var randomCrateOptions:Array<String> = new Array<String>();
		
		// Random options
		randomCrateOptions.push( CrateFactory.RED_CRATE );
		randomCrateOptions.push( CrateFactory.PURPLE_CRATE );
		randomCrateOptions.push( CrateFactory.GREEN_CRATE );
		randomCrateOptions.push( CrateFactory.GEM_CRATE );
		
		for ( visitedCrate in chain )
		{
			visited.set( visitedCrate.uniqueID, visitedCrate );
		}
		
		visited.set( crate.uniqueID, crate );
		
		var centerX:Float = crate.x;
		var centerY:Float = crate.y;
		var worldWidthToSearch:Float = crate.bitmap.originalWidth * 2;
		var worldHeightToSearch:Float = crate.bitmap.originalHeight * 2;
		var allNeighbors:Array<SceneNode> = SceneManager.PTR.getCollidersWorldSpace( centerX, centerY, worldWidthToSearch, worldHeightToSearch );
		
		// Get all neighbors and make them a random gem crate!
		for ( neighbor in allNeighbors )
		{
			if ( Std.is( neighbor.internalObject, Crate ) == false )
			{
				continue;
			}
			
			var neighborCrate:Crate = cast neighbor.internalObject;
			
			if ( visited.exists( neighborCrate.uniqueID ) )
			{
				continue;
			}
			
			if ( neighborCrate.isAlive == false )
			{
				continue;
			}
			
			var randomIndex:Int = cast Math.floor( Math.random() * randomCrateOptions.length );
			var crateOption:String = randomCrateOptions[ randomIndex ];
			
			CrateFactory.switchCrate( neighborCrate, crateOption );
		}
		
		// Set this to random, too!
		var myRandomIndex:Int = cast Math.floor( Math.random() * randomCrateOptions.length );
		var myCrateOption:String = randomCrateOptions[ myRandomIndex ];
			
		CrateFactory.switchCrate( crate, myCrateOption );
		
		return false;
	}
	
	public function getType():String { return CrateFactory.MINING_CRATE; }
	
	public function canChain():Bool { return true; }
}