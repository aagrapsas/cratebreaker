package game.crates.handlers;
import game.ActorFactory;
import game.crates.Crate;
import game.crates.CrateFactory;
import game.gems.Gem;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class GemCrateTouchHandler implements ICrateTouchHandler
{
	public function new() 
	{
		
	}
	
	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		if ( chain.length < 3 )
		{
			return true;	// Do nothing if the chain isn't at least 3
		}
		
		var gems:Int = 1 + Math.floor( Math.random() * 5 );
		gems *= chain.length;	// increase based on chain length
		
		var point:Point = new Point();
		
		for ( i in 0...gems )
		{
			var gem:Gem = ActorFactory.spawnRandomGem( crate.x, crate.y );
			point.x = Math.random() * 1000 * ( Math.random() > 0.5 ? 1 : -1 );
			point.y = Math.random() * 1000 * ( Math.random() > 0.5 ? 1 : -1 );
			
			point.normalize( 1 );
			
			gem.velocityX = point.x * Gem.STARTING_VELOCITY;
			gem.velocityY = point.y * Gem.STARTING_VELOCITY;
		}
		
		return true;
	}
	
	public function getType():String { return CrateFactory.GEM_CRATE; }
	
	public function canChain():Bool { return true; }
}