package game.crates.handlers;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import game.crates.Crate;
import game.crates.CrateFactory;

/**
 * ...
 * @author A.A. Grapsas
 */

class RadioActiveCrateTouchHandler implements ICrateTouchHandler
{
	public function new()
	{
		
	}
	
	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		if ( chain.length < 3 )
		{
			return true;
		}
		
		CrateFactory.switchCrate( crate, CrateFactory.GEM_CRATE );
		
		return false;	// Doesn't consume if 3, since it morphs
	}
	
	public function getType():String { return CrateFactory.RADIO_ACTIVE_CRATE; }
	
	public function canChain():Bool { return true; }
}