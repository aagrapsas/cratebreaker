package game.crates.handlers;
import game.config.GameConfig;
import game.crates.Crate;
import game.crates.CrateFactory;

/**
 * ...
 * @author A.A. Grapsas
 */

class DeathCrateTouchHandler implements ICrateTouchHandler
{
	public function new() 
	{
		
	}
	
	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		if ( chain.length < 3 )
		{
			// DEATH!
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.GAME_END, "deathcrate" );
			
			// We should probably show some helpful UI when you die...
			
			return true;
		}
		
		// If one and you touch it, instant death
		
		// if 3 or more and you touch it... 
		//			... something else
		
		return true;
	}
	
	public function getType():String { return CrateFactory.DEATH_CRATE; }
	
	public function canChain():Bool { return true; }
}