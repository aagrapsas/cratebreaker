package game.crates.handlers;
import game.config.GameConfig;
import game.crates.Crate;
import game.crates.CrateFactory;
import game.crates.logic.TimeCrateLogic;

/**
 * ...
 * @author A.A. Grapsas
 */

class TimeCrateTouchHandler implements ICrateTouchHandler
{
	public function new() 
	{
		
	}
	
	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		if ( chain.length < 3 )
		{
			return true;
		}
		
		if ( GameConfig.PTR.slomoManager.getIsInSlomo() )
		{
			return true;
		}
		
		var duration:Float = 10 * chain.length;
		
		var logic:TimeCrateLogic = cast crate.logic;
		
		logic.beginCountdown( duration );
		
		GameConfig.PTR.slomoManager.slomo( duration, 0.05 );
		
		return false;
	}
	
	public function getType():String { return CrateFactory.TIME_CRATE; }
	public function canChain():Bool { return true; }
}