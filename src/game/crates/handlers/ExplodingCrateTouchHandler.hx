package game.crates.handlers;
import engine.collision.SceneNode;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.ActorFactory;
import game.crates.Crate;
import game.crates.CrateFactory;
import game.gems.Gem;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class ExplodingCrateTouchHandler implements ICrateTouchHandler
{
	public function new() 
	{
		
	}

	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		var visited:IntHash<Crate> = new IntHash<Crate>();
		// Get all nearby crates and go KABOOM --> DESTROY
		
		for ( visitedCrate in chain )
		{
			visited.set( visitedCrate.uniqueID, visitedCrate );
		}
		
		visited.set( crate.uniqueID, crate );
		
		var centerX:Float = crate.x;
		var centerY:Float = crate.y;
		var worldWidthToSearch:Float = crate.bitmap.originalWidth * 2;
		var worldHeightToSearch:Float = crate.bitmap.originalHeight * 2;
		var allNeighbors:Array<SceneNode> = SceneManager.PTR.getCollidersWorldSpace( centerX, centerY, worldWidthToSearch, worldHeightToSearch );
		
		for ( neighbor in allNeighbors )
		{
			if ( Std.is( neighbor.internalObject, Crate ) == false )
			{
				continue;
			}
			
			var neighborCrate:Crate = cast neighbor.internalObject;
			
			if ( visited.exists( neighborCrate.uniqueID ) )
			{
				continue;
			}
			
			if ( neighborCrate.isAlive == false )
			{
				continue;
			}
			
			// Destroy
			neighborCrate.externallyDestroy();
		}
		
		return true;
	}
	
	public function getType():String { return CrateFactory.EXPLODE_CRATE; }
	
	public function canChain():Bool { return true; }
}