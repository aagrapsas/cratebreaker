package game.crates.handlers;
import game.ActorFactory;
import game.crates.Crate;
import game.gems.Gem;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class GreenCrateTouchHandler implements ICrateTouchHandler
{

	public function new() 
	{
		
	}
	
	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		var gems:Int = Std.int( Math.max( chain.length, 1 ) );
		
		var point:Point = new Point();
		
		for ( i in 0...gems )
		{
			var gem:Gem = ActorFactory.spawnGem( "greengem", crate.x, crate.y );
			
			point.x = Math.random() * 1000 * ( Math.random() > 0.5 ? 1 : -1 );
			point.y = Math.random() * 1000 * ( Math.random() > 0.5 ? 1 : -1 );
			
			point.normalize( 1 );
			
			gem.velocityX = point.x * Gem.STARTING_VELOCITY;
			gem.velocityY = point.y * Gem.STARTING_VELOCITY;
		}
		
		return true;
	}
	
	public function getType():String { return CrateFactory.GREEN_CRATE; }
	
	public function canChain():Bool { return true; }
}