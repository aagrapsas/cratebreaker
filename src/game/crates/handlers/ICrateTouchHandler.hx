package game.crates.handlers;
import game.crates.Crate;

/**
 * ...
 * @author A.A. Grapsas
 */

interface ICrateTouchHandler 
{
	function handleTouch( crate:Crate, chain:Array<Crate> ):Bool;
	function getType():String;
	function canChain():Bool;
}