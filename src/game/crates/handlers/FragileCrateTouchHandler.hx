package game.crates.handlers;
import game.crates.Crate;
import game.crates.CrateFactory;

/**
 * ...
 * @author A.A. Grapsas
 */

class FragileCrateTouchHandler implements ICrateTouchHandler
{
	public function new() 
	{
		
	}
	
	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		return true;
	}
	
	public function getType():String { return CrateFactory.FRAGILE_CRATE; }
	
	public function canChain():Bool { return true; }
}