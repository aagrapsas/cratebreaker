package game.crates.handlers;
import game.crates.Crate;
import game.crates.CrateFactory;
import game.crates.logic.MysteryCrateLogic;

/**
 * ...
 * @author A.A. Grapsas
 */

class MysteryCrateTouchHandler implements ICrateTouchHandler
{
	public function new() 
	{
		
	}	
	
	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		var mysteryLogic:MysteryCrateLogic = cast crate.logic;
		
		CrateFactory.switchCrate( crate, mysteryLogic.currentType );
		
		return false;
	}
	
	public function getType():String { return CrateFactory.MYSTERY_CRATE; }
	
	public function canChain():Bool { return false; }
}