package game.crates.handlers;
import game.config.GameConfig;
import game.crates.Crate;
import game.crates.CrateFactory;

/**
 * ...
 * @author A.A. Grapsas
 */

class NukeCrateTouchHandler implements ICrateTouchHandler
{
	public function new() 
	{
		
	}
	
	public function handleTouch( crate:Crate, chain:Array<Crate> ):Bool
	{
		if ( chain.length < 3 )
		{
			return true;
		}
		
		// Nuke the level
		for ( crateToNuke in GameConfig.PTR.currentLevel.getSpawner().crates )
		{
			if ( crateToNuke.isAlive == false )
			{
				continue;
			}
			
			if ( crateToNuke == crate )
			{
				continue;
			}
			
			crateToNuke.externallyDestroy();
		}
		
		return true;
	}
	
	public function getType():String { return CrateFactory.NUKE_CRATE; }
	
	public function canChain():Bool { return true; }
}