package game.crates.logic;
import game.crates.Crate;

/**
 * ...
 * @author A.A. Grapsas
 */

interface ICrateLogic 
{
	function setup( crate:Crate ):Void;
	function update( crate:Crate, deltaTime:Float ):Void;
	function breakDown( crate:Crate ):Void;
}