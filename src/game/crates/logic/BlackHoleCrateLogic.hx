package game.crates.logic;
import engine.collision.SceneNode;
import engine.render.fonts.DynamicText;
import engine.render.particles.InWorldFXPlayer;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.config.SortOrders;
import game.crates.Crate;
import game.fx.BlackHoleFX;

/**
 * ...
 * @author A.A. Grapsas
 */

class BlackHoleCrateLogic implements ICrateLogic
{
	private var blackholeFX:BlackHoleFX;
	
	private var remainingTimeText:DynamicText;
	private var remainingTime:Float = 0;
	
	private static inline var LIFE_TIME:Float = 20;
	
	private var isAlive:Bool = true;
	
	public function new()
	{
		
	}
	
	public function setup( crate:Crate ):Void
	{
		blackholeFX = new BlackHoleFX();
		
		InWorldFXPlayer.PTR.playFX( blackholeFX, crate.x, crate.y );
		
		remainingTime = LIFE_TIME;
		
		remainingTimeText = new DynamicText();
		remainingTimeText.sortOrder = SortOrders.IN_GAME_FORE;
		remainingTimeText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		remainingTimeText.setText( Std.string( Std.int( remainingTime ) ) );
	}
	
	public function update( crate:Crate, deltaTime:Float ):Void
	{
		if ( isAlive == false )
		{
			return;
		}
		
		remainingTime -= deltaTime;
		
		if ( remainingTime <= 0 )
		{
			crate.externallyDestroy();
			return;
		}
		
		blackholeFX.x = crate.tile.x;
		blackholeFX.y = crate.tile.y;
		blackholeFX.centerX = blackholeFX.x;	// @hack: there's better ways to do this
		blackholeFX.centerY = blackholeFX.y;
		
		// Check above & below for destruction
		var allNeighbors:Array<Crate> = crate.getNeighbors();
		
		for ( neighbor in allNeighbors )
		{
			if ( crate == neighbor || crate.x != neighbor.x )
			{
				continue;
			}
			
			if ( neighbor.logic != null && Std.is( neighbor.logic, BlackHoleCrateLogic ) == true )
			{
				continue;
			}
			
			if ( neighbor.isAlive == false )
			{
				continue;
			}
			
			neighbor.externallyDestroy();
		}
		
		// Handle count down text
		remainingTimeText.setText( Std.string( Std.int( remainingTime ) ) );
		remainingTimeText.x = crate.tile.x - remainingTimeText.getWidth() / 2;
		remainingTimeText.y = crate.tile.y;
		remainingTimeText.tick( deltaTime );
	}
	
	public function breakDown( crate:Crate ):Void
	{
		if ( isAlive == false )
		{
			return;
		}
		
		isAlive = false;
		
		blackholeFX.softStop();
		
		remainingTimeText.destroy();
		remainingTimeText = null;
	}
}