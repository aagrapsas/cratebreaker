package game.crates.logic;
import engine.assets.AssetManager;
import engine.render.GameTile;
import engine.scene.SceneManager;
import game.config.SortOrders;
import game.crates.Crate;
import game.crates.CrateFactory;

/**
 * ...
 * @author A.A. Grapsas
 */

class MysteryCrateLogic implements ICrateLogic
{
	public var currentType:String = null;
	
	private static inline var CHANGE_EVERY_SECONDS:Float = 0.75;
	
	private static var options:Array<String> = [ CrateFactory.EXPLODE_CRATE, CrateFactory.GEM_CRATE, CrateFactory.RADIO_ACTIVE_CRATE ];
	private var accumulator:Float = 0;
	
	private var overlay:GameTile;
	
	private var isAlive:Bool = true;
	
	public function new() 
	{
		
	}	
	
	public function setup( crate:Crate ):Void
	{
		overlay = AssetManager.PTR.getGameBitmap( "mysterybox" ).getTile();
		overlay.sort = SortOrders.IN_GAME_FORE;
		
		SceneManager.PTR.addTile( overlay );
		
		overlay.x = crate.tile.x;
		overlay.y = crate.tile.y;
		
		setToRandomCrate( crate );
	}
	
	public function update( crate:Crate, deltaTime:Float ):Void
	{
		if ( isAlive == false )
		{
			return;
		}
		
		accumulator += deltaTime;
		
		if ( accumulator >= CHANGE_EVERY_SECONDS )
		{
			accumulator = 0;
			
			// Switch display & touch handler
			setToRandomCrate( crate );
		}
		
		overlay.x = crate.tile.x;
		overlay.y = crate.tile.y;
	}
	
	private function setToRandomCrate( crate:Crate ):Void
	{
		var random:Int = Std.int( Math.floor( Math.random() * options.length ) );
		
		var crateType:String = options[ random ];
		
		CrateFactory.switchCrateDisplay( crate, crateType );
		
		currentType = crateType;
	}
	
	public function breakDown( crate:Crate ):Void
	{
		if ( isAlive == false )
		{
			return;
		}
		
		isAlive = false;
		
		SceneManager.PTR.removeTile( overlay );
		
		overlay = null;
	}
}