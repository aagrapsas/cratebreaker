package game.crates.logic;
import engine.render.fonts.DynamicText;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.config.SortOrders;
import game.crates.Crate;

/**
 * ...
 * @author A.A. Grapsas
 */

class TimeCrateLogic implements ICrateLogic
{	
	private var remainingTimeText:DynamicText;
	
	private var isActive:Bool = false;
	
	private var remainingTime:Float = 0;
	
	public function new() 
	{
		
	}

	public function setup( crate:Crate ):Void
	{
		
	}
	
	public function beginCountdown( time:Float ):Void
	{
		if ( isActive )
		{
			return;
		}
		
		isActive = true;
		
		remainingTime = time;
		
		remainingTimeText = new DynamicText();
		remainingTimeText.sortOrder = SortOrders.IN_GAME_FORE;
		remainingTimeText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		remainingTimeText.setText( Std.string( Std.int( remainingTime ) ) );
	}
	
	public function update( crate:Crate, deltaTime:Float ):Void
	{
		if ( isActive == false )
		{
			return;
		}
		
		remainingTime -= ( deltaTime / SceneManager.PTR.slomoValue );	// we're assuming we're in slomo (except our timer!)
		
		remainingTimeText.tick( deltaTime );
		remainingTimeText.setText( Std.string( Std.int( remainingTime ) ) );
		remainingTimeText.x = crate.tile.x - ( remainingTimeText.getWidth() / 2 );
		remainingTimeText.y = crate.tile.y;
		
		if ( remainingTime <= 0 )
		{
			crate.externallyDestroy();
		}
	}
	
	public function breakDown( crate:Crate ):Void
	{
		if ( isActive == false )
		{
			return;
		}
		
		isActive = false;
		
		remainingTimeText.destroy();
		remainingTimeText = null;
	}
}