package game.crates.logic;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.MathUtility;
import engine.render.fonts.DynamicText;
import engine.render.particles.InWorldFXPlayer;
import game.ActorFactory;
import game.config.GameConfig;
import game.config.SortOrders;
import game.crates.Crate;
import game.fx.WhiteHoleFX;
import game.gems.Gem;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class WhiteHoleCrateLogic implements ICrateLogic
{
	private var emitter:WhiteHoleFX;
	
	private var remainingTimeText:DynamicText;
	private var remainingTime:Float = 0;
	
	private static inline var LIFE_TIME:Float = 20;
	
	private var timeToGem:Float = 0;
	
	private static inline var MAX_TIME_TO_CRATE:Float = 3;
	private static inline var MIN_TIME_TO_CRATE:Float = 0.10;
	
	private var isAlive:Bool = true;
	
	public function new() 
	{
		
	}
	
	public function setup( crate:Crate ):Void
	{
		emitter = new WhiteHoleFX();
		
		InWorldFXPlayer.PTR.playFX( emitter, crate.tile.x, crate.tile.y );
		
		remainingTime = LIFE_TIME;
		
		remainingTimeText = new DynamicText();
		remainingTimeText.sortOrder = SortOrders.IN_GAME_FORE;
		remainingTimeText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		remainingTimeText.setText( Std.string( Std.int( remainingTime ) ) );
	}
	
	public function update( crate:Crate, deltaTime:Float ):Void
	{
		if ( isAlive == false )
		{
			return;
		}
		
		remainingTime -= deltaTime;
		
		if ( remainingTime <= 0 )
		{
			crate.externallyDestroy();
			return;
		}
		
		emitter.x = crate.tile.x;
		emitter.y = crate.tile.y;
		
		remainingTimeText.setText( Std.string( Std.int( remainingTime ) ) );
		remainingTimeText.x = crate.tile.x - remainingTimeText.getWidth() / 2;
		remainingTimeText.y = crate.tile.y;
		remainingTimeText.tick( deltaTime );
		
		timeToGem -= deltaTime;
		
		if ( timeToGem <= 0 )
		{
			spawnGem( crate );
			
			var alpha:Float = ( LIFE_TIME - remainingTime ) / LIFE_TIME;
			
			timeToGem = MathUtility.easeOut( MAX_TIME_TO_CRATE, MIN_TIME_TO_CRATE, alpha );
		}
	}
	
	private function spawnGem( crate:Crate ):Void
	{
		var gem:Gem = ActorFactory.spawnRandomGem( crate.x, crate.y );
		
		var point:Point = new Point();
		
		point.x = Math.random() * 1000 * ( Math.random() > 0.5 ? 1 : -1 );
		point.y = Math.random() * 1000 * ( Math.random() > 0.5 ? 1 : -1 );
		
		point.normalize( 1 );
		
		gem.velocityX = point.x * Gem.STARTING_VELOCITY;
		gem.velocityY = point.y * Gem.STARTING_VELOCITY;
	}
	
	public function breakDown( crate:Crate ):Void
	{
		if ( isAlive == false )
		{
			return;
		}
		
		isAlive = false;
		
		emitter.softStop();
		
		remainingTimeText.destroy();
		remainingTimeText = null;
	}
}