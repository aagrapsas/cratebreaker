package game.crates;
import game.config.GameConfig;
import game.purchases.PurchaseData;

/**
 * ...
 * @author A.A. Grapsas
 */

class CrateCatalog 
{
	private var crates:Hash<CrateInfo>;
	private var allCrates:Array<CrateInfo>;
	private var allNonDefaultCrates:Array<CrateInfo>;
	private var sets:Hash<CrateSet>;
	private var allPurchasables:Array<PurchaseData>;
	
	public function new() 
	{
		crates = new Hash<CrateInfo>();
		allCrates = new Array<CrateInfo>();
		sets = new Hash<CrateSet>();
		allNonDefaultCrates = new Array<CrateInfo>();
		
		allPurchasables = new Array<PurchaseData>();
		
		setup();
		setupPurchasables();
	}
	
	private function setup():Void
	{
		// Default -- useless
		addCrate( CrateFactory.BORING_CRATE, "default", 0.1 );
		addCrate( CrateFactory.FRAGILE_CRATE, "default", 0.1 );
		
		// Default -- death
		addCrate( CrateFactory.DEATH_CRATE, "default", 0.005 );
		
		// Default -- gem crates
		addCrate( CrateFactory.RED_CRATE, "default", 0.01 );
		addCrate( CrateFactory.GREEN_CRATE, "default", 0.05 );
		addCrate( CrateFactory.PURPLE_CRATE, "default", 0.08 );
		addCrate( CrateFactory.GEM_CRATE, "default", 0.05 );
		
		// Space
		addCrate( CrateFactory.BLACK_HOLE_CRATE, "space", 0.05 );
		addCrate( CrateFactory.WHITE_HOLE_CRATE, "space", 0.05 );
		
		// Morphing
		addCrate( CrateFactory.RADIO_ACTIVE_CRATE, "morph", 0.05 );
		addCrate( CrateFactory.MYSTERY_CRATE, "morph", 0.05, CrateFactory.BORING_CRATE, "mysterybox_representation" );
		
		// Explosive
		addCrate( CrateFactory.LOCK_CRATE, "explosive", 0.05 );
		addCrate( CrateFactory.EXPLODE_CRATE, "explosive", 0.05 );
		
		// Time
		addCrate( CrateFactory.TIME_CRATE, "time", 0.05 );
		
		// Gem morphing
		addCrate( CrateFactory.MINING_CRATE, "mining", 0.05 );
		
		addCrate( CrateFactory.NUKE_CRATE, "nuke", 0.05 );
	}
	
	private function setupPurchasables():Void
	{
		addPurchasableCrate( "morph", 50, 5, 0 );
		addPurchasableCrate( "explosive", 25, 0, 0 );
		addPurchasableCrate( "space", 100, 100, 25 );
		addPurchasableCrate( "time", 50, 0, 0 );
		addPurchasableCrate( "mining", 100, 50, 0 );
		addPurchasableCrate( "nuke", 25, 0, 0 );
		
		addPurchasableCrate( "morph", 50, 5, 0 );
		addPurchasableCrate( "explosive", 25, 0, 0 );
		addPurchasableCrate( "space", 100, 100, 25 );
		addPurchasableCrate( "time", 50, 0, 0 );
		addPurchasableCrate( "mining", 100, 50, 0 );
		addPurchasableCrate( "nuke", 25, 0, 0 );
		addPurchasableCrate( "morph", 50, 5, 0 );
		addPurchasableCrate( "explosive", 25, 0, 0 );
		addPurchasableCrate( "space", 100, 100, 25 );
		addPurchasableCrate( "time", 50, 0, 0 );
		addPurchasableCrate( "mining", 100, 50, 0 );
		addPurchasableCrate( "nuke", 25, 0, 0 );
		addPurchasableCrate( "morph", 50, 5, 0 );
		addPurchasableCrate( "explosive", 25, 0, 0 );
		addPurchasableCrate( "space", 100, 100, 25 );
		addPurchasableCrate( "time", 50, 0, 0 );
		addPurchasableCrate( "mining", 100, 50, 0 );
		addPurchasableCrate( "nuke", 25, 0, 0 );
	}
	
	private function addPurchasableCrate( set:String, purpleCost:Int, greenCost:Int, redCost:Int ):Void
	{
		var data:PurchaseData = new PurchaseData();
		data.isCrateSet = true;
		data.setKey = set;
		data.purpleCost = purpleCost;
		data.greenCost = greenCost;
		data.redCost = redCost;
		
		allPurchasables.push( data );
	}
	
	private function addCrate( name:String, set:String, proability:Float, asset:String = null, uiAsset:String = null ):Void
	{
		if ( asset == null )
		{
			asset = name;
		}
		
		if ( uiAsset == null )
		{
			uiAsset = asset;
		}
		
		// Setup info
		var info:CrateInfo = new CrateInfo( name, asset, proability, set, uiAsset );
		
		if ( set != "default" )
		{
			allNonDefaultCrates.push( info );
		}
		
		// Generate new set if none exists
		if ( sets.exists( set ) == false )
		{
			sets.set( set, new CrateSet( set, new Array<String>() ) );
		}
		
		// Get set and add to it
		sets.get( set ).set.push( name );
		
		// Add to entire list
		allCrates.push( info );
		
		// Add to look up
		crates.set( name, info );
	}
	
	public function getCrate( type:String ):CrateInfo { return crates.get( type ); }
	
	public function getSet( type:String ):CrateSet { return sets.get( type ); }
	
	public function getListFromSet( type:String ):Array<CrateInfo>
	{
		var set:CrateSet = sets.get( type );
		
		var returnSet:Array<CrateInfo> = new Array<CrateInfo>();
		
		for ( element in set.set )
		{
			returnSet.push( crates.get( element ) );
		}
		
		return returnSet;
	}
	
	public function getAllUnlockedNonDefaultCrates():Array<CrateInfo>
	{
		var set:Array<CrateInfo> = new Array<CrateInfo>();
		var lookUp:Hash<String> = new Hash<String>();
		
		for ( setKey in GameConfig.PTR.userData.unlockedSets )
		{
			lookUp.set( setKey, setKey );
		}
		
		for ( possible in allNonDefaultCrates )
		{
			if ( lookUp.exists( possible.crateSet ) )
			{
				set.push( possible );
			}
		}
		
		return set;
	}
	
	public function getAllNonDefaultCrates():Array<CrateInfo> { return allNonDefaultCrates; }
	
	public function getAllCrates():Array<CrateInfo> { return allCrates; }
	
	public function getAllPurchaseData():Array<PurchaseData> { return allPurchasables; }
}