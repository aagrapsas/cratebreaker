package game.crates;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import game.ActorFactory;
import game.config.GameConfig;
import game.crates.handlers.BoringCrateTouchHandler;
import game.crates.handlers.DeathCrateTouchHandler;
import game.crates.handlers.ExplodingCrateTouchHandler;
import game.crates.handlers.FragileCrateTouchHandler;
import game.crates.handlers.GemCrateTouchHandler;
import game.crates.handlers.GreenCrateTouchHandler;
import game.crates.handlers.ICrateTouchHandler;
import game.crates.handlers.MiningCrateTouchHandler;
import game.crates.handlers.MysteryCrateTouchHandler;
import game.crates.handlers.NukeCrateTouchHandler;
import game.crates.handlers.PurpleCrateTouchHandler;
import game.crates.handlers.RadioActiveCrateTouchHandler;
import game.crates.handlers.RedCrateTouchHandler;
import game.crates.handlers.TimeCrateTouchHandler;
import game.crates.handlers.ToxicCrateTouchHandler;
import game.crates.logic.BlackHoleCrateLogic;
import game.crates.logic.ICrateLogic;
import game.crates.logic.MysteryCrateLogic;
import game.crates.logic.TimeCrateLogic;
import game.crates.logic.WhiteHoleCrateLogic;

/**
 * ...
 * @author A.A. Grapsas
 */

class CrateFactory 
{
	public static inline var BORING_CRATE:String = "crate";
	public static inline var EXPLODE_CRATE:String = "explosives";
	public static inline var GEM_CRATE:String = "gemcrate";
	public static inline var RADIO_ACTIVE_CRATE:String = "radioactive";
	public static inline var TOXIC_CRATE:String = "biohazard";
	public static inline var FRAGILE_CRATE:String = "fragile";
	public static inline var RED_CRATE:String = "redcrate";
	public static inline var PURPLE_CRATE:String = "purplecrate";
	public static inline var GREEN_CRATE:String = "greencrate";
	public static inline var MYSTERY_CRATE:String = "mysterybox";
	public static inline var LOCK_CRATE:String = "lockcrate";
	public static inline var BLACK_HOLE_CRATE:String = "blackcrate";
	public static inline var TIME_CRATE:String = "timecrate";
	public static inline var WHITE_HOLE_CRATE:String = "whitecrate";
	public static inline var DEATH_CRATE:String = "deathcrate";
	public static inline var SMILE_CRATE:String = "smiletime";
	public static inline var SMILE_CRATE2:String = "smiletime2";
	public static inline var SMILE_CRATE3:String = "smiletime3";
	public static inline var SMILE_CRATE4:String = "smiletime4";
	public static inline var MINING_CRATE:String = "miningcrate";
	public static inline var NUKE_CRATE:String = "nukecrate";
	
	public static function getFunction( type:String ):ICrateTouchHandler
	{
		switch ( type )
		{
			case EXPLODE_CRATE:
				return new ExplodingCrateTouchHandler();
			case GEM_CRATE:
				return new GemCrateTouchHandler();
			case RADIO_ACTIVE_CRATE:
				return new RadioActiveCrateTouchHandler();
			case TOXIC_CRATE:
				return new ToxicCrateTouchHandler();
			case FRAGILE_CRATE:
				return new FragileCrateTouchHandler();
			case BORING_CRATE:
				return new BoringCrateTouchHandler();
			case RED_CRATE:
				return new RedCrateTouchHandler();
			case GREEN_CRATE:
				return new GreenCrateTouchHandler();
			case PURPLE_CRATE:
				return new PurpleCrateTouchHandler();
			case MYSTERY_CRATE:
				return new MysteryCrateTouchHandler();
			case TIME_CRATE:
				return new TimeCrateTouchHandler();
			case DEATH_CRATE:
				return new DeathCrateTouchHandler();
			case MINING_CRATE:
				return new MiningCrateTouchHandler();
			case NUKE_CRATE:
				return new NukeCrateTouchHandler();
		}
		
		return null;
	}
	
	public static function getLogic( type:String ):ICrateLogic
	{
		switch ( type )
		{
			case MYSTERY_CRATE:
				return new MysteryCrateLogic();
			case BLACK_HOLE_CRATE:
				return new BlackHoleCrateLogic();
			case TIME_CRATE:
				return new TimeCrateLogic();
			case WHITE_HOLE_CRATE:
				return new WhiteHoleCrateLogic();
		}
		
		return null;
	}
	
	public static function getCrate( type:String, x:Float, y:Float ):Crate
	{
		var newInfo:CrateInfo = GameConfig.PTR.crateCatalog.getCrate( type );
		
		var crate:Crate = ActorFactory.spawnCrate( newInfo.crateAsset, x, y );
		crate.touchHandler = getFunction( type );
		crate.logic = getLogic( type );
		
		crate.crateType = type;
		
		if ( crate.logic != null )
		{
			crate.logic.setup( crate );
		}
		
		return crate;
	}
	
	public static function switchCrate( crate:Crate, newType:String ):Void
	{
		crate.crateType = newType;
		
		var newInfo:CrateInfo = GameConfig.PTR.crateCatalog.getCrate( newType );
		
		var asset:GameBitmap = AssetManager.PTR.getGameBitmap( newInfo.crateAsset );
		crate.tile.id = asset.tileID;
		crate.touchHandler = getFunction( newType );
		
		if ( crate.logic != null )
		{
			crate.logic.breakDown( crate );
			crate.logic = null;
		}
		
		crate.logic = getLogic( newType );
		
		if ( crate.logic != null )
		{
			crate.logic.setup( crate );
		}
		
		crate.touchHandler = getFunction( newType );
		
		#if flash
		crate.tile.bitmap = asset.getNewBitmap();
		#end
	}
	
	public static function switchCrateDisplay( crate:Crate, newType:String ):Void
	{
		var newInfo:CrateInfo = GameConfig.PTR.crateCatalog.getCrate( newType );
		
		var assetName:String = newInfo != null ? newInfo.crateAsset : newType;
		
		var asset:GameBitmap = AssetManager.PTR.getGameBitmap( assetName );
		crate.tile.id = asset.tileID;
		
		#if flash
		crate.tile.bitmap = asset.getNewBitmap();
		#end
	}
}