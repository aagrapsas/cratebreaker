package game.crates;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.ITickable;
import engine.misc.MathUtility;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.ActorFactory;
import nme.events.Event;

/**
 * ...
 * @author A.A. Grapsas
 */

class CrateSpawner implements ITickable
{
	public var crates:Array<Crate>;
	public var allowedLanes:Array<Float>;
	
	private var lifeTime:Float = 0;
	private var timeUntilNextSpawn:Float = 0;
	
	private static inline var SLOWEST_TIME_TO_SPAWN:Float = 3.0;
	private static inline var FASTEST_TIME_TO_SPAWN:Float = 0.2;
	private static inline var TIME_TO_FASTEST:Float = 360.0;
	
	private static inline var SLOWEST_ACCELERATION:Float = 100.0;
	private static inline var FASTEST_ACCELERATION:Float = 400.0;
	
	private static inline var SPAWN_Y:Float = -20;
	
	private var crateDescriptors:Array<CrateInfo>;
	private var combinedProbability:Float = 0;
	
	public var shouldAllowSpawn:Bool = true;
	
	private var startingRows:Array<String>;
	
	public function new() 
	{
		crates = new Array<Crate>();
	}
	
	public function reset():Void
	{
		for ( crate in crates )
		{
			crate.destroy();
			
			SceneManager.PTR.removeTickable( crate );
			SceneManager.PTR.removeActor( crate );
		}
		
		Crate.ResetUniqueIDCount();
		
		crates.splice( 0, crates.length );
		
		lifeTime = 0;
		timeUntilNextSpawn = 0;
		
		shouldAllowSpawn = true;
	}
	
	public function setup( allowedCrates:Array<CrateInfo> ):Void
	{
		crateDescriptors = allowedCrates;
		
		for ( crateDescriptor in crateDescriptors )
		{
			combinedProbability += crateDescriptor.crateProbability;
		}
		
		// Calculate allowed lanes
		allowedLanes = new Array<Float>();
		
		var genericCrate:GameBitmap = AssetManager.PTR.getGameBitmap( "crate" );
		
		var offsetX:Float = 4;
		
		var center:Float = ResolutionConfig.PTR.expectedScreenWidth / 2;			// calculate as offset of center for everything
		allowedLanes.push( center ); 												// center
		allowedLanes.push( center - genericCrate.originalWidth - offsetX );			// left of center
		allowedLanes.push( center - ( 2 * genericCrate.originalWidth ) - ( 2 * offsetX ) );			// left most
		allowedLanes.push( center + genericCrate.originalWidth + offsetX );			// right of center
		allowedLanes.push( center + ( 2 * genericCrate.originalWidth ) + ( 2 * offsetX ) );	// right most
	}
	
	private function getRandomCrate():Crate
	{
		var crateDescriptor:CrateInfo = null;
		
		var random:Float = Math.random() * combinedProbability;
		var runningProbability:Float = 0;
		
		for ( candidate in crateDescriptors )
		{
			runningProbability += candidate.crateProbability;
			
			if ( random < runningProbability )
			{
				crateDescriptor = candidate;
				break;
			}
		}
		
		if ( crateDescriptor == null )
		{
			Debug.log( DebugChannels.ERROR, "Something went wrong when calculating crate probability" );
			return null;
		}
		
		var crate:Crate = CrateFactory.getCrate( crateDescriptor.crateName, 0, 0 );
		
		crates.push( crate );
		
		crate.addEventListener( Crate.CRATE_DESTROYED, onCrateDestroyed );
		
		SceneManager.PTR.addActor( crate );
		SceneManager.PTR.addTickable( crate );
		
		return crate;
	}
	
	private function onCrateDestroyed( e:Event ):Void
	{
		var crate:Crate = cast e.target;
		
		crate.removeEventListener( Crate.CRATE_DESTROYED, onCrateDestroyed );
		
		SceneManager.PTR.removeTickable( crate );
		SceneManager.PTR.removeActor( crate );
		
		crates.remove( crate );
	}
	
	private function getCrateX():Float
	{
		// Calculates one of five allowed lanes
		var random:Int = Math.floor( Math.random() * allowedLanes.length );
		return allowedLanes[ random ];
	}
	
	public function tick( deltaTime:Float ):Void
	{
		timeUntilNextSpawn -= deltaTime;
		lifeTime += deltaTime;
		
		if ( shouldAllowSpawn && timeUntilNextSpawn <= 0 )
		{
			var spawnAlpha:Float = Math.min( lifeTime / TIME_TO_FASTEST, 1 );
			
			// Spawn crate!
			var crate:Crate = getRandomCrate();
			crate.x = getCrateX();
			crate.y = SPAWN_Y;
			crate.acceleration = MathUtility.lerp( SLOWEST_ACCELERATION, FASTEST_ACCELERATION, spawnAlpha );
			
			// Setup next spawn
			timeUntilNextSpawn = MathUtility.easeOut( SLOWEST_TIME_TO_SPAWN, FASTEST_TIME_TO_SPAWN, spawnAlpha );
		}
	}
	
	public function spawnRow():Void
	{
		var spawnAlpha:Float = Math.min( lifeTime / TIME_TO_FASTEST, 1 );
		
		for (lane in allowedLanes)
		{
			var crate:Crate = getRandomCrate();
			crate.x = lane;
			crate.y = SPAWN_Y;
			crate.acceleration = MathUtility.lerp( SLOWEST_ACCELERATION, FASTEST_ACCELERATION, spawnAlpha );
		}
	}
	
	public function addCrate( crate:Crate ):Void
	{
		crates.push( crate );
		
		crate.addEventListener( Crate.CRATE_DESTROYED, onCrateDestroyed );
		
		SceneManager.PTR.addTickable( crate );
		SceneManager.PTR.addActor( crate );
	}
	
	public function preSpawn( rows:Int, useCache:Bool = false ):Void
	{
		var crate:Crate;
		
		var runningY:Float = ResolutionConfig.PTR.expectedScreenHeight;
		var yIncrement:Float = AssetManager.PTR.getGameBitmap( "crate" ).originalHeight / 2;
		
		if (useCache == false)
		{
			startingRows = new Array<String>();
			for ( i in 0...rows )
			{	
				runningY -= yIncrement;
				
				for ( j in 0...allowedLanes.length )
				{
					crate = getRandomCrate();
					crate.x = allowedLanes[ j ];
					crate.y = runningY;
					
					startingRows.push( crate.crateType );
				}
			}
		}
		else
		{
			var runningI:Int = 0;
			for ( i in 0...cast ( startingRows.length / allowedLanes.length ) )
			{	
				runningY -= yIncrement;
				for ( j in 0...allowedLanes.length )
				{
					crate = CrateFactory.getCrate( startingRows[ runningI ], allowedLanes[ j ], runningY );
					runningI++;
					addCrate( crate );
				}
			}
		}
	}
}