package game.crates;

/**
 * ...
 * @author A.A. Grapsas
 */

class CrateSet 
{
	public var name:String;
	public var set:Array<String>;
	
	public function new( name:String, set:Array<String> )
	{
		this.name = name;
		this.set = set;
	}
}