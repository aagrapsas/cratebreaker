package game.crates;
import engine.collision.SceneNode;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.ITickable;
import engine.misc.ITouchable;
import engine.render.particles.InWorldFXPlayer;
import engine.render.ResolutionConfig;
import engine.scene.Actor;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.crates.handlers.ICrateTouchHandler;
import game.crates.logic.ICrateLogic;
import game.fx.CrateChainFX;
import game.fx.CrateTouchFX;
import nme.events.Event;

/**
 * ...
 * @author A.A. Grapsas
 */

class Crate extends Actor, implements ITickable, implements ITouchable
{
	public static inline var CRATE_DESTROYED:String = "destroyed";
	
	private static var uniqueIDCount:Int = 0;
	
	public var uniqueID:Int = 0;
	public var isAlive( default, null ):Bool = true;
	
	public var acceleration:Float = 100;
	
	public var isStationary( default, null ):Bool = false;
	
	public var touchHandler:ICrateTouchHandler;
	public var logic:ICrateLogic;
	
	// Crates only fall DOWN
	public var velocityY:Float = 0;
	
	public var crateType:String = "none";	// identifier for the crate
	
	public static function ResetUniqueIDCount():Void
	{
		uniqueIDCount = 0;
	}
	
	public function new() 
	{
		uniqueID = uniqueIDCount;
		
		uniqueIDCount++;
		
		super();
	}
	
	public function tick( deltaTime:Float ):Void
	{
		
		if ( isAlive == false )
		{
			return;
		}
		
		var isOnBottom:Bool = ( this.y + ( this.bitmap.originalHeight / 2 ) ) >= ResolutionConfig.PTR.expectedScreenHeight;
		
		//If you're on the bottom, you're done.
		if ( isOnBottom )
		{
			isStationary = true;
			this.y = ResolutionConfig.PTR.expectedScreenHeight - ( this.bitmap.originalHeight / 2 );
		}
		else
		{
			//If you're falling, let's check where you are to where you'll go. Otherwise, we check right under you.
			var checkHeight:Float = 2;
			if ( isStationary == false )
			{
				velocityY += acceleration * deltaTime;
				checkHeight += velocityY * deltaTime;
			}
			
			var centerY:Float = this.y + this.bitmap.originalHeight / 2 + checkHeight / 2;
			var potentialColliders:Array<SceneNode> = SceneManager.PTR.getCollidersWorldSpace( this.x, centerY, this.bitmap.originalWidth * 0.9, checkHeight );
			
			var didNotCollide:Bool = true;
			var finalCollider:Crate = null;
			
			for ( collider in potentialColliders )
			{
				if ( collider.internalObject != this && Std.is( collider.internalObject, Crate ) )
				{
					didNotCollide = false;
					finalCollider = cast collider.internalObject;
					break;
				}
			}
			
			if ( didNotCollide )
			{
				if ( isStationary )
				{
					isStationary = false;
				}
				else
				{
					this.y += velocityY * deltaTime;
				}
			}
			else
			{
				//If I'm falling and they're falling, just attach and fall as a unit.
				if ( finalCollider.isStationary == false )
				{
					velocityY = finalCollider.velocityY;
					this.y = finalCollider.y - ( this.bitmap.originalHeight / 2 ) - ( finalCollider.bitmap.originalHeight / 2 );
				}
				else
				{
					isStationary = true;
					velocityY = acceleration * 4;
					this.y = finalCollider.y - ( this.bitmap.originalHeight / 2 ) - ( finalCollider.bitmap.originalHeight / 2 );
				}
			}
		}
		
		if ( logic != null )
		{
			logic.update( this, deltaTime );
		}
	}
	
	public function handleTouch():Void
	{
		
	}
	
	/**
	 * Comes from the SceneManager, notifies this crate that it has been touched by the player
	 */
	public function handleTap():Void
	{
		if ( isAlive == false || touchHandler == null )
		{
			return;
		}
		
		InWorldFXPlayer.PTR.playFX( new CrateTouchFX(), this.x, this.y );
		
		var chain:Array<Crate> = null;
		
		if ( touchHandler.canChain() )
		{
			chain = getChain();
			
			var chainLength:Int = chain.length;
			var chainValue:Int = 0;
			
			if ( chainLength > 0 )
			{
				//Calculate chain value or dispatch something about the chain
				GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.CHAIN_LENGTH, chainLength );
			}
			
			for ( chainMember in chain )
			{
				//Skip over ourselves
				if ( chainMember == this )
				{
					continue;
				}
				
				chainMember.notifyPartOfChain( chain );
			}
		}
		
		var shouldConsume:Bool = touchHandler.handleTouch( this, chain );

		if ( shouldConsume  )
		{
			removeFromScene();
			
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.CRATE_SMASHED, this );
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.CRATE_CLICK_DESTROYED, this );
		}
	}
	
	/**
	 * Comes from another crate, notifies this crate that it is being used in a chain-action
	 * @param	chain
	 */
	public function notifyPartOfChain( chain:Array<Crate> ):Void
	{
		if ( isAlive == false || touchHandler == null )
		{
			return;
		}
		
		var shouldConsume:Bool = touchHandler.handleTouch( this, chain );
		
		InWorldFXPlayer.PTR.playFX( new CrateChainFX(), this.x, this.y );
		
		if ( shouldConsume )
		{
			removeFromScene();
			
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.CRATE_SMASHED, this );
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.CRATE_CHAIN_DESTROYED, this );
		}
	}
	
	//Returns an array of all touching crates. Includes corners. Up to you to prune.
	public function getNeighbors():Array<Crate>
	{
		var centerX:Float = this.x;
		var centerY:Float = this.y;
		var worldWidthToSearch:Float = this.bitmap.originalWidth * 2;
		var worldHeightToSearch:Float = this.bitmap.originalHeight * 2;
		
		var allNeighbors:Array<SceneNode> = SceneManager.PTR.getCollidersWorldSpace( centerX, centerY, worldWidthToSearch, worldHeightToSearch );
		
		var returnSet:Array<Crate> = new Array<Crate>();
		
		for ( neighbor in allNeighbors )
		{
			if ( neighbor.internalObject == this )
			{
				continue;
			}
			
			if ( Std.is( neighbor.internalObject, Crate ) == false )
			{
				continue;
			}
			
			returnSet.push( cast neighbor.internalObject );
		}
		
		return returnSet;
	}
	
	private function getChain():Array<Crate>
	{
		var validColliders:IntHash<Crate> = new IntHash<Crate>();
		var collidersToSearch:Array<Crate> = new Array<Crate>();
		var finalSet:Array<Crate> = new Array<Crate>();
		
		collidersToSearch.push( this );
		
		// Populate valid colliders with all valid neighbors of our type
		while ( collidersToSearch.length > 0 )
		{
			
			var searchTarget:Crate = collidersToSearch.shift();
			var allNeighbors:Array<Crate> = searchTarget.getNeighbors();
			
			for ( neighbor in allNeighbors )
			{
				//I'm already in the chain.
				if ( neighbor == searchTarget )
				{
					continue;
				}
				
				// Exclude if not in our row or column
				// We should change this. Somehow. Crates should have an accessiable row/column
				if ( neighbor.x != searchTarget.x && neighbor.y != searchTarget.y )
				{
					continue;
				}
				
				// Exclude crates we have already looked at
				if ( validColliders.get( neighbor.uniqueID ) != null )
				{
					continue;
				}
				
				// Exclude crates without the appropriate touch handler
				if ( neighbor.touchHandler == null || neighbor.touchHandler.getType() != touchHandler.getType() )
				{
					continue;
				}
				
				// Add valid neighbors for search
				if ( validColliders.exists( neighbor.uniqueID ) == false )
				{
					validColliders.set( neighbor.uniqueID, neighbor );	// Quick look up fulfillment
					finalSet.push( neighbor );							// Why not populate now?
					collidersToSearch.push( neighbor );					// Continue search on children
				}
			}
		}
		
		return finalSet;
	}
	
	/**
	 * Comes from an external source indicating this box should consume itself (as with explosion)
	 */
	public function externallyDestroy():Void
	{
		if ( isAlive )
		{
			InWorldFXPlayer.PTR.playFX( new CrateChainFX( "destroy" ), this.x, this.y );
			
			removeFromScene();	// Force removal from scene (even if this doesn't normally consume)
			
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.CRATE_SMASHED, this );
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.CRATE_EXTERNALLY_DESTROYED, this );
		}
	}
	
	private function removeFromScene():Void
	{
		destroy();
		
		this.dispatchEvent( new Event( CRATE_DESTROYED ) );
	}
	
	// Externally accessable destroy used for level life cycle
	public function destroy():Void
	{
		if ( isAlive == false )
		{
			return;
		}
		
		isAlive = false;
		
		if ( logic != null )
		{
			logic.breakDown( this );
			logic = null;
		}
	}
	
	override public function toString():String {
		var out:String = "[Crate,(id=" + this.uniqueID + ",type=" + this.crateType
					   + ",isAlive=" + this.isAlive + ",isStationary=" + this.isStationary
					   + ",tapHandle=" + this.touchHandler
					   + ",x=" + this.x + ",y=" + this.y+ ",gfx=" + this.bitmap.name + ")]";
		return out;
	}
}