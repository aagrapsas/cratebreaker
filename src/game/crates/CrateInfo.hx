package game.crates;

/**
 * ...
 * @author A.A. Grapsas
 */

class CrateInfo 
{
	public var crateName:String;
	public var crateAsset:String;
	public var crateProbability:Float;
	public var crateSet:String;
	public var uiAsset:String;
	
	public function new( crateName:String, crateAsset:String, crateProbability:Float, crateSet:String, uiAsset:String ) 
	{
		this.crateAsset = crateAsset;
		this.crateName = crateName;
		this.crateProbability = crateProbability;
		this.crateSet = crateSet;
		this.uiAsset = uiAsset;
	}
}