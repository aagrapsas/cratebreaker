package game;
import engine.assets.AssetManager;
import engine.misc.ITickable;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.crates.Crate;
import game.crates.CrateInfo;
import game.crates.CrateSet;
import game.crates.CrateSpawner;
import game.systems.AchievementManager;
import game.systems.ComboLogic;
import game.systems.GameEndConditions;
import game.systems.SmileMaker;
import game.ui.ingame.UIGameOver;
import game.ui.ingame.UIInGame;
import game.ui.nongame.UIMainMenu;
import game.user.SessionStats;
import nme.display.Sprite;
import nme.events.Event;
import nme.geom.Point;
import nme.Lib;

/**
 * ...
 * @author A.A. Grapsas
 */

 
// Simple level system
class CrateLevel implements ITickable
{
	
	private var crateSpawner:CrateSpawner;
	
	private var pauseScrim:Sprite;
	
	private var inGameUI:UIInGame;
	
	private var gameConditions:GameEndConditions;
	
	private var achievementManager:AchievementManager;
	
	private var endAnim:SmileMaker;
	
	private var isAlive:Bool = true;
	
	private var isDying:Bool = false;
	
	private var stats:SessionStats;
	
	private var comboLogic:ComboLogic;
	
	public function new() 
	{
		crateSpawner = new CrateSpawner();
		gameConditions = new GameEndConditions();
		achievementManager = new AchievementManager();
		
		stats = new SessionStats();
		comboLogic = new ComboLogic();
		
		SceneManager.PTR.addTickable( achievementManager, true );
		SceneManager.PTR.addTickable( this );
		SceneManager.PTR.doesAllowSceneInput = true;
		
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.GAME_END, onGameEnd );
		
		GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.GAME_START, null );
	}
	
	public function getUIPosition( element:String ):Point
	{
		if ( isDying )
		{
			return new Point( 0, 0 );
		}
		
		return inGameUI.getUIElementPosition( element );
	}
	
	public function setAllowedCrates( crates:Array<CrateInfo> ):Void
	{		
		var defaultCrates:Array<CrateInfo> = GameConfig.PTR.crateCatalog.getListFromSet( "default" );
		
		var finalCrates:Array<CrateInfo> = new Array<CrateInfo>();
		
		for ( crate in crates )
		{
			finalCrates.push( crate );
		}
		
		for ( defaultCrate in defaultCrates )
		{
			finalCrates.push( defaultCrate );
		}
		
		crateSpawner.setup( finalCrates );
		
		// UIInGame has a dependency on crateSpawner having been setup!
		if ( inGameUI == null )
		{
			inGameUI = new UIInGame();
			inGameUI.comboMeter.setMaxTime( ComboLogic.TIME_TO_ROW );
		}
	}
	
	public function preSpawn( rows:Int, useCache:Bool = false ):Void
	{
		crateSpawner.preSpawn( rows, useCache );
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( isAlive == false )
		{
			return;
		}
		
		comboLogic.tick( deltaTime );
		crateSpawner.tick( deltaTime );
		gameConditions.tick( deltaTime );
		
		if ( comboLogic.shouldSpawnRow )
		{
			crateSpawner.spawnRow();
			comboLogic.reset();
		}
		
		if ( inGameUI != null )
		{
			inGameUI.comboMeter.setTimeElapsed( comboLogic.timeSinceLastCombo );
		}
		
		if ( endAnim != null )
		{
			endAnim.tick( deltaTime );
		}
	}
	
	//Need to investigate here. Some miscommunication between here and new game or something?
	private function onGameEnd( data:Dynamic ):Void
	{		
		isDying = true;
		
		crateSpawner.shouldAllowSpawn = false;
		
		endAnim = new SmileMaker( crateSpawner.crates );
		endAnim.addEventListener( Event.COMPLETE, onEndAnimDone );
		
		SceneManager.PTR.doesAllowSceneInput = false;
		
		inGameUI.destroy();
		inGameUI = null;
		
		GameConfig.PTR.slomoManager.reset();
	}
	
	private function onEndAnimDone( e:Event ):Void
	{
		endAnim.removeEventListener( Event.COMPLETE, onEndAnimDone );
		
		showScrim( 0.75 );
		
		// Show appropriate screen
		SceneManager.PTR.addTickable( new UIGameOver() );
		
		// Merge data (so we can save gem count)
		GameConfig.PTR.userData.mergeSessionData( stats );
		
		// Save everything
		GameConfig.PTR.userData.save();
	}
	
	public function reset( useOldCrates:Bool = false , firstTime:Bool = false ):Void
	{
		if ( isDying )
		{
			return;
		}
		
		if ( SceneManager.PTR.isPaused )
		{
			togglePause();
		}
		
		hideScrim();
		
		// Destroy all existing crates, reset scoring
		crateSpawner.reset();
		inGameUI.reset();
		gameConditions.reset();
		stats.reset();
		
		GameConfig.PTR.slomoManager.reset();
		
		preSpawn(2, useOldCrates); //<--MAGIC NUMBERS!!!! You used them before. But I need to also know when they reset from the reset thing...
		
		// Save data
		if ( firstTime )
		{
			GameConfig.PTR.userData.save();
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.GAME_CANCELED, null );
		}
	}
	
	public function togglePause():Void
	{
		if ( isDying ) 
		{
			return;
		}
		
		SceneManager.PTR.isPaused = !SceneManager.PTR.isPaused;
		
		if ( SceneManager.PTR.isPaused )
		{
			showScrim();
		}
		else
		{
			hideScrim();
		}
	}
	
	public function showScrim( percent:Float = 0.25 ):Void
	{
		if ( pauseScrim != null )
		{
			return;
		}
		
		achievementManager.postAchievement( "smiletime3", "Wow, you managed to show the SCRIM!" );
		
		pauseScrim = new Sprite();
		pauseScrim.graphics.beginFill( 0, percent );
		pauseScrim.graphics.drawRect( 0, 0, ResolutionConfig.PTR.actualScreenWidth, ResolutionConfig.PTR.actualScreenHeight );
		pauseScrim.graphics.endFill();
		
		SceneManager.PTR.tileLayer.addChild( pauseScrim );
	}
	
	public function hideScrim():Void
	{
		if ( pauseScrim == null )
		{
			return;
		}
		
		SceneManager.PTR.tileLayer.removeChild( pauseScrim );
		pauseScrim = null;
	}
	
	public function quit():Void
	{
		if ( isDying )
		{
			return;
		}
		
		if ( SceneManager.PTR.isPaused )
		{
			togglePause();
		}
		
		GameConfig.PTR.userData.save();
		
		GameConfig.PTR.currentLevel = null;
		
		destroy();
		
		SceneManager.PTR.addTickable( new UIMainMenu() );
		
		GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.GAME_CANCELED, null );
	}
	
	public function getSpawner():CrateSpawner { return crateSpawner; }
	
	public function getStats():SessionStats { return stats; }
	
	public function destroy():Void
	{
		isAlive = false;
		
		if ( inGameUI != null )
		{
			inGameUI.destroy();
			inGameUI = null;
		}
		
		stats.destroy();
		
		crateSpawner.reset();
		crateSpawner = null;
		
		SceneManager.PTR.removeTickable( this );
		
		GameConfig.PTR.gameplayEvents.unregister( GameEventTypes.GAME_END, onGameEnd );
	}
}