package game.fx;
import engine.assets.AssetManager;
import engine.misc.LerpTypes;
import engine.render.particles.fx.ExplodeFX;
import engine.render.particles.fx.FadeOverTimeFX;
import engine.render.particles.fx.ScaleOverTimeFX;
import engine.render.particles.fx.SetImageFX;
import engine.render.particles.fx.SetLifeTimeFX;
import engine.render.particles.ParticleEmitter;
import game.config.SortOrders;

/**
 * ...
 * @author A.A. Grapsas
 */

class GemExplosionFX extends ParticleEmitter
{
	private var asset:String;
	
	public function new( asset:String ) 
	{
		this.asset = asset;
		
		super();
		
		setup();
	}
	
	private function setup():Void
	{
		this.particlesPerSecond = 9999;
		this.maxParticles = 12;
		this.maxLifeTime = 0.5;
		
		var imageInit:SetImageFX = new SetImageFX();
		imageInit.images.push( AssetManager.PTR.getGameBitmap( asset ) );
		imageInit.sortOrder = SortOrders.IN_GAME_FORE;
		initializers.push( imageInit );
		
		var lifeTimeFX:SetLifeTimeFX = new SetLifeTimeFX();
		lifeTimeFX.lifeTimeSeconds = 0.5;
		initializers.push( lifeTimeFX );
		
		var explosionFX:ExplodeFX = new ExplodeFX();
		explosionFX.magnitude = 100;
		initializers.push( explosionFX );
		
		var fadeEffect:FadeOverTimeFX = new FadeOverTimeFX();
		fadeEffect.duration = 0.5;
		fadeEffect.startAlpha = 0.8;
		fadeEffect.endAlpha = 0;
		fadeEffect.lerpType = LerpTypes.EASE_OUT;
		effects.push( fadeEffect );
		
		var scaleEffect:ScaleOverTimeFX = new ScaleOverTimeFX();
		scaleEffect.duration = 0.5;
		scaleEffect.lerpType = LerpTypes.EASE_OUT;
		scaleEffect.startScale = 1;
		scaleEffect.endScale = 0.5;
		effects.push( scaleEffect );
	}
}