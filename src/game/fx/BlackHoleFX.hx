package game.fx;
import engine.assets.AssetManager;
import engine.misc.LerpTypes;
import engine.render.particles.fx.FadeOverTimeFX;
import engine.render.particles.fx.MoveOverTimeFX;
import engine.render.particles.fx.ScaleOverTimeFX;
import engine.render.particles.fx.SetImageFX;
import engine.render.particles.fx.SetLifeTimeFX;
import engine.render.particles.fx.SpawnCircleFX;
import engine.render.particles.ParticleEmitter;
import engine.render.ResolutionConfig;
import game.config.SortOrders;

/**
 * ...
 * @author A.A. Grapsas
 */

class BlackHoleFX extends ParticleEmitter
{
	public var centerX( default, setX ):Float;
	public var centerY( default, setY ):Float;
	
	private var moveFX:MoveOverTimeFX;
	
	public function new() 
	{
		super();
		
		setup();
	}
	
	private function setup():Void
	{
		this.particlesPerSecond = 30;
		
		var imageInit:SetImageFX = new SetImageFX();
		imageInit.images.push( AssetManager.PTR.getGameBitmap( "blackparticle" ) );
		imageInit.sortOrder = SortOrders.IN_GAME_FORE;
		initializers.push( imageInit );
		
		var lifeTimeFX:SetLifeTimeFX = new SetLifeTimeFX();
		lifeTimeFX.lifeTimeSeconds = 0.5;
		initializers.push( lifeTimeFX );
		
		var circleInit:SpawnCircleFX = new SpawnCircleFX();
		circleInit.maxRadius = cast ResolutionConfig.PTR.convertWorldToScreen( 180 );
		circleInit.minRadius = cast ResolutionConfig.PTR.convertWorldToScreen( 100 );
		initializers.push( circleInit );
		
		var fadeEffect:FadeOverTimeFX = new FadeOverTimeFX();
		fadeEffect.duration = 0.5;
		fadeEffect.startAlpha = 0.8;
		fadeEffect.endAlpha = 0;
		fadeEffect.lerpType = LerpTypes.EASE_OUT;
		effects.push( fadeEffect );
		
		var scaleEffect:ScaleOverTimeFX = new ScaleOverTimeFX();
		scaleEffect.duration = 0.5;
		scaleEffect.lerpType = LerpTypes.EASE_OUT;
		scaleEffect.startScale = 0.75;
		scaleEffect.endScale = 0;
		effects.push( scaleEffect );
		
		moveFX = new MoveOverTimeFX();
		moveFX.acceleration = 10;
		effects.push( moveFX );
	}
	
	private function setX( value:Float ):Float
	{
		centerX = value;
		
		moveFX.targetX = centerX;
		
		return value;
	}
	
	private function setY( value:Float ):Float
	{
		centerY = value;
		
		moveFX.targetY = centerY;
		
		return value;
	}
}