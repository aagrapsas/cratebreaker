package game.fx;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import engine.misc.LerpTypes;
import engine.render.particles.fx.FadeOverTimeFX;
import engine.render.particles.fx.ScaleOverTimeFX;
import engine.render.particles.fx.SetImageFX;
import engine.render.particles.fx.SetLifeTimeFX;
import engine.render.particles.ParticleEmitter;
import game.config.SortOrders;

/**
 * ...
 * @author A.A. Grapsas
 */

class CrateChainFX extends ParticleEmitter
{	
	private var assetName:String = "chain";
	
	public function new( assetName:String = "chain" ) 
	{
		this.assetName = assetName;
		
		super();
		setup();
	}
	
	private function setup():Void
	{
		var asset:GameBitmap = AssetManager.PTR.getGameBitmap( assetName );
		
		maxParticles = 1;
		maxLifeTime = 0.2;
		
		var imageInit:SetImageFX = new SetImageFX();
		imageInit.sortOrder = SortOrders.UI_FRONT;
		imageInit.images.push( asset );
		initializers.push( imageInit );
		
		var lifeTimeFX:SetLifeTimeFX = new SetLifeTimeFX();
		lifeTimeFX.lifeTimeSeconds = 0.2;
		initializers.push( lifeTimeFX );
		
		var fadeEffect:FadeOverTimeFX = new FadeOverTimeFX();
		fadeEffect.duration = 0.2;
		fadeEffect.startAlpha = 0.8;
		fadeEffect.endAlpha = 0;
		fadeEffect.lerpType = LerpTypes.EASE_OUT;
		effects.push( fadeEffect );
		
		var scaleEffect:ScaleOverTimeFX = new ScaleOverTimeFX();
		scaleEffect.duration = 0.20;
		scaleEffect.lerpType = LerpTypes.EASE_OUT;
		scaleEffect.startScale = 1;
		scaleEffect.endScale = 0;
		effects.push( scaleEffect );
	}
}