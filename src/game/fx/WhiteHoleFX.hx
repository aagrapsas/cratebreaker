package game.fx;
import engine.assets.AssetManager;
import engine.misc.LerpTypes;
import engine.render.particles.fx.ExplodeFX;
import engine.render.particles.fx.FadeOverTimeFX;
import engine.render.particles.fx.ScaleOverTimeFX;
import engine.render.particles.fx.SetImageFX;
import engine.render.particles.fx.SetLifeTimeFX;
import engine.render.particles.ParticleEmitter;
import game.config.SortOrders;

/**
 * ...
 * @author A.A. Grapsas
 */

class WhiteHoleFX extends ParticleEmitter
{
	public function new() 
	{
		super();
		setup();
	}
	
	public function setup():Void
	{
		this.particlesPerSecond = 15;
		
		var imageInit:SetImageFX = new SetImageFX();
		imageInit.sortOrder = SortOrders.IN_GAME_FORE;
		imageInit.images.push( AssetManager.PTR.getGameBitmap( "whiteparticle" ) );
		initializers.push( imageInit );
		
		var lifeTimeFX:SetLifeTimeFX = new SetLifeTimeFX();
		lifeTimeFX.lifeTimeSeconds = 1;
		initializers.push( lifeTimeFX );
		
		var explosionFX:ExplodeFX = new ExplodeFX();
		explosionFX.magnitude = 100;
		initializers.push( explosionFX );
		
		var fadeEffect:FadeOverTimeFX = new FadeOverTimeFX();
		fadeEffect.duration = 1;
		fadeEffect.startAlpha = 0.8;
		fadeEffect.endAlpha = 0;
		fadeEffect.lerpType = LerpTypes.EASE_OUT;
		effects.push( fadeEffect );
		
		var scaleEffect:ScaleOverTimeFX = new ScaleOverTimeFX();
		scaleEffect.duration = 1;
		scaleEffect.lerpType = LerpTypes.EASE_OUT;
		scaleEffect.startScale = 0.75;
		scaleEffect.endScale = 0;
		effects.push( scaleEffect );
	}
}