package game.config;

/**
 * ...
 * @author A.A. Grapsas
 */
class SortOrders
{
	public static inline var NORMAL:Int = 0;
	public static inline var IN_GAME_TEXT:Int = 1;
	public static inline var IN_GAME_FORE:Int = 2;
	public static inline var UI_BACKING:Int = 3;
	public static inline var UI_FRONT:Int = 4;
}