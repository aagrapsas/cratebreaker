package game.config;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import engine.collision.CollisionFlags;
import engine.collision.RectCollider;
import engine.collision.SceneNode;
import engine.misc.GenericPool;
import engine.misc.IPoolable;
import engine.render.particles.Particle;

/**
 * ...
 * @author A.A. Grapsas
 */

class PoolManager 
{
	private var pools:Hash<GenericPool>;
	
	public function new() 
	{
		pools = new Hash<GenericPool>();
		
		setupPools();
	}
	
	public function setupPools():Void
	{
		setupParticlePool( "particles", 200, 100 );
	}
	
	private function setupParticlePool( name:String, max:Int, startCount:Int ):Void
	{
		var pool:GenericPool = new GenericPool( "particles", function():IPoolable
		{
			var particle:Particle = new Particle();
			return particle;
		}, max, startCount );
		
		pools.set( name, pool );
	}
	
	public function handleEndOfUpdate():Void
	{
		for ( poolKey in pools.keys() )
		{
			var pool:GenericPool = pools.get( poolKey );
			pool.handleEndOfUpdate();
		}
	}
	
	public function getPool( key:String ):GenericPool
	{
		return pools.get( key );
	}
}