package game.config;
import engine.assets.AssetManager;
import engine.assets.SpriteSheet;
import engine.render.tilestorage.SortedDynamicTileStorage;
import engine.render.tilestorage.SortedStaticTileStorage;
import nme.Assets;

/**
 * ...
 * @author A.A. Grapsas
 */

class AssetManifest 
{
	public function new() 
	{
		
	}
	
	public function load():Void
	{
		var spritesheets:Array<SpriteSheet> = new Array<SpriteSheet>();
		// Mainsheet contains ships and FX
		
		var layers:Array<Int> = new Array<Int>();
		layers.push( 200 );
		layers.push( 100 );
		layers.push( 500 );
		layers.push( 100 );
		layers.push( 100 );
		
		#if flash
		spritesheets.push( new SpriteSheet( "ingame", Assets.getText( "assets/config/ingame.json" ), Assets.getBitmapData( "assets/img/ingame.png" ), true, true, true, new SortedDynamicTileStorage() ) );
		#else
		spritesheets.push( new SpriteSheet( "ingame", Assets.getText( "assets/config/ingame.json" ), Assets.getBitmapData( "assets/img/ingame.png" ), true, true, true, new SortedStaticTileStorage( layers ) ) );
		#end
		
		AssetManager.PTR.setupManifest( spritesheets );
	}
}