package game.config;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.EventBus;
import engine.render.fonts.FontMap;
import engine.scene.SceneManager;
import game.CrateLevel;
import game.crates.CrateCatalog;
import game.systems.SlomoManager;
import game.user.achievements.AchievementManager;
import game.user.UserData;
import haxe.Json;
import nme.Assets;

/**
 * ...
 * @author A.A. Grapsas
 */

class GameConfig 
{
	public static var PTR:GameConfig;
	
	public var gameplayEvents:EventBus;
	
	public var lookUp:Hash<String>;
	
	public var miscData:Hash<Dynamic>;
	
	public var fontMaps:Hash<FontMap>;
	
	public var poolManager:PoolManager;
	
	public var slomoManager:SlomoManager;
	
	public var currentLevel:CrateLevel;
	
	public var crateCatalog:CrateCatalog;
	
	public var userData:UserData;
	public var achievements:AchievementManager;
	
	public function new() 
	{
		PTR = this;
		
		deserializeJson();
		
		gameplayEvents = new EventBus();
		
		fontMaps = new Hash<FontMap>();
		
		poolManager = new PoolManager();
		poolManager.setupPools();
		
		setupFontMaps();
		
		slomoManager = new SlomoManager();
		
		crateCatalog = new CrateCatalog();
		
		userData = UserData.load();
		
		achievements = new AchievementManager( userData );
	}
	
	private function setupFontMaps():Void
	{
		fontMaps.set( "num", new FontMap( "num", "0123456789x" ) );
	}
	
	private function deserializeJson():Void
	{
		var json:String = Assets.getText( "assets/config/config.json" );
		var jsonObj:Dynamic = Json.parse( json );
		var rawLookUp:Dynamic = jsonObj.lookup;
		
		lookUp = new Hash<String>();
		
		// Iterate all fields of the look up object and push their properties into
		// the publicly accessable, type safe look up
		for ( key in Reflect.fields( rawLookUp ) )
		{
			lookUp.set( key, Reflect.field( rawLookUp, key ) );
		}
		
		miscData = new Hash<Dynamic>();
		
		//SceneManager.PTR.slomoValue = cast lookUp.get( "slomo" );
	}
}