package game;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import engine.collision.CollisionFlags;
import engine.collision.RectCollider;
import engine.collision.SceneNode;
import engine.scene.Actor;
import engine.scene.SceneManager;
import game.config.SortOrders;
import game.crates.Crate;
import game.crates.handlers.ExplodingCrateTouchHandler;
import game.gems.Gem;

/**
 * ...
 * @author A.A. Grapsas
 */

class ActorFactory 
{
	private static var gems:Array<String> = [ "redgem", "greengem", "gem" ];
	
	private static var particles:Array<String> = [ "redparticle", "greenparticle", "purpleparticle" ];
	
	public function new() 
	{
		
	}
	
	public static function spawnCrate( asset:String, x:Float, y:Float ):Crate
	{
		var crate:Crate = new Crate();
		
		setupInternal( crate, asset );
		
		crate.x = x;
		crate.y = y;
		
		return crate;
	}
	
	private static function setupInternal( actor:Actor, asset:String ):Void
	{
		var bitmap:GameBitmap = AssetManager.PTR.getGameBitmap( asset );
		actor.tile = bitmap.getTile();
		actor.bitmap = bitmap;
		
		actor.sceneNode = new SceneNode();
		actor.sceneNode.internalObject = actor;
		actor.sceneNode.collisionComponent = new RectCollider();
		actor.sceneNode.collisionComponent.width = bitmap.originalWidth;
		actor.sceneNode.collisionComponent.height = bitmap.originalHeight;
		actor.sceneNode.collisionComponent.collisionFlag = CollisionFlags.ACTORS;
	}
	
	public static function spawnRandomGem( x:Float, y:Float ):Gem
	{		
		var gem:Gem = new Gem();
		
		var index:Int = Math.floor( Math.random() *  gems.length );
		
		var asset:String = gems[ index ];
		var particle:String = particles[ index ];
		
		setupInternal( gem, asset );
		
		gem.tile.sort = SortOrders.IN_GAME_FORE;
		
		gem.x = x;
		gem.y = y;
		
		gem.explosionAsset = particle;
		
		SceneManager.PTR.addActor( gem );
		SceneManager.PTR.addTickable( gem );
		
		return gem;
	}
	
	public static function spawnGem( asset:String, x:Float, y:Float ):Gem
	{
		var index:Int = 0;
		
		// Simple search
		for ( color in gems )
		{
			if ( color == asset )
			{
				break;
			}
			
			index++;
		}
		
		var particleAsset:String = particles[ index ];
		
		var gem:Gem = new Gem();
		
		setupInternal( gem, asset );
		
		gem.x = x;
		gem.y = y;
		
		gem.explosionAsset = particleAsset;
		
		SceneManager.PTR.addActor( gem );
		SceneManager.PTR.addTickable( gem );
		
		return gem;
	}
}