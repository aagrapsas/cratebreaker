package game;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import engine.misc.PersistentData;
import engine.render.particles.InWorldFXPlayer;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.config.AssetManifest;
import game.config.GameConfig;
import game.crates.Crate;
import game.crates.CrateFactory;
import game.crates.CrateSpawner;
import game.ui.nongame.UIMainMenu;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.Lib;
import nme.ui.Keyboard;

/**
 * ...
 * @author A.A. Grapsas
 */

class CrateGame extends Sprite
{
	private var assetManifest:AssetManifest;
	
	public function new() 
	{
		super();
		
		this.addEventListener( Event.ADDED_TO_STAGE, onAdded );
	}	
	
	private function onAdded( e: Event ):Void
	{
		this.removeEventListener( Event.ADDED_TO_STAGE, onAdded );
		
		new ResolutionConfig();
		
		var stageWidth:Float = Lib.current.stage.stageWidth;
		var stageHeight:Float = Lib.current.stage.stageHeight;
		ResolutionConfig.PTR.setup( 720, 1280, stageWidth, stageHeight );
		
		new AssetManager();
		assetManifest = new AssetManifest();
		assetManifest.load();
		
		new SceneManager();
		SceneManager.PTR.initializeCollision( Std.int( ResolutionConfig.PTR.actualScreenWidth * 1.25 ), Std.int( ResolutionConfig.PTR.actualScreenHeight * 1.25 ), 25 );
		this.addChild( SceneManager.PTR.display );
		
		new InWorldFXPlayer();
		
		new PersistentData( "mythcb" );
		
		new GameConfig();
		
		setupBG();
		setupFG();
		//test();
		//cheats();
	}
	
	private function setupBG():Void
	{
		var bg:Bitmap = AssetManager.PTR.getBitmap( "background" );
		SceneManager.PTR.backgroundLayer.addChild( bg );	
	}
	
	private function setupFG():Void
	{
		var menu:UIMainMenu = new UIMainMenu();
		SceneManager.PTR.addTickable( menu );
	}
	
	private function test():Void
	{
		GameConfig.PTR.currentLevel = new CrateLevel();
		GameConfig.PTR.currentLevel.setAllowedCrates( GameConfig.PTR.crateCatalog.getAllCrates() );
	}
	
	private function cheats():Void
	{
		this.stage.addEventListener( KeyboardEvent.KEY_DOWN, onKey );
	}
	
	private function onKey( e:KeyboardEvent ):Void
	{
		var crate:Crate = null;
		
		if (GameConfig.PTR.currentLevel == null) return;
		
		if ( e.keyCode == Keyboard.M )
		{
			var type:String = CrateFactory.MYSTERY_CRATE;
			crate = CrateFactory.getCrate( type, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		else if ( e.keyCode == Keyboard.G )
		{
			crate = CrateFactory.getCrate( CrateFactory.GEM_CRATE, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		else if ( e.keyCode == Keyboard.L )
		{
			crate = CrateFactory.getCrate( CrateFactory.LOCK_CRATE, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		else if ( e.keyCode == Keyboard.E )
		{
			crate = CrateFactory.getCrate( CrateFactory.EXPLODE_CRATE, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		else if ( e.keyCode == Keyboard.B )
		{
			crate = CrateFactory.getCrate( CrateFactory.BLACK_HOLE_CRATE, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		else if ( e.keyCode == Keyboard.T )
		{
			crate = CrateFactory.getCrate( CrateFactory.TIME_CRATE, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		else if ( e.keyCode == Keyboard.C )
		{
			crate = CrateFactory.getCrate( CrateFactory.BORING_CRATE, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		else if ( e.keyCode == Keyboard.W )
		{
			crate = CrateFactory.getCrate( CrateFactory.WHITE_HOLE_CRATE, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		else if ( e.keyCode == Keyboard.END )
		{
			GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.GAME_END, null );
		}
		else if ( e.keyCode == Keyboard.D )
		{
			crate = CrateFactory.getCrate( CrateFactory.DEATH_CRATE, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		else if ( e.keyCode == Keyboard.I )
		{
			crate = CrateFactory.getCrate( CrateFactory.MINING_CRATE, ResolutionConfig.PTR.expectedScreenWidth / 2, 0 );
		}
		
		if ( crate != null )
		{
			GameConfig.PTR.currentLevel.getSpawner().addCrate( crate );
		}
	}
}