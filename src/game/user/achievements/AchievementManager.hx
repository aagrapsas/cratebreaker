package game.user.achievements;
import engine.misc.EventBus;
import game.config.GameConfig;
import game.user.SessionStats;
import game.user.UserData;

/**
 * ...
 * @author A.A. Grapsas
 */

class AchievementManager 
{	
	private var userData:UserData;
	
	private var sessionAchievements:Array<Achievement>;
	private var globalAchievements:Array<Achievement>;
	
	private var completedSessionAchievements:Array<Achievement>;
	private var completedGlobalAchievements:Array<Achievement>;
	
	public function new( userData:UserData ) 
	{
		this.userData = userData;
		
		sessionAchievements = new Array<Achievement>();
		globalAchievements = new Array<Achievement>();
		
		completedSessionAchievements = new Array<Achievement>();
		completedGlobalAchievements = new Array<Achievement>();
		
		setupAchievements();		
		handleCompleted();
	}
	
	private function setupAchievements():Void
	{
		// Manually fill in achievements here
	}
	
	private function handleCompleted():Void
	{
		var completed:Array<String> = userData.completedAchievements;
		
		var fastLookUp:Hash<Bool> = new Hash<Bool>();
		
		for ( complete in completed )
		{
			fastLookUp.set( complete, true );
		}
		
		for ( sessionAchievement in sessionAchievements )
		{
			if ( fastLookUp.exists( sessionAchievement.name ) )
			{
				completedSessionAchievements.push( sessionAchievement );
			}
		}
		
		for ( globalAchievement in globalAchievements )
		{
			if ( fastLookUp.exists( globalAchievement.name ) )
			{
				completedGlobalAchievements.push( globalAchievement );
			}
		}
		
		if ( completedSessionAchievements.length > 0 )
		{
			for ( finishedSession in completedSessionAchievements )
			{
				sessionAchievements.remove( finishedSession );
			}
			
			completedSessionAchievements.splice( 0, completedSessionAchievements.length );
		}
		
		if ( completedGlobalAchievements.length > 0 )
		{
			for ( finishedGlobal in completedGlobalAchievements )
			{
				globalAchievements.remove( finishedGlobal );
			}
			
			completedGlobalAchievements.splice( 0, completedGlobalAchievements.length );
		}
	}
	
	public function evaluate( session:SessionStats ):Void
	{
		// Dispatch achievement unlocked if any unlocked
		if ( session != null )
		{			
			for ( sessionAchievement in sessionAchievements )
			{
				sessionAchievement.evaluateSession( session );
				
				if ( sessionAchievement.isComplete )
				{
					completedSessionAchievements.push( sessionAchievement );
					GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.ACHIEVEMENT_UNLOCKED, sessionAchievement );
				}
			}
		}
		
		for ( globalAchievement in globalAchievements )
		{
			globalAchievement.evaluateLifetime( userData.records );
			
			if ( globalAchievement.isComplete )
			{
				completedGlobalAchievements.push( globalAchievement );
				GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.ACHIEVEMENT_UNLOCKED, globalAchievement );
			}
		}
		
		if ( completedSessionAchievements.length > 0 )
		{
			for ( finishedSession in completedSessionAchievements )
			{
				sessionAchievements.remove( finishedSession );
			}
			
			completedSessionAchievements.splice( 0, completedSessionAchievements.length );
		}
		
		if ( completedGlobalAchievements.length > 0 )
		{
			for ( finishedGlobal in completedGlobalAchievements )
			{
				globalAchievements.remove( finishedGlobal );
			}
			
			completedGlobalAchievements.splice( 0, completedGlobalAchievements.length );
		}
	}
}