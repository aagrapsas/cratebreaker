package game.user.achievements;
import game.user.LifetimeStats;
import game.user.SessionStats;

/**
 * ...
 * @author A.A. Grapsas
 */

class Achievement 
{
	public var isComplete:Bool = false;
	
	public var name:String;
	
	public function new() 
	{
		
	}
	
	public function evaluateLifetime( stats:LifetimeStats ):Void
	{
		
	}
	
	public function evaluateSession( stats:SessionStats ):Void
	{
		
	}
}