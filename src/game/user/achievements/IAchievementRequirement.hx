package game.user.achievements;

/**
 * ...
 * @author A.A. Grapsas
 */

interface IAchievementRequirement 
{
	public function getDescriptor():String;
	public function getProgressText():String;
	public function getCompleteText():String;
}