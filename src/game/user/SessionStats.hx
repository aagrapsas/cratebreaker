package game.user;
import engine.scene.SceneManager;
import game.config.GameConfig;
import nme.Lib;

/**
 * ...
 * @author A.A. Grapsas
 */

class SessionStats 
{
	public var cratesSmashed:Int = 0;
	public var cratesSmashedByType:Hash<Int>; // NOT IMPLEMENTED YET?
	public var sessionLength:Float = 0;
	public var longestSmashChain:Int = 0;
	
	public var purpleGemsCollected:Int = 0;
	public var greenGemsCollected:Int = 0;
	public var redGemsCollected:Int = 0;
	
	private var startTime:Float = 0;
	
	public function new() 
	{
		setup();
		
		startTime = SceneManager.PTR.engineElapsedTime;
	}
	
	public function reset():Void
	{
		purpleGemsCollected = 0;
		greenGemsCollected = 0;
		redGemsCollected = 0;
		cratesSmashed = 0;
		longestSmashChain = 0;
	}
	
	public function setup():Void
	{
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.CRATE_SMASHED, onCrateSmashed );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.GREEN_GEM_OBTAINED, onGreenGem );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.RED_GEM_OBTAINED, onRedGem );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.PURPLE_GEM_OBTAINED, onPurpleGem );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.CHAIN_LENGTH, onChainLength );
	}
	
	private function breakDown():Void
	{
		GameConfig.PTR.gameplayEvents.unregister( GameEventTypes.CRATE_SMASHED, onCrateSmashed );
		GameConfig.PTR.gameplayEvents.unregister( GameEventTypes.GREEN_GEM_OBTAINED, onGreenGem );
		GameConfig.PTR.gameplayEvents.unregister( GameEventTypes.RED_GEM_OBTAINED, onRedGem );
		GameConfig.PTR.gameplayEvents.unregister( GameEventTypes.PURPLE_GEM_OBTAINED, onPurpleGem );
		GameConfig.PTR.gameplayEvents.unregister( GameEventTypes.CHAIN_LENGTH, onChainLength );
	}
	
	private function onCrateSmashed( data:Dynamic ):Void
	{
		cratesSmashed++;
	}
	
	private function onChainLength( data:Dynamic ):Void
	{
		var length:Int = cast data;
		
		if ( length > longestSmashChain )
		{
			longestSmashChain = length;
		}
	}
	
	private function onGameEnd( data:Dynamic ):Void
	{
		sessionLength = SceneManager.PTR.engineElapsedTime - startTime;
	}
	
	private function onGreenGem( data:Dynamic ):Void
	{
		greenGemsCollected++;
	}
	
	private function onPurpleGem( data:Dynamic ):Void
	{
		purpleGemsCollected++;
	}
	
	private function onRedGem( data:Dynamic ):Void
	{
		redGemsCollected++;
	}
	
	public function destroy():Void
	{
		breakDown();
	}
}