package game.user;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.PersistentData;
import haxe.Json;

/**
 * ...
 * @author A.A. Grapsas
 */

class UserData 
{
	// Resources
	public var purpleGems:Int = 0;
	public var greenGems:Int = 0;
	public var redGems:Int = 0;
	
	public var records:LifetimeStats;
	
	public var unlockedSets:Array<String>;
	
	public var completedAchievements:Array<String>;
	public var happyCrates:Int = 0;
	
	public function new() 
	{
		unlockedSets = new Array<String>();
		completedAchievements = new Array<String>();
		records = new LifetimeStats();
	}
	
	public function save():Void
	{
		var dataObj:Dynamic = serialize();
		var dataStr:String = Json.stringify( dataObj );
		
		PersistentData.PTR.set( "gamedata", dataStr );
		
		// Debug.log( DebugChannels.IO, "Saved user data: " + dataStr );
	}
	
	public function serialize():Dynamic
	{
		var obj:Dynamic = { "purpleGems" : purpleGems, "greenGems" : greenGems, "redGems" : redGems };
		
		obj.records = records.serialize();
		obj.unlockedSets = unlockedSets;
		
		obj.completedAchievements = cast completedAchievements;
		obj.happyCrates = happyCrates;
		
		return obj;
	}
	
	public function deserialize( json:Dynamic ):Void
	{
		purpleGems = json.purpleGems;
		greenGems = json.greenGems;
		redGems = json.redGems;
		unlockedSets = cast json.unlockedSets;
		
		completedAchievements = cast json.completedAchievements;
		happyCrates = json.happyCrates;
		
		records.deserialize( json.records );
		
		// Automatically unlock some crates for testing
		if ( unlockedSets.length == 0 )
		{
			unlockedSets.push( "explosive" );
			unlockedSets.push( "morph" );
		}
	}
	
	public static function load():UserData
	{
		var data:UserData = new UserData();
		
		if ( PersistentData.PTR.has( "gamedata" ) )
		{
			var json:Dynamic = Json.parse( PersistentData.PTR.get( "gamedata" ) );
			
			data.deserialize( json );
			
			// Debug.log( DebugChannels.IO, "Loaded user data: " + PersistentData.PTR.get( "gamedata" ) );
		}
		else
		{
			// Automatically unlock some crates for testing
			if ( data.unlockedSets.length == 0 )
			{
				data.unlockedSets.push( "explosive" );
				data.unlockedSets.push( "morph" );
			}
		}
		
		return data;
	}
	
	public function mergeSessionData( session:SessionStats ):Void
	{
		purpleGems += session.purpleGemsCollected;
		greenGems += session.greenGemsCollected;
		redGems += session.redGemsCollected;
		
		if ( session.sessionLength > records.longestSession )
		{
			records.longestSession = session.sessionLength;
		}
		else if ( records.shortestSession == 0 || session.sessionLength < records.shortestSession )
		{
			records.shortestSession = session.sessionLength;
		}
		
		// @TODO: calculate average session length using running average (weighted?)
	}
}