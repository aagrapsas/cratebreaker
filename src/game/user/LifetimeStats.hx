package game.user;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import game.config.GameConfig;
import game.crates.Crate;
import haxe.Json;

/**
 * ...
 * @author A.A. Grapsas
 */

class LifetimeStats 
{
	public var purpleGemsCollected:Int = 0;
	public var greenGemsCollected:Int = 0;
	public var redGemsCollected:Int = 0;
	
	public var sessionsPlayed:Int = 0;
	public var sessionsFinished:Int = 0;
	public var cratesBusted:Int = 0;
	
	public var longestSmashChain:Int = 0;
	public var cratesSmashedByType:Hash<Int>;
	
	public var longestSession:Float = 0;
	public var shortestSession:Float = 0;
	public var averageSession:Float = 0;
	
	public function new() 
	{
		cratesSmashedByType = new Hash<Int>();
		
		setup();
	}
	
	public function deserialize( json:Dynamic ):Void
	{
		purpleGemsCollected = json.purpleGemsCollected;
		greenGemsCollected = json.greenGemsCollected;
		redGemsCollected = json.redGemsCollected;
		sessionsPlayed = json.sessionsPlayed;
		sessionsFinished = json.sessionsFinished;
		cratesBusted = json.cratesBusted;
		longestSmashChain = json.longestSmashChain;
		
		var cratesSmashedObj:Dynamic = json.cratesSmashedByType;
		
		var cratesSmashedFields:Array<String> = Reflect.fields( cratesSmashedObj );
		
		for ( key in cratesSmashedFields )
		{
			cratesSmashedByType.set( key, Reflect.field( cratesSmashedObj, key ) );
		}
		
		longestSession = json.longestSession;
		shortestSession = json.shortestSession;
		averageSession = json.averageSession;
	}
	
	public function serialize():Dynamic
	{
		var obj:Dynamic = { };
		obj.purpleGemsCollected = purpleGemsCollected;
		obj.greenGemsCollected = greenGemsCollected;
		obj.redGemsCollected = redGemsCollected;
		obj.sessionsPlayed = sessionsPlayed;
		obj.sessionsFinished = sessionsFinished;
		obj.cratesBusted = cratesBusted;
		obj.longestSmashChain = longestSmashChain;
		
		var cratesSmashedObj:Dynamic = { };
		for ( key in cratesSmashedByType.keys() )
		{
			Reflect.setField( cratesSmashedObj, key, cratesSmashedByType.get( key ) );
		}
		
		obj.cratesSmashedByType = cratesSmashedObj;
		
		obj.longestSession = longestSession;
		obj.shortestSession = shortestSession;
		obj.averageSession = averageSession;
		
		return obj;
	}
	
	private function setup():Void
	{
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.CRATE_SMASHED, onCrateSmashed );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.CHAIN_LENGTH, onChainLength );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.CRATE_CHAIN_DESTROYED, onCrateDestroyedInChain );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.CRATE_CLICK_DESTROYED, onCrateClicked );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.CRATE_EXTERNALLY_DESTROYED, onCrateExternallyDestroyed );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.GAME_END, onGameEnd );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.GAME_START, onGameStart );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.GAME_CANCELED, onGameCanceled );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.GREEN_GEM_OBTAINED, onGreenGem );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.RED_GEM_OBTAINED, onRedGem );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.PURPLE_GEM_OBTAINED, onPurpleGem );
	}
	
	private function onCrateSmashed( data:Dynamic ):Void
	{
		var crate:Crate = cast data;
		
		cratesBusted++;
		
		if ( cratesSmashedByType.exists( crate.crateType ) == false )
		{
			cratesSmashedByType.set( crate.crateType, 0 );
		}
		
		var value:Int = cratesSmashedByType.get( crate.crateType );
		value++;
		cratesSmashedByType.set( crate.crateType, value );
	}
	
	private function onChainLength( data:Dynamic ):Void
	{
		var length:Int = cast data;
		
		if ( length > longestSmashChain )
		{
			longestSmashChain = length;
		}
	}
	
	private function onCrateDestroyedInChain( data:Dynamic ):Void
	{
		
	}
	
	private function onCrateClicked( data:Dynamic ):Void
	{
		
	}
	
	private function onCrateExternallyDestroyed( data:Dynamic ):Void
	{
		
	}
	
	private function onGameEnd( data:Dynamic ):Void
	{
		sessionsFinished++;
	}
	
	private function onGameStart( data:Dynamic ):Void
	{
		sessionsPlayed++;
	}
	
	private function onGameCanceled( data:Dynamic ):Void
	{
		
	}
	
	private function onGreenGem( data:Dynamic ):Void
	{
		greenGemsCollected++;
	}
	
	private function onPurpleGem( data:Dynamic ):Void
	{
		purpleGemsCollected++;
	}
	
	private function onRedGem( data:Dynamic ):Void
	{
		redGemsCollected++;
	}
}