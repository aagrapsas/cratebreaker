package game.systems;
import engine.misc.ITickable;
import engine.misc.LerpTypes;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.ui.elements.AchievementToast;
import game.ui.transitions.MoveToFX;
import game.ui.transitions.TimeFX;
import game.ui.transitions.TransitionPlayer;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class AchievementManager implements ITickable {

	
	public static inline var ACHIEVEMENT_HEIGHT:Float = 50;
	public static inline var FLY_IN_TIME:Float = 2;
	public static inline var FLY_OUT_TIME:Float = 1;
	public static inline var TOAST_WAIT_TIME:Float = 2;
	
	public static var PTR:AchievementManager;
	
	public static var currentToast:AchievementToast;
	
	var toastQueue:Array<AchievementToast>;
	var transitionPlayer:TransitionPlayer;
	
	var isAnimating:Bool;
	
	public function new()
	{
		PTR = this;
		
		setup();
	}
	
	public function setup()
	{
		toastQueue = new Array<AchievementToast>();
		transitionPlayer = new TransitionPlayer();
		isAnimating = false;
	}
	
	public function postAchievement( iconFileName:String, message:String ):Void
	{
		var newToast:AchievementToast = new AchievementToast( ResolutionConfig.PTR.actualScreenWidth, ACHIEVEMENT_HEIGHT, iconFileName, message );
		toastQueue.push(newToast);
	}
	
	private function slideInOver():Void
	{
		var toastWait:TimeFX = new TimeFX( TOAST_WAIT_TIME );
		transitionPlayer.addTransition( toastWait );
		transitionPlayer.doneCallback = toastWaitOver;
	}
	
	private function toastWaitOver():Void
	{
		var toastMoveOut:MoveToFX = new MoveToFX( currentToast, 0, 0, 0, -currentToast.height, FLY_OUT_TIME, LerpTypes.EASE_OUT );
		transitionPlayer.addTransition( toastMoveOut );
		transitionPlayer.doneCallback = slideOutOver;
	}
	
	private function slideOutOver():Void
	{
		if ( currentToast != null )
		{
			SceneManager.PTR.uiLayer.removeChild( currentToast );
			currentToast.destroy();
			currentToast = null;
		}
		
		isAnimating = false;
	}
	
	/* INTERFACE engine.misc.ITickable */
	
	public function tick( deltaTime:Float ):Void
	{
		
		transitionPlayer.tick( deltaTime );
		
		//If the transition player is completely done
		if ( !isAnimating )
		{
			if ( toastQueue.length > 0 )
			{
				isAnimating = true;
				
				//Chose a new toast. Play a sound or something here.
				currentToast = toastQueue.shift();
				
				SceneManager.PTR.uiLayer.addChild( currentToast );
				
				var toastMoveIn:MoveToFX = new MoveToFX( currentToast, ResolutionConfig.PTR.actualScreenWidth + 50, 0, 0, 0, FLY_IN_TIME, LerpTypes.EASE_OUT );
				transitionPlayer.addTransition( toastMoveIn );
				transitionPlayer.doneCallback = slideInOver;
			}
		}
	}
	
}