package game.systems;
import engine.assets.AssetManager;
import engine.collision.SceneNode;
import engine.misc.ITickable;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.CrateLevel;
import game.crates.Crate;
import game.crates.CrateSpawner;

/**
 * ...
 * @author A.A. Grapsas
 */

class GameEndConditions implements ITickable
{
	
	//Screen information
	private var width:Float = 0;
	private var height:Float = 0;
	
	//Crate information
	private var crateWidth:Float = 0;
	private var crateHeight:Float = 0;
	
	private var hasEndedGame:Bool = false;
	
	//Some end game constants
	static inline var MAX_PER_COLUMN:Float = 9;
	static inline var PADDING:Float = 2;
	
	public function new() 
	{
		setup();
	}
	
	private function setup():Void
	{
		width = ResolutionConfig.PTR.expectedScreenWidth;
		height = ResolutionConfig.PTR.expectedScreenHeight;
		
		crateWidth = AssetManager.PTR.getGameBitmap( "crate" ).originalWidth;
		crateHeight = AssetManager.PTR.getGameBitmap( "crate" ).originalHeight;
	}
	
	public function reset():Void
	{
		hasEndedGame = false;
	}
	
	public function stop():Void
	{
		hasEndedGame = true;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( hasEndedGame )
		{
			return;
		}
		
		var crateSpawner:CrateSpawner = GameConfig.PTR.currentLevel.getSpawner();
		
		for ( lane in crateSpawner.allowedLanes )
		{
			var laneContents:Array<SceneNode> = SceneManager.PTR.getCollidersWorldSpace(lane, height / 2, 5, height );
			var laneSize:Int = 0;
			
			for ( node in laneContents )
			{
				if ( Std.is( node.internalObject, Crate ) )
				{
					var asCrate:Crate = cast node.internalObject;
					laneSize++;
				}
			}
			
			if ( laneSize >= MAX_PER_COLUMN )
			{
				// END GAME
				hasEndedGame = true;
				GameConfig.PTR.gameplayEvents.dispatch( GameEventTypes.GAME_END, "stack" );
				break;
			}
		}
		
	}
}