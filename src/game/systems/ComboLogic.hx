package game.systems;
import engine.misc.ITickable;
import game.config.GameConfig;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class ComboLogic implements ITickable
{

	public static inline var TIME_TO_ROW:Int = 10000;
	public static inline var MIN_CHAIN_REQUIRED:Int = 3;
	
	public var shouldSpawnRow:Bool = false;
	
	public var timeSinceLastCombo:Int = 0;
	
	public function new()
	{
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.CHAIN_LENGTH, onChainLength );
		reset();
	}
	
	public function reset():Void
	{
		timeSinceLastCombo = 0;
		shouldSpawnRow = false;
	}
	
	public function onChainLength( chainLength:Dynamic ):Void
	{
		if ( chainLength >= MIN_CHAIN_REQUIRED )
		{
			reset();
		}
	}
	
	public function tick( deltaTime:Float ):Void
	{
		
		timeSinceLastCombo += Std.int( deltaTime * 1000 );
		
		if ( timeSinceLastCombo >= TIME_TO_ROW )
		{
			shouldSpawnRow = true;
		}
	}
	
}