package game.systems;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.ITickable;
import engine.misc.MathUtility;
import engine.scene.SceneManager;

/**
 * ...
 * @author A.A. Grapsas
 */

class SlomoManager implements ITickable
{
	private var slomoDuration:Float = 0;
	private var elapsed:Float = 0;
	private var speed:Float = 1;
	
	private var isActive:Bool = false;
	private var isTransitioning:Bool = false;
	private var isTransitioningIn:Bool = false;
	
	private var transitionIn:Float = 0;
	private var nonTransitionDuration:Float = 0;
	private var transitionOut:Float = 0;
	
	private static inline var TRANSITION_PERCENTAGE:Float = 0.015;
	
	public function new() 
	{
		SceneManager.PTR.addTickable( this );
	}	
	
	public function slomo( duration:Float, speed:Float ):Void
	{		
		// We aren't allowing stacking slomo, at the moment
		if ( isActive )
		{
			return;
		}
		
		this.slomoDuration = duration;
		this.speed = speed;
		
		var halfTransition:Float = duration * ( TRANSITION_PERCENTAGE / 2 );
		transitionIn = halfTransition * duration;
		transitionOut = transitionIn;
		nonTransitionDuration = duration - ( 2 * transitionIn );
		
		isActive = true;
		
		isTransitioning = true;
		isTransitioningIn = true;
	}
	
	public function reset():Void
	{
		isActive = false;
		SceneManager.PTR.slomoValue = 1;
	}
	
	public function tick( deltaTime:Float ):Void
	{		
		if ( isActive == false )
		{
			return;
		}
		
		var actualDT:Float = deltaTime / SceneManager.PTR.slomoValue;
		
		elapsed += actualDT;
		
		if ( isTransitioning )
		{
			if ( isTransitioningIn )
			{				
				var rawAlpha:Float = Math.min( elapsed / transitionIn, 1.0 );
				var alpha:Float = MathUtility.easeIn( 1.0, speed, rawAlpha );
				SceneManager.PTR.slomoValue = alpha;
				
				if ( rawAlpha == 1 )
				{
					isTransitioningIn = false;
					isTransitioning = false;
					elapsed = 0;
				}
			}
			else
			{
				var rawAlpha:Float = Math.min( elapsed / transitionOut, 1.0 );
				var alpha:Float = MathUtility.easeOut( speed, 1.0, rawAlpha );
				SceneManager.PTR.slomoValue = alpha;
				
				if ( rawAlpha == 1 )
				{
					isActive = false;
				}
			}
		}
		else
		{
			if ( elapsed >= nonTransitionDuration )
			{
				isTransitioning = true;
				isTransitioningIn = false;
				elapsed = 0;
			}
		}
	}
	
	public function destroy():Void
	{
		SceneManager.PTR.removeTickable( this );
	}
	
	public function getIsInSlomo():Bool { return isActive; }
}