package game.systems;
import engine.misc.ITickable;
import game.crates.Crate;
import game.crates.CrateFactory;
import nme.events.Event;
import nme.events.EventDispatcher;

/**
 * ...
 * @author A.A. Grapsas
 */

class SmileMaker extends EventDispatcher, implements ITickable
{
	private var fractionalTraversals:Float = 0;
	private var totalTimeToTake:Float = 1.5;
	private var traversalsPerSecond:Float = 0;
	
	private var lastUpdatedIndex:Int = 0;
	
	private var crates:Array<Crate>;
	
	private var isActive:Bool = true;
	
	private static var RANDOM_SMILES:Array<String> = [ CrateFactory.SMILE_CRATE, CrateFactory.SMILE_CRATE2, CrateFactory.SMILE_CRATE3, CrateFactory.SMILE_CRATE4 ];
	
	public function new( crates:Array<Crate> ) 
	{
		super();
		
		this.crates = crates;
		
		traversalsPerSecond = crates.length / totalTimeToTake;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( isActive == false )
		{
			return;
		}
		
		var fractionalTraversalsThisFrame:Float = traversalsPerSecond * deltaTime;
		var traversalsThisFrame:Int = cast fractionalTraversalsThisFrame;
		
		fractionalTraversals += ( fractionalTraversalsThisFrame - traversalsThisFrame );
		
		if ( fractionalTraversals >= 1 )
		{
			traversalsThisFrame += 1;
			fractionalTraversals -= 1;
		}
		
		traversalsThisFrame = cast Math.min( traversalsThisFrame, crates.length - lastUpdatedIndex );
		
		for ( i in 0...traversalsThisFrame )
		{
			var randomSmile:Int = cast Math.floor( Math.random() * RANDOM_SMILES.length );
			CrateFactory.switchCrateDisplay( crates[ lastUpdatedIndex ], RANDOM_SMILES[ randomSmile ] );
			
			// Break down logic (to kill particles, etc.)
			if ( crates[ lastUpdatedIndex ].logic != null )
			{
				crates[ lastUpdatedIndex ].logic.breakDown( crates[ lastUpdatedIndex ] );
			}
			
			lastUpdatedIndex++;
		}
		
		if ( lastUpdatedIndex >= crates.length )
		{
			isActive = false;
			this.dispatchEvent( new Event( Event.COMPLETE ) );
			return;
		}
	}
}