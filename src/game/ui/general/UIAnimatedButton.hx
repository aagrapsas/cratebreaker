package game.ui.general;
import engine.misc.ITickable;
import engine.misc.LerpTypes;
import engine.render.ResolutionConfig;
import game.ui.transitions.MoveToFX;
import nme.events.MouseEvent;

/**
 * ...
 * @author A.A. Grapsas
 */

class UIAnimatedButton extends UIButton, implements ITickable
{
	private var shouldAnimate:Bool = false;
	private var shouldAnimateOut:Bool = false;
	
	private var animDuration:Float = 0.15;
	private var movementDist:Float = 5;
	
	private var anim:MoveToFX;
	
	private var isEnabled:Bool = true;
	
	public function new( asset:String, clickHandler:Void -> Void ) 
	{
		super( asset, clickHandler );
		
		movementDist = ResolutionConfig.PTR.convertWorldToScreen( movementDist );
	}
	
	public function setIsEnabled( isEnabled:Bool ):Void
	{
		if ( this.isEnabled && isEnabled == false )
		{
			this.buttonMode = false;
		}
		else if ( this.isEnabled == false && isEnabled )
		{
			this.buttonMode = true;
		}
		
		this.isEnabled = isEnabled;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( shouldAnimate == false )
		{
			return;
		}
		
		anim.tick( deltaTime );
		
		if ( anim.getIsDone() )
		{
			if ( shouldAnimateOut )
			{
				shouldAnimateOut = false;
				anim = new MoveToFX( this, this.x, this.y, this.x - 5, this.y - 5, animDuration, LerpTypes.LINEAR );
			}
			else
			{
				shouldAnimate = false;
				shouldAnimateOut = false;
				
				if ( clickHandler != null )
				{
					clickHandler();
				}
			}
		}
	}
	
	override public function onClick( e:MouseEvent ):Void
	{
		if ( isEnabled == false )
		{
			return;
		}
		
		if ( anim != null && anim.getIsDone() == false )
		{
			return;
		}
		
		anim = new MoveToFX( this, this.x, this.y, this.x + 5, this.y + 5, animDuration, LerpTypes.LINEAR );
		
		shouldAnimate = true;
		shouldAnimateOut = true;
	}
}