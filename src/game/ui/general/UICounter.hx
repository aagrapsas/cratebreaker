package game.ui.general;
import engine.misc.ITickable;
import engine.render.fonts.DynamicText;

/**
 * ...
 * @author A.A. Grapsas
 */

class UICounter extends DynamicText, implements ITickable
{
	private var amountToAdd:Float = 0;
	private var displayedAmount:Int = 0;
	private var amountPerSecond:Float = 50;
	
	public function new() 
	{
		super();
	}
	
	public function setup():Void
	{
		this.setText( getPadded( 0 ) );
	}
	
	public function reset():Void
	{
		this.setText( getPadded( 0 ) );
	}
	
	public function add( amount:Int ):Void
	{
		amountToAdd += amount;
	}
	
	override public function tick( deltaTime:Float ):Void
	{
		if ( amountToAdd > 0 )
		{
			var incrementAmount:Int = Std.int( amountPerSecond * deltaTime );
			
			if ( incrementAmount > amountToAdd )
			{
				incrementAmount = Std.int( amountToAdd );
			}
			
			amountToAdd -= incrementAmount;
			
			displayedAmount += incrementAmount;
			
			this.setText( getPadded( displayedAmount ) );
		}
		
		super.tick( deltaTime );
	}
	
	private function getPadded( value:Int ):String
	{
		var padding:Int = 4;
		
		var converted:String = Std.string( value );
		
		var len:Int = converted.length;
		
		if ( len >= padding )
		{
				return converted;
		}
		
		var difference:Int = padding - len;
		
		for ( i in 0...difference )
		{
				converted = "0" + converted;
		}
		
		return converted;
	}
}