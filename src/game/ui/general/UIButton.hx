package game.ui.general;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import engine.scene.SceneManager;
import nme.display.Sprite;
import nme.events.MouseEvent;

/**
 * ...
 * @author A.A. Grapsas
 */

class UIButton extends Sprite
{	
	public var clickHandler:Void -> Void;
	
	public function new( asset:String, clickHandler:Void -> Void ) 
	{
		super();
		
		this.clickHandler = clickHandler;
		
		setup( asset );
	}	
	
	private function setup( asset:String ):Void
	{
		var gameBitmap:GameBitmap = AssetManager.PTR.getGameBitmap( asset );
		
		this.addChild( gameBitmap.getNewBitmap() );
		
		this.buttonMode = true;
		this.useHandCursor = true;
		
		this.addEventListener( MouseEvent.CLICK, onClick );
		
		SceneManager.PTR.uiLayer.addChild( this );
	}
	
	public function onClick( e:MouseEvent ):Void
	{
		if ( clickHandler != null )
		{
			clickHandler();
		}
	}
	
	public function destroy():Void
	{
		this.removeEventListener( MouseEvent.CLICK, onClick );
		
		clickHandler = null;
		
		SceneManager.PTR.uiLayer.removeChild( this );
	}
}