package game.ui.nongame.lab;

import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import engine.misc.BlitText;
import engine.misc.ITickable;
import engine.misc.LerpTypes;
import engine.misc.StringUtils;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.config.AssetManifest;
import game.config.GameConfig;
import game.purchases.PurchaseData;
import game.ui.general.UIAnimatedButton;
import game.ui.layout.GroupItem;
import game.ui.layout.Scrollbar;
import game.ui.layout.VGroup;
import game.ui.nongame.UIMainMenu;
import game.ui.transitions.FadeOutFX;
import game.ui.transitions.MoveToFX;
import game.ui.transitions.TransitionPlayer;
import game.ui.UIEventTypes;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.events.Event;

/**
 * ...
 * @author A.A. Grapsas
 */
class UICrateLab implements ITickable
{
	private var layer:Sprite;
	
	private var btnBack:UIAnimatedButton;
	
	// Navigation
	private var purchaseGroup:VGroup;
	
	private var displayed:Array<UILabPackage>;
	
	private var bottomVisibleIndex:Int = 0;
	private var visibleCount:Int = 0;
	
	private var transitions:TransitionPlayer;
	
	private var purpleGem:Bitmap;
	private var greenGem:Bitmap;
	private var redGem:Bitmap;
	
	private var purpleText:BlitText;
	private var greenText:BlitText;
	private var redText:BlitText;

	public function new() 
	{
		displayed = new Array<UILabPackage>();
		
		setup();
	}
	
	private function setup():Void
	{
		transitions = new TransitionPlayer();
		
		layer = new Sprite();
		SceneManager.PTR.uiLayer.addChild( layer );
		
		var unlockedCrates:Array<String> = GameConfig.PTR.userData.unlockedSets;
		var unlockedLookUp:Hash<String> = new Hash<String>();
		
		for ( unlockedSet in unlockedCrates )
		{
			unlockedLookUp.set( unlockedSet, unlockedSet );
		}
		
		var purchaseData:Array<PurchaseData> = GameConfig.PTR.crateCatalog.getAllPurchaseData();
		
		var unlockedData:Array<PurchaseData> = new Array<PurchaseData>();
		var lockedData:Array<PurchaseData> = new Array<PurchaseData>();
		
		// Determine what sets are unlocked and which are locked
		for ( data in purchaseData )
		{
			if ( data.isCrateSet )
			{
				if ( unlockedLookUp.exists( data.setKey ) )
				{
					unlockedData.push( data );
				}
				else
				{
					lockedData.push( data );
				}
			}
		}
		
		// For now, populate bottom up
		var bottom:Float = ResolutionConfig.PTR.actualScreenHeight;
		var leftOffset:Float = 0;
		
		// Back button
		btnBack = new UIAnimatedButton( "back", onBack );
		btnBack.x = leftOffset;
		btnBack.y = bottom - btnBack.height;
		
		//Purchase Items, arranged using a VGroup
		var scrollThumbBitmap:GameBitmap = AssetManager.PTR.getGameBitmap( "usethese" );
		var scrollThumb:Sprite = new Sprite();
		scrollThumb.addChild( scrollThumbBitmap.getNewBitmap() );
		
		var purchaseScrollBar:Scrollbar = new Scrollbar( scrollThumb );
		purchaseGroup = new VGroup( purchaseScrollBar );
		purchaseGroup.x = leftOffset;
		purchaseGroup.y = 0;
		purchaseGroup.setScrollRange( 0, 0, ResolutionConfig.PTR.actualScreenWidth, ResolutionConfig.PTR.actualScreenHeight );
		
		var itemRank:Int = 0;
		
		for ( unlocked in unlockedData )
		{
			var unlockedDisplay:UILabPackage = new UILabPackage( unlocked, true );
			purchaseGroup.addItem( unlockedDisplay, itemRank );
			itemRank++;
		}
		
		for ( locked in lockedData )
		{
			var lockedDisplay:UILabPackage = new UILabPackage( locked, true );
			purchaseGroup.addItem( lockedDisplay, itemRank );
			itemRank++;
		}
		
		purchaseGroup.doLayout();
		
		purchaseScrollBar.enabled = false;
		
		layer.addChild( purchaseGroup );
		layer.addChild( scrollThumb );
		
		// Setup gems
		purpleGem = AssetManager.PTR.getBitmap( "gem" );
		greenGem = AssetManager.PTR.getBitmap( "greengem" );
		redGem = AssetManager.PTR.getBitmap( "redgem" );
		
		var gemWidth:Float = redGem.width * 0.75;
		var gemHeight:Float = redGem.height * 0.75;
		
		purpleGem.width = gemWidth;
		purpleGem.height = gemHeight;
		
		greenGem.width = gemWidth;
		greenGem.height = gemHeight;
		
		redGem.width = gemWidth;
		redGem.height = gemHeight;
		
		purpleGem.x = ResolutionConfig.PTR.convertWorldToScreen( 16 );
		greenGem.x = purpleGem.x;
		redGem.x = greenGem.x;
		
		purpleGem.y = ResolutionConfig.PTR.convertWorldToScreen( 8 );
		greenGem.y = purpleGem.y + purpleGem.height;
		redGem.y = greenGem.y + greenGem.height;
		
		layer.addChild( purpleGem );
		layer.addChild( greenGem );
		layer.addChild( redGem );
		
		// Gem text
		purpleText = new BlitText();
		purpleText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		purpleText.setText( StringUtils.getPadded( GameConfig.PTR.userData.purpleGems, 6 ) );
		purpleText.x = purpleGem.x + purpleGem.width + ResolutionConfig.PTR.convertWorldToScreen( 16 );
		purpleText.y = purpleGem.y + ( purpleGem.height / 2 ) - ( purpleText.getHeight() / 2 );
		layer.addChild( purpleText );
		
		greenText = new BlitText();
		greenText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		greenText.setText( StringUtils.getPadded( GameConfig.PTR.userData.greenGems, 6 ) );
		greenText.x = greenGem.x + greenGem.width + ResolutionConfig.PTR.convertWorldToScreen( 16 );
		greenText.y = greenGem.y + ( greenGem.height / 2 ) - ( greenText.getHeight() / 2 );
		layer.addChild( greenText );
		
		redText = new BlitText();
		redText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		redText.setText( StringUtils.getPadded( GameConfig.PTR.userData.redGems, 6 ) );
		redText.x = redGem.x + redGem.width + ResolutionConfig.PTR.convertWorldToScreen( 16 );
		redText.y = redGem.y + ( redGem.height / 2 ) - ( redText.getHeight() / 2 );
		layer.addChild( redText );
		
		transitions.addTransition( new FadeOutFX( purpleGem, 0, 1, 0.25 ) );
		transitions.addTransition( new FadeOutFX( greenGem, 0, 1, 0.50 ) );
		transitions.addTransition( new FadeOutFX( redGem, 0, 1, 0.75 ) );
		
		transitions.addTransition( new FadeOutFX( purpleText, 0, 1, 0.35 ) );
		transitions.addTransition( new FadeOutFX( greenText, 0, 1, 0.65 ) );
		transitions.addTransition( new FadeOutFX( redText, 0, 1, 0.85 ) );
		
		transitions.addEventListener( Event.COMPLETE, onTransitionInDone );
		
		bottomVisibleIndex = 0;	// Bottom index starts as the first object in display
		
		GameConfig.PTR.gameplayEvents.register( UIEventTypes.BALANCE_UPDATED, updateBalance );
	}
	
	private function updateBalance( data:Dynamic ):Void
	{
		purpleText.setText( StringUtils.getPadded( GameConfig.PTR.userData.purpleGems, 6 ) );
		greenText.setText( StringUtils.getPadded( GameConfig.PTR.userData.greenGems, 6 ) );
		redText.setText( StringUtils.getPadded( GameConfig.PTR.userData.redGems, 6 ) );
	}
	
	private function onTransitionInDone( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onTransitionInDone );
	}
	
	private function onBack():Void
	{
		transitions.addTransition( new MoveToFX( btnBack, btnBack.x, btnBack.y, -btnBack.width, btnBack.y, 0.25, LerpTypes.EASE_OUT ) );
		
		purchaseGroup.scrollbar.destroy();
		
		transitions.addEventListener( Event.COMPLETE, onTransitionOutFirst );
	}
	
	private function onTransitionOutFirst( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onTransitionOutFirst );
		transitions.clear();
		
		var runningTime:Float = 0.25;
		
		transitions.addTransition( new FadeOutFX( purchaseGroup, 1, 0, 0.25 ) );
		
		transitions.addTransition( new FadeOutFX( purpleGem, 1, 0, 0.25 ) );
		transitions.addTransition( new FadeOutFX( greenGem, 1, 0, 0.50 ) );
		transitions.addTransition( new FadeOutFX( redGem, 1, 0, 0.75 ) );
		
		transitions.addTransition( new FadeOutFX( purpleText, 1, 0, 0.35 ) );
		transitions.addTransition( new FadeOutFX( greenText, 1, 0, 0.65 ) );
		transitions.addTransition( new FadeOutFX( redText, 1, 0, 0.85 ) );
		
		transitions.addEventListener( Event.COMPLETE, onTransitonOut );
	}
	
	private function onTransitonOut( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onTransitonOut );
		transitions.clear();
		
		destroy();
		
		SceneManager.PTR.removeTickable( this );
		
		SceneManager.PTR.addTickable( new UIMainMenu() );
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( btnBack != null )
		{
			btnBack.tick( deltaTime );
		}
		
		if ( transitions != null )
		{
			transitions.tick( deltaTime );
		}
		
		if ( purpleText != null )
		{
			purpleText.tick( deltaTime );
		}
		
		if ( greenText != null )
		{
			greenText.tick( deltaTime );
		}
		
		if ( redText != null )
		{
			redText.tick( deltaTime );
		}
		
		if (purchaseGroup != null)
		{
			purchaseGroup.tick( deltaTime );
		}
	}
	
	public function destroy():Void
	{
		SceneManager.PTR.uiLayer.removeChild( layer );
		
		GameConfig.PTR.gameplayEvents.unregister( UIEventTypes.BALANCE_UPDATED, updateBalance );
		
		for ( i in 0...displayed.length )
		{
			displayed[ i ].destroy();
		}
	}
}