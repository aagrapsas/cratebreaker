package game.ui.nongame.lab;
import engine.assets.AssetManager;
import engine.misc.BlitText;
import engine.misc.ITickable;
import engine.misc.LerpTypes;
import engine.misc.StringUtils;
import engine.render.ResolutionConfig;
import game.config.GameConfig;
import game.crates.CrateFactory;
import game.crates.CrateInfo;
import game.crates.CrateSet;
import game.purchases.Purchasable;
import game.purchases.PurchaseData;
import game.ui.general.UIAnimatedButton;
import game.ui.transitions.FadeOutFX;
import game.ui.transitions.MoveToFX;
import game.ui.transitions.TimeFX;
import game.ui.transitions.TransitionPlayer;
import game.ui.UIEventTypes;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.events.Event;

/**
 * ...
 * @author A.A. Grapsas
 */
class UILabPackage extends Sprite, implements ITickable
{
	private var data:PurchaseData;
	
	private var transitionsManager:TransitionPlayer;
	
	private var mainLayer:Sprite;
	private var notEnoughGemsLayer:Sprite;
	private var notEnoughGemsBitmap:Bitmap;
	
	private var purpleText:BlitText;
	private var greenText:BlitText;
	private var redText:BlitText;
	
	private var purpleGem:Bitmap;
	private var greenGem:Bitmap;
	private var redGem:Bitmap;
	
	private var backLayer:Sprite;
	
	private var sold:Bitmap;
	private var btnBuy:UIAnimatedButton;
	
	private var isSold:Bool = false;
	
	public function new( data:PurchaseData, isSold:Bool ) 
	{
		this.data = data;
		this.isSold = isSold;
		
		super();
		
		setup();
	}	
	
	private function setup():Void
	{
		transitionsManager = new TransitionPlayer();
		
		mainLayer = new Sprite();
		this.addChild( mainLayer );
		
		backLayer = new Sprite();
		mainLayer.addChild( backLayer );
		
		notEnoughGemsLayer = new Sprite();
		
		if ( data.isCrateSet )
		{
			setupCrates();
		}
	}
	
	private function setupCrates():Void
	{
		var setKey:String = data.setKey;
		var set:Array<CrateInfo> = GameConfig.PTR.crateCatalog.getListFromSet( setKey );
		
		var runningX:Float = ResolutionConfig.PTR.convertWorldToScreen( 32 );
		
		var crateWidth:Float = 0;
		
		for ( crate in set )
		{
			var bitmap:Bitmap = AssetManager.PTR.getBitmap( crate.uiAsset );
			mainLayer.addChild( bitmap );
			crateWidth = bitmap.width;
			bitmap.x = runningX;
			runningX += bitmap.width;
		}
		
		runningX = ( 2 * crateWidth ) + ResolutionConfig.PTR.convertWorldToScreen( 48 );
		
		var verticalSpacing:Float = ResolutionConfig.PTR.convertWorldToScreen( 4 );
		var postTextSpacing:Float = ResolutionConfig.PTR.convertWorldToScreen( 16 );
		
		redGem = AssetManager.PTR.getBitmap( "redgem" );
		var gemWidth:Float = redGem.width * 0.75;
		var gemHeight:Float = redGem.height * 0.75;
		
		var addedGems:Array<Bitmap> = new Array<Bitmap>();
		var addedText:Array<BlitText> = new Array<BlitText>();
		
		if ( data.purpleCost > 0 )
		{
			purpleGem = AssetManager.PTR.getBitmap( "gem" );
			purpleGem.width = gemWidth;
			purpleGem.height = gemHeight;
			mainLayer.addChild( purpleGem );
			
			purpleText = new BlitText();
			purpleText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
			purpleText.setText( StringUtils.getPadded( data.purpleCost, 3 ) );
			mainLayer.addChild( purpleText );
			
			addedGems.push( purpleGem );
			addedText.push( purpleText );
		}
		
		if ( data.greenCost > 0 )
		{
			greenGem = AssetManager.PTR.getBitmap( "greengem" );
			greenGem.width = gemWidth;
			greenGem.height = gemHeight;
			mainLayer.addChild( greenGem );
			
			greenText = new BlitText();
			greenText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
			greenText.setText( StringUtils.getPadded( data.greenCost, 3 ) );
			mainLayer.addChild( greenText );
			
			addedGems.push( greenGem );
			addedText.push( greenText );
		}
		
		if ( data.greenCost > 0 )
		{
			redGem.width = gemWidth;
			redGem.height = gemHeight;
			mainLayer.addChild( redGem );
			
			redText = new BlitText();
			redText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
			redText.setText( StringUtils.getPadded( data.greenCost, 3 ) );
			mainLayer.addChild( redText );
			
			addedGems.push( redGem );
			addedText.push( redText );
		}
		
		var combinedHeight:Float = 0;
		
		for ( gemAsset in addedGems )
		{
			combinedHeight += gemAsset.height;
		}
		
		var runningY:Float = ( this.height / 2 ) - ( combinedHeight / 2 );
		
		for ( i in 0...addedGems.length )
		{
			var gem:Bitmap = addedGems[ i ];
			var text:BlitText = addedText[ i ];
			
			gem.x = runningX;
			text.x = runningX + gem.width + postTextSpacing;
			
			gem.y = runningY;
			text.y = gem.y + ( gem.height / 2 ) - ( text.getHeight() / 2 );
			text.getWidth();	// Cache
			
			runningY += gem.height;
		}
	}
	
	public function showDisplay( isEven:Bool, shouldFade:Bool ):Void
	{
		var value:Float = isEven ? 0.5 : 0.75;
		
		backLayer.graphics.clear();
		backLayer.graphics.beginFill( 0, value );
		backLayer.graphics.drawRect( 0, 0, ResolutionConfig.PTR.actualScreenWidth, this.height );
		backLayer.graphics.endFill();
		
		if ( shouldFade )
		{
			transitionsManager.addTransition( new FadeOutFX( backLayer, 0, 1, 0.5 ) );
		}
		
		transitionsManager.addEventListener( Event.COMPLETE, onDisplayDone );
	}
	
	private function onDisplayDone( e:Event ):Void
	{
		transitionsManager.removeEventListener( Event.COMPLETE, onDisplayDone );
		
		setSoldStatus( isSold );
	}
	
	public function hideDisplay():Void
	{
		transitionsManager.addTransition( new FadeOutFX( backLayer, 1, 0, 0.5 ) );
		
		if ( sold != null )
		{
			transitionsManager.addTransition( new FadeOutFX( sold, 1, 0, 0.5 ) );
		}
		
		if ( btnBuy != null )
		{
			transitionsManager.addTransition( new FadeOutFX( btnBuy, 1, 0, 0.5 ) );
		}
	}
	
	public function setSoldStatus( isSold:Bool ):Void
	{		
		if ( sold != null )
		{
			this.removeChild( sold );
			sold = null;
		}
		
		if ( btnBuy != null )
		{
			btnBuy.destroy();
			btnBuy = null;
		}
		
		if ( isSold )
		{
			sold = AssetManager.PTR.getBitmap( "sold" );
			this.addChild( sold );
			sold.y = ( this.height / 2 ) - ( sold.height / 2 );
			sold.x = ResolutionConfig.PTR.actualScreenWidth - sold.width;
			
			transitionsManager.addTransition( new FadeOutFX( sold, 0, 1, 1 ) );
		}
		else
		{
			btnBuy = new UIAnimatedButton( "purchase", onPurchase );
			btnBuy.y = this.y + ( this.height / 2 ) - ( btnBuy.height / 2 );
			btnBuy.x = this.width - btnBuy.width;
			
			transitionsManager.addTransition( new FadeOutFX( btnBuy, 0, 1, 1 ) );
		}
	}
	
	private function onPurchase():Void
	{
		var purchase:Purchasable = new Purchasable();
		purchase.data = data;
		
		if ( purchase.canExecute() )
		{
			purchase.execute();
			
			setSoldStatus( true );
			
			GameConfig.PTR.gameplayEvents.dispatch( UIEventTypes.BALANCE_UPDATED, null );
		}
		else
		{
			showNotEnoughGems();
		}
	}
	
	private function showNotEnoughGems():Void
	{
		// Fill and add image if needed
		if ( notEnoughGemsBitmap == null )
		{
			notEnoughGemsLayer.graphics.clear();
			notEnoughGemsLayer.graphics.beginFill( 0xf6b64b, 0.75 );
			notEnoughGemsLayer.graphics.drawRect( 0, 0, this.width, this.height );
			notEnoughGemsLayer.graphics.endFill();
			
			notEnoughGemsBitmap = AssetManager.PTR.getBitmap( "moregems" );
			notEnoughGemsBitmap.x = ( this.width / 2 ) - ( notEnoughGemsBitmap.width / 2 );
			notEnoughGemsBitmap.y = ( this.height / 2 ) - ( notEnoughGemsBitmap.height / 2 );
			notEnoughGemsLayer.addChild( notEnoughGemsBitmap );
		}
		
		this.addChild( notEnoughGemsLayer );
		
		notEnoughGemsLayer.x = this.width;	// move all the way to the right
		
		transitionsManager.addTransition( new FadeOutFX( btnBuy, 1, 0, 0.5 ) );
		transitionsManager.addTransition( new MoveToFX( mainLayer, 0, 0, -mainLayer.width, 0, 0.5, LerpTypes.EASE_IN_OUT ) );
		transitionsManager.addTransition( new MoveToFX( notEnoughGemsLayer, mainLayer.width, 0, 0, 0, 0.5, LerpTypes.EASE_IN_OUT ) );
		
		btnBuy.setIsEnabled( false );
		
		transitionsManager.addEventListener( Event.COMPLETE, onStartDelay );
	}
	
	private function onStartDelay( e:Event ):Void
	{
		transitionsManager.removeEventListener( Event.COMPLETE, onStartDelay );
		transitionsManager.clear();
		
		transitionsManager.addTransition( new TimeFX( 0.80 ) );
		
		transitionsManager.addEventListener( Event.COMPLETE, onNotEnoughShown );
	}
	
	private function onNotEnoughShown( e:Event ):Void
	{
		transitionsManager.removeEventListener( Event.COMPLETE, onNotEnoughShown );
		transitionsManager.clear();
		
		// Transitions back
		transitionsManager.addTransition( new MoveToFX( mainLayer, mainLayer.x, 0, 0, 0, 0.50, LerpTypes.EASE_IN ) );
		transitionsManager.addTransition( new MoveToFX( notEnoughGemsLayer, notEnoughGemsLayer.x, 0, notEnoughGemsLayer.width, 0, 0.50, LerpTypes.EASE_IN ) );
		
		transitionsManager.addEventListener( Event.COMPLETE, onEntireTransitionDone );
	}
	
	private function onEntireTransitionDone( e:Event ):Void
	{
		transitionsManager.removeEventListener( Event.COMPLETE, onEntireTransitionDone );
		transitionsManager.clear();
		
		this.removeChild( notEnoughGemsLayer );
		
		btnBuy.setIsEnabled( true );
		
		// Fade button back in
		transitionsManager.addTransition( new FadeOutFX( btnBuy, 0, 1, 0.25 ) );
	}
	
	public function tick( deltaTime:Float ):Void
	{
		transitionsManager.tick( deltaTime );
		
		if ( purpleText != null )
		{
			purpleText.tick( deltaTime );
		}
		
		if ( greenText != null )
		{
			greenText.tick( deltaTime );
		}
		
		if ( redText != null )
		{
			redText.tick( deltaTime );
		}
		
		if ( btnBuy != null )
		{
			btnBuy.tick( deltaTime );
		}
	}
	
	public function destroy():Void
	{
		transitionsManager.destroy();
		
		if ( btnBuy != null )
		{
			btnBuy.destroy();
		}
	}
}