package game.ui.nongame;
import engine.assets.AssetManager;
import engine.misc.BlitText;
import engine.misc.ITickable;
import engine.misc.LerpTypes;
import engine.misc.MathUtility;
import engine.misc.StringUtils;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.crates.CrateFactory;
import game.crates.CrateInfo;
import game.ui.general.UIAnimatedButton;
import game.ui.general.UIButton;
import game.ui.nongame.lab.UICrateLab;
import game.ui.transitions.FadeOutFX;
import game.ui.transitions.MoveToFX;
import game.ui.transitions.TransitionPlayer;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.Lib;
import nme.net.URLRequest;

/**
 * ...
 * @author A.A. Grapsas
 */

class UIMainMenu implements ITickable
{
	private var middleDiv:Bitmap;
	private var banner:Bitmap;
	
	private var btnGame:UIAnimatedButton;
	private var btnLab:UIAnimatedButton;
	private var btnRecord:UIAnimatedButton;
	private var btnAbout:UIAnimatedButton;
	
	private var transitions:TransitionPlayer;
	
	private var crateLayer:Sprite;
	
	private var timeUntilNextSpawn:Float = 0;
	private var minTimeToSpawn:Float = 1;
	private var maxTimeToSpawn:Float = 7;
	
	private var crates:Array<Bitmap>;
	private var cratesToRemove:Array<Bitmap>;
	
	private var canSpawnCrates:Bool = true;
	
	private var certified:Sprite;
	
	/* UI Gems */
	// Purple
	var purpleIcon:Bitmap;
	var purpleText:BlitText;
	
	// Green
	var greenIcon:Bitmap;
	var greenText:BlitText;
	
	// Red
	var redIcon:Bitmap;
	var redText:BlitText;
	
	public function new() 
	{
		setup();
	}
	
	private function setup():Void
	{
		crateLayer = new Sprite();
		SceneManager.PTR.uiLayer.addChild( crateLayer );
		
		// Setup middle div
		middleDiv = AssetManager.PTR.getBitmap( "verticalpipe" );
		middleDiv.x = ( ResolutionConfig.PTR.actualScreenWidth / 2 ) - ( middleDiv.width / 2 );
		SceneManager.PTR.uiLayer.addChild( middleDiv );
		
		// Setup banner
		banner = AssetManager.PTR.getBitmap( "banner" );
		banner.x = ( ResolutionConfig.PTR.actualScreenWidth / 2 ) - ( banner.width / 2 );
		SceneManager.PTR.uiLayer.addChild( banner );
		
		// Certified
		certified = new Sprite();
		certified.addChild( AssetManager.PTR.getBitmap( "certified" ) );
		certified.x = ResolutionConfig.PTR.actualScreenWidth - certified.width;
		certified.y = ResolutionConfig.PTR.actualScreenHeight - certified.height;
		SceneManager.PTR.uiLayer.addChild( certified );
		
		// Ceritified listener
		certified.addEventListener( MouseEvent.CLICK, onCertifiedClick );
		certified.useHandCursor = true;
		certified.buttonMode = true;
		
		btnGame = new UIAnimatedButton( "gameon", onGameClicked );
		btnLab = new UIAnimatedButton( "cratelab", onLabClicked );
		btnRecord = new UIAnimatedButton( "yourrecord", onRecordClicked );
		btnAbout = new UIAnimatedButton( "about", onAboutClicked );
		
		btnGame.x = ( ResolutionConfig.PTR.actualScreenWidth / 2 ) - ( btnGame.width / 2 );
		btnLab.x = btnGame.x;
		btnRecord.x = btnGame.x;
		btnAbout.x = btnGame.x;
		
		banner.y = banner.height / 2;
		
		var gameY:Float = banner.y + banner.height + banner.height / 3;
		var labY:Float = gameY + btnGame.height + btnGame.height / 2;
		var recordY:Float = labY + btnLab.height + btnLab.height / 2;
		
		var startY:Float = ResolutionConfig.PTR.actualScreenHeight;
		
		/* GEMS */
		purpleIcon = AssetManager.PTR.getBitmap( "gem" );
		greenIcon = AssetManager.PTR.getBitmap( "greengem" );
		redIcon = AssetManager.PTR.getBitmap( "redgem" );
		
		purpleIcon.width = redIcon.width;
		purpleIcon.height = redIcon.height;
		greenIcon.width = redIcon.width;
		greenIcon.height = redIcon.height;
		
		SceneManager.PTR.uiLayer.addChild( purpleIcon );
		SceneManager.PTR.uiLayer.addChild( greenIcon );
		SceneManager.PTR.uiLayer.addChild( redIcon );
		
		purpleIcon.x = ResolutionConfig.PTR.convertWorldToScreen( 256 );
		purpleIcon.y = recordY + btnRecord.height;
		
		greenIcon.x = purpleIcon.x;
		greenIcon.y = purpleIcon.y + purpleIcon.height;
		
		redIcon.x = purpleIcon.x;
		redIcon.y = greenIcon.y + greenIcon.height;
		
		var leftGemPadding:Float = ResolutionConfig.PTR.convertWorldToScreen( 16 );
		
		// Purple
		purpleText = new BlitText();
		purpleText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		purpleText.setText( StringUtils.getPadded( GameConfig.PTR.userData.purpleGems, 6 ) );
		purpleText.x = leftGemPadding + purpleIcon.x + purpleIcon.width;
		purpleText.y = purpleIcon.y + ( purpleIcon.height / 2 ) - ( purpleText.getHeight() / 2 );
		SceneManager.PTR.addTickable( purpleText, true );
		SceneManager.PTR.uiLayer.addChild( purpleText );
		
		// Green
		greenText = new BlitText();
		greenText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		greenText.setText( StringUtils.getPadded( GameConfig.PTR.userData.greenGems, 6 ) );
		greenText.x = leftGemPadding + greenIcon.x + greenIcon.width;
		greenText.y = greenIcon.y + ( greenIcon.height / 2 ) - ( greenText.getHeight() / 2 );
		SceneManager.PTR.addTickable( greenText, true );
		SceneManager.PTR.uiLayer.addChild( greenText );
		
		// Red
		redText = new BlitText();
		redText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		redText.setText( StringUtils.getPadded( GameConfig.PTR.userData.redGems, 6 ) );
		redText.x = leftGemPadding + redIcon.x + redIcon.width;
		redText.y = redIcon.y + ( redIcon.height / 2 ) - ( redText.getHeight() / 2 );
		SceneManager.PTR.addTickable( redText, true );
		SceneManager.PTR.uiLayer.addChild( redText );
		
		var aboutY:Float = redIcon.y + redIcon.height + ResolutionConfig.PTR.convertWorldToScreen( 16 );
		
		/* TRANSITIONS */
		transitions = new TransitionPlayer();
		transitions.addTransition( new MoveToFX( btnGame, btnGame.x, startY, btnGame.x, gameY, 1, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( btnLab, btnLab.x, startY, btnLab.x, labY, 0.75, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( btnRecord, btnRecord.x, startY, btnRecord.x, recordY, 0.5, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( btnAbout, btnAbout.x, startY, btnAbout.x, aboutY, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( middleDiv, middleDiv.x, -middleDiv.height, middleDiv.x, middleDiv.y, 0.75, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( banner, banner.x, -banner.height, banner.x, banner.y, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( purpleIcon, -purpleIcon.width, purpleIcon.y, purpleIcon.x, purpleIcon.y, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( greenIcon, ResolutionConfig.PTR.actualScreenWidth + greenIcon.width, greenIcon.y, greenIcon.x, greenIcon.y, 0.50, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( redIcon, -redIcon.width, redIcon.y, redIcon.x, redIcon.y, 0.75, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( purpleText, -purpleText.width, purpleText.y, purpleText.x, purpleText.y, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( greenText, ResolutionConfig.PTR.actualScreenWidth + greenText.width, greenText.y, greenText.x, greenText.y, 0.50, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( redText, -redText.width, redText.y, redText.x, redText.y, 0.75, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( certified, -certified.width, certified.y, certified.x, certified.y, 0.50, LerpTypes.EASE_IN ) );
		transitions.addEventListener( Event.COMPLETE, onIntroAnimComplete );
		
		canSpawnCrates = false;
		
		crates = new Array<Bitmap>();
		cratesToRemove = new Array<Bitmap>();
	}
	
	private function onIntroAnimComplete( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onIntroAnimComplete );
		transitions.clear();
		
		canSpawnCrates = true;
	}
	
	private function onGameAnimComplete( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onGameAnimComplete );
		
		breakDownScene();
		
		SceneManager.PTR.addTickable( new UICrateSelection() );
	}
	
	private function onLabAnimComplete( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onLabAnimComplete );
		
		breakDownScene();
		
		SceneManager.PTR.addTickable( new UICrateLab() );
	}
	
	private function onGameClicked():Void
	{
		transitions.addEventListener( Event.COMPLETE, onGameAnimComplete );
		playBreakDownAnims();
	}
	
	private function breakDownScene():Void
	{
		transitions.clear();
		
		SceneManager.PTR.removeTickable( this );
		
		destroy();
	}
	
	private function playBreakDownAnims():Void
	{
		// Setup outro transitions
		transitions.addTransition( new MoveToFX( btnGame, btnGame.x, btnGame.y, ResolutionConfig.PTR.actualScreenWidth + btnGame.width, btnGame.y, 0.25, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( btnLab, btnLab.x, btnLab.y, -btnLab.width, btnLab.y, 0.50, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( btnRecord, btnRecord.x, btnRecord.y, ResolutionConfig.PTR.actualScreenWidth + btnRecord.width, btnRecord.y, 0.75, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( btnAbout, btnAbout.x, btnAbout.y, -btnAbout.width, btnAbout.y, 1.0, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( middleDiv, middleDiv.x, middleDiv.y, middleDiv.x, -middleDiv.height, 1.5, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( banner, banner.x, banner.y, banner.x, -( 2 * banner.y ), 0.50, LerpTypes.LINEAR ) );
		transitions.addTransition( new MoveToFX( purpleIcon, purpleIcon.x, purpleIcon.y, -purpleIcon.width, purpleIcon.y, 0.25, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( greenIcon, greenIcon.x, greenIcon.y, ResolutionConfig.PTR.actualScreenWidth + greenIcon.width, greenIcon.y, 0.50, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( redIcon, redIcon.x, redIcon.y, -redIcon.width, redIcon.y, 0.75, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( purpleText, purpleText.x, purpleText.y, -purpleText.width, purpleText.y, 0.25, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( greenText, greenText.x, greenText.y, ResolutionConfig.PTR.actualScreenWidth + greenText.width, greenText.y, 0.50, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( redText, redText.x, redText.y, -redText.width, redText.y, 0.75, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( certified, certified.x, certified.y, ResolutionConfig.PTR.actualScreenWidth + certified.width, certified.y, 0.50, LerpTypes.EASE_OUT ) );
		
		for ( crate in crates )
		{
			transitions.addTransition( new FadeOutFX( crate, 1, 0, 0.5 ) );
		}
		
		canSpawnCrates = false;
	}
	
	private function onCertifiedClick( e:MouseEvent ):Void
	{
		Lib.getURL( new URLRequest( "http://certifiedorganicgames.com" ) );
	}
	
	private function onLabClicked():Void
	{
		transitions.addEventListener( Event.COMPLETE, onLabAnimComplete );
		playBreakDownAnims();
	}
	
	private function onRecordClicked():Void
	{
		
	}
	
	private function onAboutClicked():Void
	{
		
	}
	
	private function spawnCrate():Void
	{
		var randomCrates:Array<String> = [ CrateFactory.BORING_CRATE, CrateFactory.DEATH_CRATE, CrateFactory.FRAGILE_CRATE, CrateFactory.LOCK_CRATE, "mysterybox_representation", CrateFactory.RADIO_ACTIVE_CRATE, CrateFactory.GEM_CRATE, CrateFactory.SMILE_CRATE, CrateFactory.TIME_CRATE, CrateFactory.TOXIC_CRATE ];
		var randomIndex:Int = cast Math.floor( Math.random() * randomCrates.length );
		var crate:Bitmap = AssetManager.PTR.getBitmap( randomCrates[ randomIndex ] );
		
		crate.x = ( ResolutionConfig.PTR.actualScreenWidth / 2 ) - ( crate.width / 2 );
		crate.y = -crate.height;
		
		crates.push( crate );
		
		crateLayer.addChild( crate );
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( transitions != null )
		{
			transitions.tick( deltaTime );
		}
		
		if ( canSpawnCrates )
		{
			timeUntilNextSpawn -= deltaTime;
		
			if ( timeUntilNextSpawn <= 0 )
			{
				timeUntilNextSpawn = MathUtility.getRandomFloatInRange( minTimeToSpawn, maxTimeToSpawn );
				
				// spawn
				spawnCrate();
			}
		}
		
		for ( crate in crates )
		{
			crate.y += 300 * deltaTime;
			
			if ( crate.y > ResolutionConfig.PTR.actualScreenHeight )
			{
				cratesToRemove.push( crate );
			}
		}
		
		if ( cratesToRemove.length > 0 )
		{
			for ( toRemove in cratesToRemove )
			{
				crates.remove( toRemove );
				crateLayer.removeChild( toRemove );
			}
			
			cratesToRemove.splice( 0, cratesToRemove.length );
		}
		
		btnAbout.tick( deltaTime );
		btnGame.tick( deltaTime );
		btnLab.tick( deltaTime );
		btnRecord.tick( deltaTime );
	}
	
	public function destroy():Void
	{
		btnAbout.destroy();
		btnGame.destroy();
		btnLab.destroy();
		btnRecord.destroy();
		
		certified.removeEventListener( MouseEvent.CLICK, onCertifiedClick );
		
		SceneManager.PTR.uiLayer.removeChild( certified );
		
		SceneManager.PTR.uiLayer.removeChild( crateLayer );
		SceneManager.PTR.uiLayer.removeChild( middleDiv );
		SceneManager.PTR.uiLayer.removeChild( banner );
		
		SceneManager.PTR.uiLayer.removeChild( redIcon );
		SceneManager.PTR.uiLayer.removeChild( redText );
		SceneManager.PTR.uiLayer.removeChild( greenIcon );
		SceneManager.PTR.uiLayer.removeChild( greenText );
		SceneManager.PTR.uiLayer.removeChild( purpleIcon );
		SceneManager.PTR.uiLayer.removeChild( purpleText );
		
		SceneManager.PTR.removeTickable( redText, true );
		SceneManager.PTR.removeTickable( greenText, true );
		SceneManager.PTR.removeTickable( purpleText, true );
	}
}