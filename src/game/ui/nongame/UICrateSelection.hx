package game.ui.nongame;
import engine.assets.AssetManager;
import engine.misc.ITickable;
import engine.misc.LerpTypes;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.CrateLevel;
import game.crates.CrateFactory;
import game.crates.CrateInfo;
import game.ui.general.UIAnimatedButton;
import game.ui.transitions.FadeOutFX;
import game.ui.transitions.MoveToFX;
import game.ui.transitions.TransitionPlayer;
import native.display.InterpolationMethod;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.events.Event;

/**
 * ...
 * @author A.A. Grapsas
 */

class UICrateSelection implements ITickable
{
	private var crateLayer:Sprite;
	
	private var banner:Bitmap;
	private var crateLabInfo:Bitmap;
	
	private var btnRollCrates:UIAnimatedButton;
	private var btnStartGame:UIAnimatedButton;
	private var btnBack:UIAnimatedButton;
	
	private var crates:Array<Bitmap>;
	private var crateTypes:Array<CrateInfo>;
	private var oldCrates:Array<Bitmap>;
	
	private var transitions:TransitionPlayer;
	private var crateTransitions:TransitionPlayer;
	
	private var isTransitioningOut:Bool = false;
	
	public function new() 
	{
		setup();
	}
	
	private function setup():Void
	{
		crateLayer = new Sprite();
		SceneManager.PTR.uiLayer.addChild( crateLayer );
		
		banner = AssetManager.PTR.getBitmap( "gameon_icon" );
		SceneManager.PTR.uiLayer.addChild( banner );
		
		crateLabInfo = AssetManager.PTR.getBitmap( "unlockmorecrates" );
		SceneManager.PTR.uiLayer.addChild( crateLabInfo );
		
		btnRollCrates = new UIAnimatedButton( "gimmecrates", onRollCrates );
		btnStartGame = new UIAnimatedButton( "usethese", onPlayGame );
		btnBack = new UIAnimatedButton( "back", onBack );
		
		transitions = new TransitionPlayer();
		
		var halfResWidth:Float = ResolutionConfig.PTR.actualScreenWidth / 2;
		
		var offscreenBottom:Float = ResolutionConfig.PTR.actualScreenHeight;
		
		transitions.addTransition( new MoveToFX( banner, -banner.width, banner.height, halfResWidth - ( banner.width / 2 ), banner.height, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( btnRollCrates, halfResWidth - ( btnRollCrates.width / 2 ), offscreenBottom - btnRollCrates.height, halfResWidth - ( btnRollCrates.width / 2 ), banner.y + banner.height + banner.height / 2, 0.5, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( btnStartGame, halfResWidth - ( btnStartGame.width / 2 ) + btnBack.width / 2.25, offscreenBottom - btnStartGame.height, halfResWidth - ( btnStartGame.width / 2 ) + btnBack.width / 2.25, offscreenBottom - btnStartGame.height, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( btnBack, -btnBack.width, offscreenBottom - btnBack.height, btnStartGame.x - btnBack.width / 1.25, offscreenBottom - btnBack.height, 0.30, LerpTypes.EASE_IN ) );
		
		var crateLabY:Float = banner.y + banner.height + ( banner.height / 2 ) + btnRollCrates.height;
		transitions.addTransition( new MoveToFX( crateLabInfo, -crateLabInfo.width, crateLabY, halfResWidth - ( crateLabInfo.width / 2 ), crateLabY, 0.25, LerpTypes.EASE_IN ) );
		
		transitions.addEventListener( Event.COMPLETE, onAnimInEnded );
		
		crates = new Array<Bitmap>();
		crateTypes = new Array<CrateInfo>();
		oldCrates = new Array<Bitmap>();
		
		crateTransitions = new TransitionPlayer();
	}
	
	private function onAnimInEnded( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onAnimInEnded );
		transitions.clear();
		
		// Begin loading first set of random crates!
		selectRandomCrates();
	}
	
	private function selectRandomCrates():Void
	{		
		if ( isTransitioningOut )
		{
			return;
		}
		
		if ( crateTransitions.getCount() > 0 )
		{
			return;
		}
		
		var bottomScreen:Float = ResolutionConfig.PTR.actualScreenHeight;
		
		if ( crates.length > 0 )
		{
			// Destroy all existing crates
			for ( crate in crates )
			{
				oldCrates.push( crate );
				
				crateTransitions.addTransition( new MoveToFX( crate, crate.x, crate.y, crate.x, bottomScreen + crate.height, 0.5, LerpTypes.EASE_OUT ) );
			}
			
			crates.splice( 0, crates.length );
			crateTypes.splice( 0, crateTypes.length );
		}
		
		var possibilities:Array<CrateInfo> = GameConfig.PTR.crateCatalog.getAllUnlockedNonDefaultCrates();
		
		var localPossibilities:Array<CrateInfo> = new Array<CrateInfo>();
		
		for ( possibility in possibilities )
		{
			localPossibilities.push( possibility );
		}
		
		var count:Int = 4;
		
		var set:Array<String> = new Array<String>();
		
		var runningTime:Float = 0.25;
		
		var crateSize:Float = AssetManager.PTR.getGameBitmap( "crate" ).scaledWidth;
		
		var leftLaneX:Float = ( ResolutionConfig.PTR.actualScreenWidth / 2 ) - ( crateSize * 1.5 );
		var rightLaneX:Float = ( ResolutionConfig.PTR.actualScreenWidth / 2 ) + ( crateSize * 0.5 );
		var top:Float = btnStartGame.y;
		
		for ( i in 0...count )
		{
			var random:Int = cast Math.floor( Math.random() * localPossibilities.length );
			
			var selected:CrateInfo = localPossibilities[ random ];
			
			localPossibilities.remove( selected );
			
			crateTypes.push( selected );
			
			// Setup asset
			var asset:Bitmap = AssetManager.PTR.getBitmap( selected.uiAsset );
			
			crateLayer.addChild( asset );
			
			var crateLeft:Float = i % 2 == 0 ? leftLaneX : rightLaneX;
			var crateTop:Float = i > 1 ? top - ( 2 * asset.height ) : top - asset.height;
			
			crateTransitions.addTransition( new MoveToFX( asset, crateLeft, -asset.height, crateLeft, crateTop, runningTime, LerpTypes.EASE_IN ) );
			
			runningTime += 0.25;
			
			crates.push( asset );
		}
		
		crateTransitions.addEventListener( Event.COMPLETE, onSelectedCratesTransitioned );
	}
	
	private function onSelectedCratesTransitioned( e:Event ):Void
	{
		if ( oldCrates.length > 0 )
		{
			for ( oldCrate in oldCrates )
			{
				crateLayer.removeChild( oldCrate );
			}
			
			oldCrates.splice( 0, oldCrates.length );
		}
		
		crateTransitions.removeEventListener( Event.COMPLETE, onSelectedCratesTransitioned );
		crateTransitions.clear();
	}
	
	private function onRollCrates():Void
	{
		selectRandomCrates();
	}
	
	private function onBack():Void
	{
		transitionOut();
		transitions.addEventListener( Event.COMPLETE, onTransitionOutBackDone );
	}
	
	private function transitionOut():Void
	{
		isTransitioningOut = true;
		
		// Move all UI out of the world
		transitions.addTransition( new MoveToFX( banner, banner.x, banner.y, banner.x, -banner.height, 0.25, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new FadeOutFX( crateLabInfo, 1, 0, 0.25 ) );
		transitions.addTransition( new MoveToFX( btnBack, btnBack.x, btnBack.y, -btnBack.width, btnBack.y, 0.30, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( btnRollCrates, btnRollCrates.x, btnRollCrates.y, -btnRollCrates.width, btnRollCrates.y, 0.35, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( btnStartGame, btnStartGame.x, btnStartGame.y, btnStartGame.x, ResolutionConfig.PTR.actualScreenHeight + btnStartGame.height, 0.15, LerpTypes.EASE_OUT ) );
		
		var runningTime:Float = 0.60;
		
		for ( crate in crates )
		{
			transitions.addTransition( new MoveToFX( crate, crate.x, crate.y, crate.x, -crate.height, runningTime, LerpTypes.EASE_OUT ) );
			runningTime += 0.20;
		}
	}
	
	private function onTransitionOutBackDone( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onTransitionOutBackDone );
		transitions.clear();
		
		destroy();
		
		SceneManager.PTR.removeTickable( this );
		
		SceneManager.PTR.addTickable( new UIMainMenu() );
	}
	
	private function onPlayGame():Void
	{		
		transitionOut();
		transitions.addEventListener( Event.COMPLETE, onTransitionPlayDone );
	}
	
	private function onTransitionPlayDone( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onTransitionPlayDone );
		transitions.clear();
		
		destroy();
		
		SceneManager.PTR.removeTickable( this );
		
		// Setup GAME
		GameConfig.PTR.currentLevel = new CrateLevel();
		GameConfig.PTR.currentLevel.setAllowedCrates( crateTypes );
		GameConfig.PTR.currentLevel.reset( false, true );//<--reset seems to be the way to start as well. Why not?
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( transitions != null )
		{
			transitions.tick( deltaTime );
		}
		
		if ( crateTransitions != null )
		{
			crateTransitions.tick( deltaTime );
		}
		
		btnRollCrates.tick( deltaTime );
		btnBack.tick( deltaTime );
		btnStartGame.tick( deltaTime );
	}
	
	public function destroy():Void
	{
		SceneManager.PTR.uiLayer.removeChild( banner );
		SceneManager.PTR.uiLayer.removeChild( crateLabInfo );
		SceneManager.PTR.uiLayer.removeChild( crateLayer );
		
		btnRollCrates.destroy();
		btnStartGame.destroy();
		btnBack.destroy();
		
		crateTransitions.destroy();
		transitions.destroy();
	}
}