package game.ui.transitions;
import engine.misc.ITickable;
import engine.misc.LerpTypes;
import engine.misc.MathUtility;
import nme.display.DisplayObject;

/**
 * ...
 * @author A.A. Grapsas
 */

class MoveToFX implements ITransition
{
	private var isDone:Bool = false;
	
	private var display:DisplayObject;
	
	private var duration:Float = 0;
	private var accumulator:Float = 0;
	
	private var startX:Float = 0;
	private var endX:Float = 0;
	
	private var startY:Float = 0;
	private var endY:Float = 0;

	private var interpolation:Int;
	
	public function new( display:DisplayObject, startX:Float, startY:Float, endX:Float, endY:Float, time:Float, interpolation:Int ) 
	{
		this.display = display;
		
		this.startX = startX;
		this.startY = startY;
		
		this.endX = endX;
		this.endY = endY;
		
		this.duration = time;
		
		this.interpolation = interpolation;
		
		display.x = startX;
		display.y = startY;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( isDone )
		{
			return;
		}
		
		accumulator += deltaTime;
		
		var alpha:Float = Math.min( accumulator / duration, 1 );
		
		display.x = LerpTypes.getLerpValue( interpolation, startX, endX, alpha );
		display.y = LerpTypes.getLerpValue( interpolation, startY, endY, alpha );
		
		if ( alpha >= 1 )
		{
			isDone = true;
		}
	}
	
	public function getIsDone():Bool { return isDone; }
}