package game.ui.transitions;

/**
 * ...
 * @author A.A. Grapsas
 */
class TimeFX implements ITransition
{
	private var duration:Float = 0;
	private var accumulator:Float = 0;
	
	public function new( duration:Float ) 
	{
		this.duration = duration;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		accumulator += deltaTime;
	}
	
	public function getIsDone():Bool { return accumulator >= duration; }
}