package game.ui.transitions;
import engine.misc.LerpTypes;
import engine.render.GameTile;

/**
 * ...
 * @author A.A. Grapsas
 */
class FadeOutFXTile implements ITransition
{
	private var isDone:Bool = false;
	
	private var display:GameTile;
	
	private var startAlpha:Float = 0;
	private var targetAlpha:Float = 0;
	private var duration:Float = 0;
	
	private var accumulator:Float = 0;
	
	public function new( display:GameTile, startAlpha:Float, endAlpha:Float, duration:Float ) 
	{
		this.display = display;
		this.startAlpha = startAlpha;
		this.targetAlpha = endAlpha;
		this.duration = duration;
		
		display.alpha = startAlpha;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( isDone )
		{
			return;
		}
		
		accumulator += deltaTime;
		
		var alpha:Float = Math.min( accumulator / duration, 1 );
		
		display.alpha = LerpTypes.getLerpValue( LerpTypes.LINEAR, startAlpha, targetAlpha, alpha );
		
		if ( alpha >= 1 )
		{
			isDone = true;
		}
	}
	
	public function getIsDone():Bool { return isDone; }
}