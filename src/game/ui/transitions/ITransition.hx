package game.ui.transitions;
import engine.misc.ITickable;
import nme.events.IEventDispatcher;

/**
 * ...
 * @author A.A. Grapsas
 */

interface ITransition implements ITickable
{
	function getIsDone():Bool;
}