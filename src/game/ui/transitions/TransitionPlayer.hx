package game.ui.transitions;
import engine.misc.ITickable;
import engine.scene.SceneManager;
import nme.events.Event;
import nme.events.EventDispatcher;

/**
 * ...
 * @author A.A. Grapsas
 */

class TransitionPlayer extends EventDispatcher, implements ITickable
{
	public var doneCallback:Void -> Void;
	
	public var isDone( default, null ):Bool = false;
	
	public var transitions:Array<ITransition>;
	
	public function new() 
	{
		super();
		
		transitions = new Array<ITransition>();
	}
	
	public function addTransition( transition:ITransition ):Void
	{
		transitions.push( transition );
		
		isDone = false;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( isDone )
		{
			return;
		}
		
		var isPossiblyDone:Bool = true;
		
		for ( transition in transitions )
		{
			transition.tick( deltaTime );
			
			if ( transition.getIsDone() == false )
			{
				isPossiblyDone = false;
			}
		}
		
		if ( isPossiblyDone && transitions.length > 0 )
		{
			isDone = true;
			
			this.dispatchEvent( new Event( Event.COMPLETE ) );
			
			if ( doneCallback != null )
			{
				//Logic changed so chaining can work
				var tmpDoneCallback:Void -> Void = doneCallback;
				doneCallback = null;
				tmpDoneCallback();
			}
		}
	}
	
	public function clear():Void
	{
		transitions.splice( 0, transitions.length );
		isDone = false;
	}
	
	public function getCount():Int { return transitions.length; }
	
	public function destroy():Void
	{
		clear();
	}
}