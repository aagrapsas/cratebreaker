package game.ui.ingame;
import engine.assets.AssetManager;
import engine.misc.BlitText;
import engine.misc.ITickable;
import engine.misc.LerpTypes;
import engine.misc.StringUtils;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.ui.general.UIAnimatedButton;
import game.ui.nongame.UICrateSelection;
import game.ui.nongame.UIMainMenu;
import game.ui.transitions.FadeOutFX;
import game.ui.transitions.MoveToFX;
import game.ui.transitions.TransitionPlayer;
import nme.display.Bitmap;
import nme.events.Event;

/**
 * ...
 * @author A.A. Grapsas
 */

class UIGameOver implements ITickable
{
	private var banner:Bitmap;
	
	private var gems:Bitmap;
	private var total:Bitmap;
	private var cratesSmashed:Bitmap;
	private var mostSmashed:Bitmap;
	
	private var purpleGem:Bitmap;
	private var greenGem:Bitmap;
	private var redGem:Bitmap;
	
	private var purpleText:BlitText;
	private var greenText:BlitText;
	private var redText:BlitText;
	
	private var purpleTotalText:BlitText;
	private var greenTotalText:BlitText;
	private var redTotalText:BlitText;
	
	private var btnAnother:UIAnimatedButton;
	private var btnLab:UIAnimatedButton;
	private var btnMenu:UIAnimatedButton;
	
	private var transitions:TransitionPlayer;
	
	public function new() 
	{
		setup();
	}
	
	private function setup():Void
	{
		transitions = new TransitionPlayer();
		
		banner = AssetManager.PTR.getBitmap( "gameover" );
		banner.x = ( ResolutionConfig.PTR.actualScreenWidth / 2 ) - ( banner.width / 2 );
		banner.y = -banner.height;
		SceneManager.PTR.uiLayer.addChild( banner );
		transitions.addTransition( new MoveToFX( banner, banner.x, banner.y, banner.x, banner.height / 2, 2.5, LerpTypes.EASE_IN ) );
		//transitions.addEventListener( Event.COMPLETE, onBannerAppeared );
		transitions.doneCallback = onBannerAppeared;
	}
	
	private function onBannerAppeared():Void
	{
		//transitions.removeEventListener( Event.COMPLETE, onBannerAppeared );
		transitions.clear();
		
		var screenRight:Float = ResolutionConfig.PTR.actualScreenWidth;
		var screenBottom:Float = ResolutionConfig.PTR.actualScreenHeight;
		var leftBuffer:Float = ResolutionConfig.PTR.convertWorldToScreen( 25 );
		
		// Setup everything else's transition
		gems = AssetManager.PTR.getBitmap( "gameover_gems" );
		gems.x = -gems.width;
		gems.y = banner.y + banner.height + gems.height / 2;
		SceneManager.PTR.uiLayer.addChild( gems );
		transitions.addTransition( new MoveToFX( gems, gems.x, gems.y, leftBuffer, gems.y, 0.50, LerpTypes.EASE_IN ) );
		
		total = AssetManager.PTR.getBitmap( "gameover_total" );
		total.x = screenRight + total.width;
		total.y = gems.y + ( gems.height / 2 ) - ( total.height / 2 );
		SceneManager.PTR.uiLayer.addChild( total );
		transitions.addTransition( new MoveToFX( total, total.x, total.y, screenRight / 2, total.y, 0.50, LerpTypes.EASE_IN ) );
		
		cratesSmashed = AssetManager.PTR.getBitmap( "gameover_cratessmashed" );
		cratesSmashed.x = -cratesSmashed.width;
		cratesSmashed.y = gems.y + gems.height + ResolutionConfig.PTR.convertWorldToScreen( 200 );
		SceneManager.PTR.uiLayer.addChild( cratesSmashed );
		transitions.addTransition( new MoveToFX( cratesSmashed, cratesSmashed.x, cratesSmashed.y, leftBuffer, cratesSmashed.y, 0.75, LerpTypes.EASE_IN ) );
		
		mostSmashed = AssetManager.PTR.getBitmap( "gameover_mostsmashed" );
		mostSmashed.x = -mostSmashed.width;
		mostSmashed.y = cratesSmashed.y + cratesSmashed.height + ResolutionConfig.PTR.convertWorldToScreen( 100 );
		SceneManager.PTR.uiLayer.addChild( mostSmashed );
		transitions.addTransition( new MoveToFX( mostSmashed, mostSmashed.x, mostSmashed.y, leftBuffer, mostSmashed.y, 1, LerpTypes.EASE_IN ) );
		
		// Buttons
		btnMenu = new UIAnimatedButton( "menu", onMenuClicked );
		btnMenu.x = ( screenRight / 2 ) - ( btnMenu.width / 2 );
		transitions.addTransition( new MoveToFX( btnMenu, btnMenu.x, screenBottom + btnMenu.height, btnMenu.x, screenBottom - btnMenu.height, 0.25, LerpTypes.EASE_IN ) );
		
		btnLab = new UIAnimatedButton( "tothelab", onLabClicked );
		btnLab.x = ( screenRight / 2 ) - ( btnLab.width / 2 );
		transitions.addTransition( new MoveToFX( btnLab, btnLab.x, screenBottom + btnLab.height, btnLab.x, screenBottom - btnMenu.height - btnLab.height, 0.50, LerpTypes.EASE_IN ) );
		
		btnAnother = new UIAnimatedButton( "another", onAgainClicked );
		btnAnother.x = ( screenRight / 2 ) - ( btnAnother.width / 2 );
		transitions.addTransition( new MoveToFX( btnAnother, btnAnother.x, screenBottom + btnAnother.height, btnAnother.x, screenBottom - btnMenu.height - btnLab.height - btnAnother.height, 0.75, LerpTypes.EASE_IN ) );
		
		// Gems
		purpleGem = AssetManager.PTR.getBitmap( "gem" );
		greenGem = AssetManager.PTR.getBitmap( "greengem" );
		redGem = AssetManager.PTR.getBitmap( "redgem" );
		
		purpleGem.width = redGem.width;
		greenGem.width = redGem.width;
		purpleGem.height = redGem.height;
		greenGem.height = redGem.height;
		
		SceneManager.PTR.uiLayer.addChild( purpleGem );
		SceneManager.PTR.uiLayer.addChild( greenGem );
		SceneManager.PTR.uiLayer.addChild( redGem );
		
		var gemLeft:Float = ResolutionConfig.PTR.convertWorldToScreen( 32 );
		var gemVerticalBuffer:Float = ResolutionConfig.PTR.convertWorldToScreen( 8 );
		
		purpleGem.y = gems.y + gems.height + gemVerticalBuffer;
		greenGem.y = purpleGem.y + purpleGem.height + gemVerticalBuffer;
		redGem.y = greenGem.y + greenGem.height + gemVerticalBuffer;
		
		purpleGem.x = gemLeft;
		greenGem.x = gemLeft;
		redGem.x = gemLeft;
		
		// Gem text
		var gemTextLeftBuffer:Float = ResolutionConfig.PTR.convertWorldToScreen( 8 );
		purpleText = new BlitText();
		purpleText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		purpleText.setText( StringUtils.getPadded( GameConfig.PTR.currentLevel.getStats().purpleGemsCollected, 4 ) );
		purpleText.x = purpleGem.x + purpleGem.width + gemTextLeftBuffer;
		purpleText.y = purpleGem.y + ( purpleGem.height / 2 ) - ( purpleText.getHeight() / 2 );
		
		SceneManager.PTR.uiLayer.addChild( purpleText );
		SceneManager.PTR.addTickable( purpleText, true );
		
		greenText = new BlitText();
		greenText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		greenText.setText( StringUtils.getPadded( GameConfig.PTR.currentLevel.getStats().greenGemsCollected, 4 ) );
		greenText.x = greenGem.x + greenGem.width + gemTextLeftBuffer;
		greenText.y = greenGem.y + ( greenGem.height / 2 ) - ( greenText.getHeight() / 2 );
		
		SceneManager.PTR.uiLayer.addChild( greenText );
		SceneManager.PTR.addTickable( greenText, true );
		
		redText = new BlitText();
		redText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		redText.setText( StringUtils.getPadded( GameConfig.PTR.currentLevel.getStats().redGemsCollected, 4 ) );
		redText.x = redGem.x + redGem.width + gemTextLeftBuffer;
		redText.y = redGem.y + ( redGem.height / 2 ) - ( redText.getHeight() / 2 );
		
		SceneManager.PTR.uiLayer.addChild( redText );
		SceneManager.PTR.addTickable( redText, true );
		
		// Gem totals
		purpleTotalText = new BlitText();
		purpleTotalText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		purpleTotalText.setText( StringUtils.getPadded( GameConfig.PTR.userData.purpleGems, 4 ) );
		purpleTotalText.x = ( screenRight / 2 ) + gemTextLeftBuffer;
		purpleTotalText.y = purpleText.y;
		
		SceneManager.PTR.uiLayer.addChild( purpleTotalText );
		SceneManager.PTR.addTickable( purpleTotalText, true );
		
		greenTotalText = new BlitText();
		greenTotalText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		greenTotalText.setText( StringUtils.getPadded( GameConfig.PTR.userData.greenGems, 4 ) );
		greenTotalText.x = purpleTotalText.x;
		greenTotalText.y = greenText.y;
		
		SceneManager.PTR.uiLayer.addChild( greenTotalText );
		SceneManager.PTR.addTickable( greenTotalText, true );
		
		redTotalText = new BlitText();
		redTotalText.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		redTotalText.setText( StringUtils.getPadded( GameConfig.PTR.userData.redGems, 4 ) );
		redTotalText.x = purpleTotalText.x;
		redTotalText.y = redText.y;
		
		SceneManager.PTR.uiLayer.addChild( redTotalText );
		SceneManager.PTR.addTickable( redTotalText, true );
		
		// Gem transitions (all of 'em!)
		transitions.addTransition( new MoveToFX( purpleGem, -purpleGem.width, purpleGem.y, purpleGem.x, purpleGem.y, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( greenGem, -greenGem.width, greenGem.y, greenGem.x, greenGem.y, 0.50, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( redGem, -redGem.width, redGem.y, redGem.x, redGem.y, 0.75, LerpTypes.EASE_IN ) );
		
		transitions.addTransition( new MoveToFX( purpleText, -purpleText.width, purpleText.y, purpleText.x, purpleText.y, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( greenText, -greenText.width, greenText.y, greenText.x, greenText.y, 0.50, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( redText, -redText.width, redText.y, redText.x, redText.y, 0.75, LerpTypes.EASE_IN ) );
		
		transitions.addTransition( new MoveToFX( purpleTotalText, screenRight + purpleTotalText.getWidth(), purpleTotalText.y, purpleTotalText.x, purpleTotalText.y, 0.75, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( greenTotalText, screenRight + greenTotalText.getWidth(), greenTotalText.y, greenTotalText.x, greenTotalText.y, 0.50, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( redTotalText, screenRight + redTotalText.getWidth(), redTotalText.y, redTotalText.x, redTotalText.y, 0.25, LerpTypes.EASE_IN ) );
		
		transitions.addEventListener( Event.COMPLETE, onMainTransitionsDone );
	}
	
	private function onMainTransitionsDone( e:Event ):Void 
	{
		transitions.removeEventListener( Event.COMPLETE, onMainTransitionsDone );
		transitions.clear();
	}
	
	private function onMenuClicked():Void
	{
		transitionOut();
		transitions.addEventListener( Event.COMPLETE, onTransitionOutMenu );
	}
	
	private function onTransitionOutMenu( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onTransitionOutMenu );
		transitions.clear();
		
		// Head to the main menu
		destroy();
		SceneManager.PTR.addTickable( new UIMainMenu() );
	}
	
	private function onLabClicked():Void
	{
		//transitionOut();
		//transitions.addEventListener( Event.COMPLETE, onTransitionOutLab );
	}
	
	private function onTransitionOutLab( e:Event ):Void
	{
		// Head to the lab
		// @TODO: hook up to lab
	}
	
	private function onAgainClicked():Void
	{
		transitionOut();
		transitions.addEventListener( Event.COMPLETE, onTransitionOutAgain );
	}
	
	private function onTransitionOutAgain( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE, onTransitionOutAgain );
		transitions.clear();
		
		// Head to crate selector
		destroy();
		SceneManager.PTR.addTickable( new UICrateSelection() );
	}
	
	private function transitionOut():Void
	{
		var rightSide:Float = ResolutionConfig.PTR.actualScreenWidth;
		var bottom:Float = ResolutionConfig.PTR.actualScreenHeight;
		
		// Move everything away!
		transitions.addTransition( new MoveToFX( banner, banner.x, banner.y, banner.x, -banner.height, 1.0, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( gems, gems.x, gems.y, -gems.width, gems.y, 0.50, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( total, total.x, total.y, rightSide + total.width, total.y, 0.50, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( cratesSmashed, cratesSmashed.x, cratesSmashed.y, -cratesSmashed.width, cratesSmashed.y, 0.75, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new MoveToFX( mostSmashed, mostSmashed.x, mostSmashed.y, -mostSmashed.width, mostSmashed.y, 1.0, LerpTypes.EASE_OUT ) );
		transitions.addTransition( new FadeOutFX( btnAnother, 1, 0, 0.50 ) );
		transitions.addTransition( new FadeOutFX( btnLab, 1, 0, 0.75 ) );
		transitions.addTransition( new FadeOutFX( btnMenu, 1, 0, 1.0 ) );
		
		// All the gems in the friggin' world
		transitions.addTransition( new MoveToFX( purpleGem, purpleGem.x, purpleGem.y, -purpleGem.width, purpleGem.y, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( greenGem, greenGem.x, greenGem.y, -greenGem.width, greenGem.y, 0.50, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( redGem, redGem.x, redGem.y, -redGem.width, redGem.y, 0.75, LerpTypes.EASE_IN ) );
		
		transitions.addTransition( new MoveToFX( purpleText, purpleText.x, purpleText.y, -purpleText.width, purpleText.y, 0.25, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( greenText, greenText.x, greenText.y, -greenText.width, greenText.y, 0.50, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( redText, redText.x, redText.y, -redText.width, redText.y, 0.75, LerpTypes.EASE_IN ) );
		
		transitions.addTransition( new MoveToFX( purpleTotalText, purpleTotalText.x, purpleTotalText.y, rightSide + purpleTotalText.getWidth(), purpleTotalText.y, 0.75, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( greenTotalText, greenTotalText.x, greenTotalText.y, rightSide + greenTotalText.getWidth(), greenTotalText.y, 0.50, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( redTotalText, redTotalText.x, redTotalText.y, rightSide + redTotalText.getWidth(), redTotalText.y, 0.25, LerpTypes.EASE_IN ) );
	}
	
	public function tick( deltaTime:Float ):Void
	{
		transitions.tick( deltaTime );
		
		if ( btnAnother != null )
		{
			btnAnother.tick( deltaTime );
		}
		
		if ( btnLab != null )
		{
			btnLab.tick( deltaTime );
		}
		
		if ( btnMenu != null )
		{
			btnMenu.tick( deltaTime );
		}
	}
	
	public function destroy():Void
	{
		SceneManager.PTR.uiLayer.removeChild( banner );
		SceneManager.PTR.uiLayer.removeChild( gems );
		SceneManager.PTR.uiLayer.removeChild( total );
		SceneManager.PTR.uiLayer.removeChild( cratesSmashed );
		SceneManager.PTR.uiLayer.removeChild( mostSmashed );
		
		// All of the gem related elements!
		SceneManager.PTR.uiLayer.removeChild( purpleGem );
		SceneManager.PTR.uiLayer.removeChild( greenGem );
		SceneManager.PTR.uiLayer.removeChild( redGem );
		SceneManager.PTR.uiLayer.removeChild( purpleText );
		SceneManager.PTR.uiLayer.removeChild( greenText );
		SceneManager.PTR.uiLayer.removeChild( redText );
		SceneManager.PTR.uiLayer.removeChild( purpleTotalText );
		SceneManager.PTR.uiLayer.removeChild( greenTotalText );
		SceneManager.PTR.uiLayer.removeChild( redTotalText );
		
		SceneManager.PTR.removeTickable( purpleText, true );
		SceneManager.PTR.removeTickable( greenText, true );
		SceneManager.PTR.removeTickable( redText, true );
		SceneManager.PTR.removeTickable( purpleTotalText, true );
		SceneManager.PTR.removeTickable( greenTotalText, true );
		SceneManager.PTR.removeTickable( redTotalText, true );
		
		btnAnother.destroy();
		btnLab.destroy();
		btnMenu.destroy();
		
		transitions.clear();
		
		GameConfig.PTR.currentLevel.hideScrim();
		
		// Break down current level
		GameConfig.PTR.currentLevel.destroy();
		GameConfig.PTR.currentLevel = null;
	}
}