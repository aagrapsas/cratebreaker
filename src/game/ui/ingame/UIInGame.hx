package game.ui.ingame;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import engine.collision.RectCollider;
import engine.misc.ITickable;
import engine.misc.LerpTypes;
import engine.render.GameTile;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.config.GameConfig;
import game.config.SortOrders;
import game.ui.general.UIAnimatedButton;
import game.ui.general.UIButton;
import game.ui.general.UICounter;
import game.ui.transitions.FadeOutFX;
import game.ui.transitions.FadeOutFXTile;
import game.ui.transitions.MoveToFX;
import game.ui.transitions.TransitionPlayer;
import nme.display.Bitmap;
import nme.events.Event;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class UIInGame implements ITickable
{
	public var comboMeter:UIComboMeter;
	
	private var resetButton:UIButton;
	private var pauseButton:UIButton;
	private var resumeButton:UIButton;
	
	private var purpleGem:Bitmap;
	private var greenGem:Bitmap;
	private var redGem:Bitmap;
	
	private var purpleGemCount:UICounter;
	private var greenGemCount:UICounter;
	private var redGemCount:UICounter;
	
	private var quit:UIAnimatedButton;
	private var paused:Bitmap;
	
	private var cachedPoint:Point;
	
	private var cratePipes:Array<Bitmap>;
	private var background:GameTile;
	
	private var transitions:TransitionPlayer;
	
	public function new() 
	{
		SceneManager.PTR.addTickable( this, true );
		
		setup();
	}
	
	private function setup():Void
	{
		cachedPoint = new Point();
		
		// Crate pipes
		cratePipes = new Array<Bitmap>();
		
		var crateWidth:Float = AssetManager.PTR.getGameBitmap( "crate" ).originalWidth / 2;
		
		for ( allowedLane in GameConfig.PTR.currentLevel.getSpawner().allowedLanes )
		{
			var bitmap:Bitmap = AssetManager.PTR.getBitmap( "channel" );
			
			SceneManager.PTR.uiLayer.addChild( bitmap );
			
			bitmap.x = ResolutionConfig.PTR.convertWorldToScreen( allowedLane - crateWidth );
			bitmap.y = ResolutionConfig.PTR.convertWorldToScreen( 140 );
			
			bitmap.alpha = 0;
			
			cratePipes.push( bitmap );
		}
		
		var bgTile:GameBitmap = AssetManager.PTR.getGameBitmap( "upperchannel" );
		
		background = bgTile.getTile();
		background.sort = SortOrders.UI_BACKING;
		background.x = bgTile.scaledWidth / 2;
		background.y = bgTile.scaledHeight / 2;
		
		SceneManager.PTR.addTile( background );
		
		// Setup buttons
		resetButton = new UIButton( "restart", onReset );
		pauseButton = new UIButton( "pause", onPause );
		resumeButton = new UIButton( "resume", onPause );
		
		pauseButton.y = pauseButton.height / 2;
		resetButton.y = pauseButton.y;
		
		pauseButton.x = ResolutionConfig.PTR.actualScreenWidth - pauseButton.width - ( pauseButton.width / 2 );
		resetButton.x = pauseButton.x - pauseButton.width - ( pauseButton.width / 2 );
		
		resumeButton.visible = false;
		resumeButton.x = pauseButton.x;
		resumeButton.y = pauseButton.y;
		
		var pauseRect:RectCollider = new RectCollider();
		pauseRect.x = pauseButton.x + ( pauseButton.width / 2 );
		pauseRect.y = pauseButton.y + ( pauseButton.height / 2 );
		pauseRect.width = pauseButton.width;
		pauseRect.height = pauseButton.height;
		
		SceneManager.PTR.registerNoClickZone( pauseRect );
		
		var resetRect:RectCollider = new RectCollider();
		resetRect.x = resetButton.x + ( resetButton.width / 2 );
		resetRect.y = resetButton.y + ( resetButton.height / 2 );
		resetRect.width = resetButton.width;
		resetRect.height = resetButton.height;
		
		SceneManager.PTR.registerNoClickZone( resetRect );
		
		// Setup gems
		purpleGem = AssetManager.PTR.getBitmap( "gem" );
		greenGem = AssetManager.PTR.getBitmap( "greengem" );
		redGem = AssetManager.PTR.getBitmap( "redgem" );
		
		purpleGem.scaleX = 0.5;
		purpleGem.scaleY = 0.5;
		greenGem.scaleX = 0.6;
		greenGem.scaleY = 0.6;
		redGem.scaleX = 0.8;
		redGem.scaleY = 0.8;
		
		SceneManager.PTR.uiLayer.addChild( purpleGem );
		SceneManager.PTR.uiLayer.addChild( greenGem );
		SceneManager.PTR.uiLayer.addChild( redGem );
		
		var gemBuffer:Float = ResolutionConfig.PTR.convertScreenXToWorld( 4 );
		
		purpleGem.x = gemBuffer + ( purpleGem.width / 2 );
		greenGem.x = purpleGem.x;
		redGem.x = purpleGem.x + purpleGem.width + ResolutionConfig.PTR.convertWorldToScreen( 168 );
		
		purpleGem.y = purpleGem.height / 2;
		greenGem.y = purpleGem.y + purpleGem.height + purpleGem.height / 2;
		redGem.y = purpleGem.y;
		
		// Setup counters
		purpleGemCount = new UICounter();
		purpleGemCount.sortOrder = SortOrders.UI_FRONT;
		purpleGemCount.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		purpleGemCount.x = purpleGem.x + purpleGem.width + purpleGem.width / 2;
		purpleGemCount.y = purpleGem.y + purpleGem.height / 2;
		purpleGemCount.setup();
		
		greenGemCount = new UICounter();
		greenGemCount.sortOrder = SortOrders.UI_FRONT;
		greenGemCount.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		greenGemCount.x = greenGem.x + greenGem.width + greenGem.width / 2;
		greenGemCount.y = greenGem.y + greenGem.height / 2;
		greenGemCount.setup();
		
		redGemCount = new UICounter();
		redGemCount.sortOrder = SortOrders.UI_FRONT;
		redGemCount.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		redGemCount.x = redGem.x + redGem.width + redGem.width / 2;
		redGemCount.y = redGem.y + redGem.height / 2;
		redGemCount.setup();
		
		comboMeter = new UIComboMeter();
		comboMeter.x = redGem.x;
		comboMeter.y = greenGem.y + ( greenGem.height / 2 ) - ( comboMeter.height / 2 );
		
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.RED_GEM_OBTAINED, onRedGemCollected );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.GREEN_GEM_OBTAINED, onGreenGemCollected );
		GameConfig.PTR.gameplayEvents.register( GameEventTypes.PURPLE_GEM_OBTAINED, onPurpleGemCollected );
		
		SceneManager.PTR.uiLayer.addChild( comboMeter );
		
		// Set all to invisible!
		resetButton.alpha = 0;
		pauseButton.alpha = 0;
		
		resetButton.mouseEnabled = false;
		pauseButton.mouseEnabled = false;
		
		purpleGem.alpha = 0;
		greenGem.alpha = 0;
		redGem.alpha = 0;
		
		purpleGemCount.alpha = 0;
		greenGemCount.alpha = 0;
		redGemCount.alpha = 0;
		
		comboMeter.alpha = 0;
		
		// Transitions
		transitions = new TransitionPlayer();
		
		transitions.addTransition( new FadeOutFXTile( background, 0, 1, 0.25 ) );
		
		transitions.doneCallback = onBGTransitionedIn;
		
		//transitions.addEventListener( Event.COMPLETE, BGTransitionedIn );
	}
	
	private function onBGTransitionedIn():Void
	{
		//transitions.removeEventListener( Event.COMPLETE, onBGTransitionedIn );
		
		transitions.clear();
		
		purpleGem.alpha = 1;
		greenGem.alpha = 1;
		redGem.alpha = 1;
		
		//transitions.addEventListener( Event.COMPLETE, onMoveElementsIn );
		transitions.doneCallback = onMoveElementsIn;
		
		// Transition these all in		
		transitions.addTransition( new MoveToFX( purpleGem, -purpleGem.width, purpleGem.y, purpleGem.x, purpleGem.y, 0.10, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( greenGem, -greenGem.width, greenGem.y, greenGem.x, greenGem.y, 0.15, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( redGem, -redGem.width, redGem.y, redGem.x, redGem.y, 0.20, LerpTypes.EASE_IN ) );
	}
	
	private function onMoveElementsIn():Void
	{
		//transitions.removeEventListener( Event.COMPLETE, onMoveElementsIn );
		
		transitions.clear();
		
		resetButton.alpha = 1;
		pauseButton.alpha = 1;
		
		resetButton.mouseEnabled = true;
		pauseButton.mouseEnabled = true;
		
		var runningTime:Float = 0.15;
		
		// All of the channels
		for ( pipe in cratePipes )
		{
			pipe.alpha = 1;
			
			transitions.addTransition( new MoveToFX( pipe, pipe.x, -pipe.height, pipe.x, pipe.y, runningTime, LerpTypes.EASE_IN ) );
			
			runningTime += 0.10;
		}
		
		purpleGemCount.alpha = 1;
		greenGemCount.alpha = 1;
		redGemCount.alpha = 1;
		
		var rightSide:Float = ResolutionConfig.PTR.actualScreenWidth;
		
		//transitions.addEventListener( Event.COMPLETE, onTransitionInDone );
		
		transitions.doneCallback = onTransitionInDone;
		
		// Transition and done
		transitions.addTransition( new MoveToFX( resetButton, rightSide + resetButton.width, resetButton.y, resetButton.x, resetButton.y, 0.10, LerpTypes.EASE_IN ) );
		transitions.addTransition( new MoveToFX( pauseButton, rightSide + pauseButton.width, pauseButton.y, pauseButton.x, pauseButton.y, 0.15, LerpTypes.EASE_IN ) );
		transitions.addTransition( new FadeOutFX( comboMeter, 0, 1, 0.15 ) );
	}
	
	private function onTransitionInDone():Void
	{
		//transitions.removeEventListener( Event.COMPLETE, onTransitionInDone );
		
		transitions.clear();
	}
	
	public function getUIElementPosition( element:String ):Point
	{
		cachedPoint.x = 0;
		cachedPoint.y = 0;
		
		if ( element == "redparticle" )
		{
			cachedPoint.x = redGem.x;
			cachedPoint.y = redGem.y;
		}
		else if ( element == "greenparticle" )
		{
			cachedPoint.x = greenGem.x;
			cachedPoint.y = greenGem.y;
		}
		else if ( element == "purpleparticle" )
		{
			cachedPoint.x = purpleGem.x;
			cachedPoint.y = purpleGem.y;
		}
		
		return cachedPoint;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		redGemCount.tick( deltaTime );
		purpleGemCount.tick( deltaTime );
		greenGemCount.tick( deltaTime );
		
		if ( quit != null )
		{
			quit.tick( deltaTime );
		}
		
		if ( transitions != null )
		{
			transitions.tick( deltaTime );
		}
	}
	
	private function onRedGemCollected( data:Dynamic ):Void
	{
		redGemCount.add( 1 );
	}
	
	private function onGreenGemCollected( data:Dynamic ):Void
	{
		greenGemCount.add( 1 );
	}
	
	private function onPurpleGemCollected( data:Dynamic ):Void
	{
		purpleGemCount.add( 1 );
	}
	
	private function onReset():Void
	{		
		GameConfig.PTR.currentLevel.reset( true );
	}
	
	private function onPause():Void
	{
		GameConfig.PTR.currentLevel.togglePause();
		
		if ( SceneManager.PTR.isPaused )
		{
			resumeButton.visible = true;
			pauseButton.visible = false;
			
			paused = AssetManager.PTR.getBitmap( "paused" );
			paused.x = ( ResolutionConfig.PTR.actualScreenWidth / 2 ) - ( paused.width / 2 );
			paused.y = ( ResolutionConfig.PTR.actualScreenHeight / 2 ) - ( paused.height / 2 );
			
			quit = new UIAnimatedButton( "quit", onQuitClicked );
			quit.x = ( ResolutionConfig.PTR.actualScreenWidth / 2 ) - ( quit.width / 2 );
			quit.y = paused.y + paused.height + ( quit.height / 2 );
			
			SceneManager.PTR.uiLayer.addChild( paused );
		}
		else
		{
			SceneManager.PTR.uiLayer.removeChild( paused );
			quit.destroy();
			paused = null;
			quit = null;
			
			resumeButton.visible = false;
			pauseButton.visible = true;
		}
	}
	
	private function onQuitClicked():Void
	{
		GameConfig.PTR.currentLevel.quit();
	}
	
	public function reset():Void
	{
		if ( paused != null )
		{
			SceneManager.PTR.uiLayer.removeChild( paused );
			quit.destroy();
			paused = null;
			quit = null;
			
			resumeButton.visible = false;
			pauseButton.visible = true;
		}
		
		redGemCount.reset();
		greenGemCount.reset();
		purpleGemCount.reset();
	}
	
	public function destroy():Void
	{
		resetButton.destroy();
		pauseButton.destroy();
		resumeButton.destroy();
		
		SceneManager.PTR.resetNoClickZones();
		
		SceneManager.PTR.uiLayer.removeChild( purpleGem );
		SceneManager.PTR.uiLayer.removeChild( greenGem );
		SceneManager.PTR.uiLayer.removeChild( redGem );
		
		redGemCount.destroy();
		greenGemCount.destroy();
		purpleGemCount.destroy();
		
		if ( paused != null )
		{
			SceneManager.PTR.uiLayer.removeChild( paused );
		}
		
		if ( quit != null )
		{
			quit.destroy();
		}
		
		if ( background != null )
		{
			SceneManager.PTR.removeTile( background );
		}
		
		for ( cratePipe in cratePipes )
		{
			SceneManager.PTR.uiLayer.removeChild( cratePipe );
		}
		
		if ( comboMeter != null )
		{
			SceneManager.PTR.uiLayer.removeChild( comboMeter );
		}
		
		SceneManager.PTR.removeTickable( this, true );
	}
}