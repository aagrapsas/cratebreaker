package game.ui.ingame;
import engine.assets.AssetManager;
import engine.render.ResolutionConfig;
import nme.display.Bitmap;
import nme.display.Sprite;

/**
 * ...
 * @author A.A. Grapsas
 */
class UIComboMeter extends Sprite
{
	private var active:Array<Bitmap>;
	private var inactive:Array<Bitmap>;
	
	private var numActive:Int = 0;
	
	private var maxTime:Float = 0;
	
	public function new() 
	{
		super();
		
		setup();
	}
	
	private function setup():Void
	{
		active = new Array<Bitmap>();
		inactive = new Array<Bitmap>();
		
		var runningX:Float = 0;
		var padding:Float = ResolutionConfig.PTR.convertWorldToScreen( 2 );
		
		for ( i in 0...5 )
		{
			active.push( AssetManager.PTR.getBitmap( "comboactive" ) );
			inactive.push( AssetManager.PTR.getBitmap( "comboinactive" ) );
			
			active[ i ].x = runningX;
			inactive[ i ].x = runningX;
			
			active[ i ].visible = false;
			inactive[ i ].visible = true;
			
			this.addChild( active[ i ] );
			this.addChild( inactive[ i ] );
			
			runningX += active[ i ].width + padding;
		}
	}
	
	public function setMaxTime( time:Float ):Void
	{
		maxTime = time;
	}
	
	public function setTimeElapsed( time:Float ):Void
	{
		var percent:Float = ( time / maxTime ) * 100;
		
		var percentBlock:Float = 100 / 5;
		
		var activeBlocks:Int = Math.floor( percent / percentBlock );
		
		if ( activeBlocks != numActive )
		{
			for ( i in 0...active.length )
			{
				if ( i < activeBlocks )
				{
					active[ i ].visible = true;
					inactive[ i ].visible = false;
				}
				else
				{
					active[ i ].visible = false;
					inactive[ i ].visible = true;
				}
			}
		}
	}
}