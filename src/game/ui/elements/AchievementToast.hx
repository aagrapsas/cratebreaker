package game.ui.elements;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;
import engine.misc.BlitText;
import engine.scene.Actor;
import engine.scene.SceneManager;
import engine.ui.UIElement;
import game.config.GameConfig;
import nme.display.Bitmap;
import nme.display.Sprite;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Set the width and height and transition it in.
 * 
 * Orientation is currently icon to the left, text rendered to the right.
 * 
 * Top left is (0, 0). Extends right and down from there.
 */
class AchievementToast extends Sprite
{
	//Style stuff
	static inline var BG_COLOR:Int = 0x333333;
	
	var icon:GameBitmap;
	var textDisplay:BlitText;
	
	var achievementText:String;
	
	var graphicWidth:Float;
	var graphicHeight:Float;
	
	var iconBitmapName:String;
	
	public function new( width:Float, height:Float, iconBitmapName:String, achievementText:String )
	{
		super();
		
		this.achievementText = achievementText;
		this.iconBitmapName = iconBitmapName;
		
		textDisplay = new BlitText();
		
		graphicWidth = width;
		graphicHeight = height;
		
		setup();
	}
	
	public function setup():Void
	{
		textDisplay.setFont( GameConfig.PTR.fontMaps.get( "num" ) );
		textDisplay.setText( achievementText );
		textDisplay.tick( 0 );
		
		graphics.lineStyle( 1 );
		graphics.beginFill( BG_COLOR );
		graphics.drawRect( 0, 0, graphicWidth, graphicHeight );
		graphics.endFill();
		
		icon = AssetManager.PTR.getGameBitmap( iconBitmapName );
		
		var bmp:Bitmap = icon.getNewBitmap();
		bmp.x = 1;
		bmp.y = 1;
		
		addChild( bmp );
		addChild( textDisplay );
		
		SceneManager.PTR.addTickable( textDisplay, true );
	}
	
	public function destroy():Void
	{
		SceneManager.PTR.removeTickable( textDisplay, true );
		
		while (numChildren > 0)
		{
			removeChildAt( 0 );
		}
	}
	
}