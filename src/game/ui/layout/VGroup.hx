package game.ui.layout;
import engine.misc.LerpTypes;
import game.ui.nongame.lab.UILabPackage;
import game.ui.transitions.MoveToFX;
import nme.events.Event;
import nme.geom.Rectangle;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Tightly packed, transitions, scrolbar, vertical group.
 */
class VGroup extends Group {

	public function new( ?attachedScroll:Scrollbar ) {
		
		attachedScroll.direction = Scrollbar.DIRECTION_VERTICAL;
		
		attachedScroll.addEventListener( Event.SCROLL , onScroll );
		
		super( attachedScroll );
	}
	
	private function onScroll(e:Event):Void
	{
		if ( scrollEnabled )
		{
			internalItemStorage.y = -scrollbar.currentValue;
		}
	}
	
	public function getItemsInScrollArea():Array<GroupItem> {
		var out:Array<GroupItem> = new Array<GroupItem>();
		
		var scrollArea:Rectangle = new Rectangle(0, 0, scrollWidth, scrollHeight);
		
		for (item in groupItems)
		{
			var itemRect:Rectangle = item.internalDisplayObject.getRect( this );
			
			var isItemInDisplay:Bool = scrollArea.intersects( itemRect );
			
			if ( isItemInDisplay )
			{
				out.push( item );
			}
		}
		
		return out;
	}
	
	override public function updateScroll():Void {
		
		var sumHeight:Float = 0;
		
		for (item in groupItems)
		{
			sumHeight += item.internalDisplayObject.height;
		}
		
		var scrollRange:Float = sumHeight - scrollHeight;
		
		if (scrollbar != null)
		{
			if ( scrollHeight < sumHeight)
			{
				scrollbar.enabled = true;
				scrollbar.dragDensity = -1;
				scrollbar.setRange(0, scrollRange);
			}
			else
			{
				scrollbar.enabled = false;
			}
		}
	}
	
	override public function doLayout():Void {
		
		groupItems.sort( rankSort );
		
		for (index in 0...groupItems.length)
		{
			var item:GroupItem = groupItems[index];
			item.groupPlaceX = 0;
			item.groupPlaceY = item.internalDisplayObject.height * item.rank;
			
			item.internalDisplayObject.x = item.groupPlaceX;
			item.internalDisplayObject.y = item.groupPlaceY;
		}
		
		transitionItemsIn();
	}
	
	//Bring in the bacon. Only tween the visible ones. The rest get set
	public function transitionItemsIn():Void
	{
		
		var visibleHeight:Float = scrollHeight;
		var visibleCount:Int = cast Math.min( Math.ceil( visibleHeight / groupItems[ 0 ].internalDisplayObject.height ), groupItems.length );
		
		var totalTime:Float = 1.8;
		var timeStep:Float = totalTime / visibleCount;
		var runningTime:Float = timeStep;
		
		#if !android
			transitions.addEventListener( Event.COMPLETE , onTransitionInComplete );
			for ( i in 0...visibleCount )
			{
				var item:GroupItem = groupItems[i];
				transitions.addTransition( new MoveToFX( item.internalDisplayObject, item.groupPlaceX, -2 * item.internalDisplayObject.height, item.groupPlaceX, item.groupPlaceY, runningTime, LerpTypes.EASE_OUT ) );
				runningTime += timeStep;
			}
		#end
	}
	
	private function onTransitionInComplete( e:Event ):Void
	{
		transitions.removeEventListener( Event.COMPLETE , onTransitionInComplete );
		
		for ( i in 0...groupItems.length )
		{
			var item:GroupItem = groupItems[i];
			if ( Std.is( item.internalDisplayObject, UILabPackage ) )
			{
				var itemAsLabPackage:UILabPackage = cast item.internalDisplayObject;
				itemAsLabPackage.showDisplay( i % 2 == 0, true );
			}
		}
		
		scrollbar.enabled = true;
	}
	
	/*
	 * item.internalDisplayObject.x = item.groupPlaceX;
	   item.internalDisplayObject.y = item.groupPlaceY;
	// Setup transitions for visible
	for ( i in 0...visibleCount )
	{
		displayed[ i ].visible = true;
		transitions.addTransition( new MoveToFX( displayed[ i ], 0, -displayed[ i ].height, 0, displayed[ i ].y, runningTime, LerpTypes.EASE_OUT ) );
		runningTime += timeStep;
	}
	*/
	
	override public function tick(deltaTime:Float):Void {
		for ( i in 0...groupItems.length )
		{
			var item:GroupItem = groupItems[i];
			if ( Std.is( item.internalDisplayObject, UILabPackage ) )
			{
				var itemAsLabPackage:UILabPackage = cast item.internalDisplayObject;
				itemAsLabPackage.tick( deltaTime );
			}
		}
	}
	
	//Sort ascending by rank
	private function rankSort( a:GroupItem, b:GroupItem ):Int
	{
		return a.rank - b.rank;
	}
	
}