package game.ui.layout;
import nme.display.DisplayObject;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class GroupItem {

	//These are where the group item ends up within a group.
	//Specify these with doLayout().
	//And then use the transition player do manipulate them.
	public var groupPlaceX:Float = 0;
	public var groupPlaceY:Float = 0;
	
	public var internalDisplayObject:DisplayObject;
	public var rank:Int;
	
	public function new(attachedItem:DisplayObject, itemRank:Int) {
		internalDisplayObject = attachedItem;
		rank = itemRank;
	}
	
}