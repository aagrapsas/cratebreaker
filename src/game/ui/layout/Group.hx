package game.ui.layout;

import engine.misc.ITickable;
import engine.scene.SceneManager;
import game.ui.transitions.TransitionPlayer;
import nme.display.DisplayObject;
import nme.display.Shape;
import nme.display.Sprite;
import nme.Lib;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * A Group is a basic layout manager. Curently uses a Dynamic and assumes it has an x and y
 * 
 * Subclasses override the doLayout function.
 * 
 * This class does NOTHING to arrange/work with your goup.
 * Use VGroup, HGroup, or make your own group, to change layout.
 * 
 * These all exist in screen space.
 */
class Group extends Sprite, implements ITickable {
	
	public var scrollbar:Scrollbar;
	
	public var scrollEnabled:Bool = true;
	
	var groupItems:Array<GroupItem>;
	
	var transitions:TransitionPlayer;
	
	var internalItemStorage:Sprite;
	
	var scrollHeight(default, null):Float;
	var scrollWidth(default, null):Float;
	
	public function new( ?attachedScroll:Scrollbar )
	{
		super();
		
		groupItems = new Array<GroupItem>();
		transitions = new TransitionPlayer();
		
		internalItemStorage = new Sprite();
		
		scrollbar = attachedScroll;
		
		SceneManager.PTR.addTickable( transitions );
		
		addChild(internalItemStorage);
	}
	
	public function setScrollRange(x:Float, y:Float, width:Float, height:Float):Void
	{
		//Update the scrollbar's drag area to be the mask area
		
		scrollHeight = height;
		scrollWidth = width;
		
		if (scrollbar != null)
		{
			scrollbar.setPosition( this.x + x + width, this.y + y );
			
			scrollbar.dragArea.x = this.x + x + width / 2;
			scrollbar.dragArea.y = this.y + y + height / 2;
			scrollbar.dragArea.width = width;
			scrollbar.dragArea.height = height;
			
			scrollbar.length = height;
		}
	}
	
	//Override this, or replace this function, to change how the group organizes its items
	public function doLayout():Void
	{
		
	}
	
	//Override this when you organize.
	//Use this to update the scrollbar's range and drag density.
	public function updateScroll():Void
	{
		
	}
	
	//Assume the item has (x, y)
	//Returns the new group item associated with the added dynamic, or null on failure.
	public function addItem( toAdd:DisplayObject, itemRank:Int = 0 ):GroupItem
	{
		var newItem:GroupItem = null;
		
		newItem = new GroupItem(toAdd, itemRank);
		
		groupItems.push(newItem);
		
		internalItemStorage.addChild(toAdd);
		
		updateScroll();
		
		return newItem;
	}
	
	public function removeItem( toRemove:GroupItem ):Bool
	{
		
		var didRemove:Bool = groupItems.remove(toRemove);
		
		if (didRemove)
		{
			updateScroll();
		}
		
		return didRemove;
	}
	
	public function destroy():Void
	{
		while (groupItems.length > 0)
		{
			var item:GroupItem = groupItems.pop();
			internalItemStorage.removeChild(item.internalDisplayObject);
		}
		
	}
	
	/* INTERFACE engine.misc.ITickable */
	
	public function tick(deltaTime:Float):Void {
		
	}
}