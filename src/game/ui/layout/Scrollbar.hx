package game.ui.layout;

import engine.collision.RectCollider;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import game.ui.transitions.FadeOutFX;
import game.ui.transitions.TimeFX;
import game.ui.transitions.TransitionPlayer;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.EventDispatcher;
import nme.events.MouseEvent;
import nme.Lib;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * A scrollbar lets you use screenspace/touch to manipulate a value between a min and max.
 * 
 * Yes, it was painful to make the {} be the way they are. I'm slowly working on getting good at this.
 * 
 * Listen for SCROLL events, or just check the currentValue.
 * 
 * This lives in screen space, as it is considered to be UI.
 * 
 * Min and max values are NOT enforced (that is, you can pass min=5 max=-5)
 * 
 */
class Scrollbar extends EventDispatcher
{
	
	public static inline var DIRECTION_HORIZONTAL:Bool = true;
	public static inline var DIRECTION_VERTICAL:Bool = false;
	
	public static inline var THUMB_FADE_DURATION:Float = 1;//	Magic number prevention system
	public static inline var THUMB_IDLE_DURATION:Float = 3;//	Magic number prevention system
	
	public var enabled:Bool = true;
	
	public var dragDensity:Float = 1;//							Scalar multiplier of dragArea events in screen space.
	public var dragArea:RectCollider;//							Lives in screen space.

	public var direction:Bool = DIRECTION_HORIZONTAL;//			Scrollbar direction.
	public var thumb:Sprite;//									Swap this out to change the thumb sprite. Fixed width graphic.
	
	private var transitions:TransitionPlayer;
	
	public var length:Float = 0;//								Length of the bar. x + length = maxValue if horizontal, y + length = maxvalue if vertical
	
	private var x:Float = 0;//									Bar information. Track is invisible, but doens't have to be?
	private var y:Float = 0;
	private var minimumValue:Float = 0;
	private var maximumValue:Float = 0;
	private var _currentValue:Float = 0;
	
	public var isDragging(default, null):Bool = false;
	
	private var lastX:Float = 0;
	private var lastY:Float = 0;
	
	private var isDestroied:Bool = false;
	private var useThumbDrag:Bool = false;
	
	public function new( scrollThumb:Sprite,  ?dragDensity:Float, ?scrollDirection:Bool )
	{
		super();
		
		if ( scrollDirection != null ) direction = scrollDirection;
		if ( dragDensity != null ) this.dragDensity = dragDensity;
		thumb = scrollThumb;
		thumb.alpha = 0;
		transitions = new TransitionPlayer();
		transitions.addEventListener(Event.COMPLETE, transitionComplete);
		
		dragArea = new RectCollider();
		
		SceneManager.PTR.addTickable(transitions);
		
		Lib.stage.addEventListener(MouseEvent.MOUSE_UP, onScreenTouch);
		Lib.stage.addEventListener(MouseEvent.MOUSE_MOVE, onScreenTouch);
		Lib.stage.addEventListener(MouseEvent.MOUSE_DOWN, onScreenTouch);
		
		thumb.addEventListener(MouseEvent.MOUSE_DOWN, thumbTouch);
	}
	
	public function setup( length:Float, minValue:Float, maxValue:Float, ?initialValue:Float )
	{
		this.length = length;
		setRange(minValue, maxValue, false);
		if (initialValue != null) setCurrentValue(initialValue);
	}
	
	private function thumbTouch( e:MouseEvent ):Void
	{
		//The alpha check is a tolerance. You don't want to accidentally hit the scroll thumb. That's the worst.
		if (thumb.alpha >= 0.8)
		{
			useThumbDrag = true;
			isDragging = true;
			lastX = e.stageX;
			lastY = e.stageY;
		}
	}
	
	private function onScreenTouch( e:MouseEvent ):Void
	{
		if ( e.type == MouseEvent.MOUSE_DOWN && enabled )
		{
			if ( dragArea.doesAABBContainPoint( e.stageX, e.stageY ) )
			{
				thumbIn();
				isDragging = true;
				lastX = e.stageX;
				lastY = e.stageY;
			}
		}
		else if ( e.type == MouseEvent.MOUSE_UP )
		{
			if ( isDragging )
			{
				tumbFadeDelay();
			}
			useThumbDrag = false;
			isDragging = false;
		}
		else if ( e.type == MouseEvent.MOUSE_MOVE && enabled )
		{
			if ( isDragging )
			{
				var delta:Float = 0;
				if ( direction == DIRECTION_HORIZONTAL )
				{
					delta = ( e.stageX - lastX );
				}
				else
				{
					delta = ( e.stageY - lastY );
				}
				
				lastX = e.stageX;
				lastY = e.stageY;
				
				var scrollDeltaDensity:Float = dragDensity;
				
				//If we're dragging the thumb, change scroll density
				if ( useThumbDrag )
				{
					//These are to prevent thumbscrolling past the actual bounds.
					if ( direction == DIRECTION_HORIZONTAL)
					{
						if (lastX < x) lastX = x;
						if (lastX > x + length) lastX = x + length;
					}
					else
					{
						if (lastY < y) lastY = y;
						if (lastY > y + length) lastY = y + length;
					}
					
					scrollDeltaDensity = ((maximumValue - minimumValue) / length);
					
				}
				
				delta *= scrollDeltaDensity;
				
				if ( delta != 0 )
				{
					setCurrentValue(_currentValue + delta);
					dispatchEvent( new Event( Event.SCROLL ) );
				}
			}
		}
	}
	
	private function tumbFadeDelay():Void
	{
		transitions.addTransition( new TimeFX( THUMB_IDLE_DURATION ) );
	}
	
	private function thumbOut():Void
	{
		transitions.clear();
		transitions.addTransition( new FadeOutFX( thumb, thumb.alpha, 0, thumb.alpha * THUMB_FADE_DURATION ) );
	}
	
	private function thumbIn():Void
	{
		transitions.clear();
		transitions.addTransition( new FadeOutFX( thumb, thumb.alpha, 1, (1 - thumb.alpha) * THUMB_FADE_DURATION ) );
	}
	
	private function transitionComplete( e:Event ):Void
	{
		
		if ( isDestroied )
		{
			transitions.removeEventListener(Event.COMPLETE, transitionComplete);
			SceneManager.PTR.removeTickable(transitions);
		}
		else if ( isDragging == false && thumb.alpha > 0 )
		{
			thumbOut();
		}
	}
	
	/* Change the min/max ranage of the scrollbar.
	 * Example, Range = [0, 100], currentvalue = 50, after setRange(50, 100, false), Range = [50, 100], currentvalue = 75
	 * Example, Range = [0, 100], currentvalue = 50, after setRange(50, 100, true), Range = [50, 100], currentvalue = 50
	 * Example, Range = [0, 100], currentvalue = 50, after setRange(0, 10, true), Range = [0, 10], currentvalue = 10
	 * 
	 * Dispatches a CHANGE event if _currentValue changes
	 */
	public function setRange( minValue:Float, maxValue:Float, fixCurrentValue:Bool = false ):Void
	{
		var oldRange:Float = maximumValue - minimumValue;
		var oldRatio:Float = ( _currentValue - minValue ) / oldRange;
		var newRange:Float = maxValue - minValue;
		var oldCurrentValue:Float = _currentValue;
		minimumValue = minValue;
		maximumValue = maxValue;
		
		if ( !fixCurrentValue && !Math.isNaN( oldRatio ) )
		{
			setCurrentValue( minimumValue + newRange * oldRatio );
		}
		else
		{
			setCurrentValue( _currentValue );
		}
		
		//Dispatch an update if _currentValue is different from before
		if ( oldCurrentValue != _currentValue )
		{
			dispatchEvent( new Event( Event.SCROLL ) );
		}
		update();
	}
	
	//Relative to the top left of the scrollbar. This is the position of the scrolbar, not the thumb.
	public function setPosition( ?x:Float, ?y:Float ):Void
	{
		if (x != null) this.x = x;
		if (y != null) this.y = y;
		update();
	}
	
	//Updates the graphics
	public function update():Void
	{
		var hasThumb:Bool = (thumb != null);
		var valueRatio:Float = (_currentValue - minimumValue) / (maximumValue - minimumValue);
		
		if ( hasThumb )
		{
			if ( direction == DIRECTION_HORIZONTAL )
			{
				thumb.x = x + length * valueRatio - thumb.width / 2;
				thumb.y = y - thumb.height / 2;
			}
			else
			{
				thumb.x = x - thumb.width / 2;
				thumb.y = y + length * valueRatio - thumb.height / 2;
			}
		}
	}
	
	private function getCurrentValue():Float
	{
		return _currentValue;
	}
	
	private function setCurrentValue( newValue:Float ):Float
	{
		var oldValue:Float = _currentValue;
		
		_currentValue = newValue;
		_currentValue = Math.max( Math.min( _currentValue, maximumValue ), minimumValue );
		
		var hasChanged:Bool = oldValue != _currentValue;
		if (hasChanged)
		{
			update();
			dispatchEvent( new Event( Event.SCROLL ) );
		}
		return _currentValue;
	}
	
	public var currentValue( getCurrentValue, setCurrentValue ):Float;
	
	public function destroy():Void
	{
		isDestroied = true;
		enabled = false;
		
		thumbOut();
		
		Lib.stage.removeEventListener(MouseEvent.MOUSE_UP, onScreenTouch);
		Lib.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onScreenTouch);
		Lib.stage.removeEventListener(MouseEvent.MOUSE_DOWN, onScreenTouch);
	}
}