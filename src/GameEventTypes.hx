package ;

/**
 * ...
 * @author A.A. Grapsas
 */

class GameEventTypes 
{
	public static inline var RED_GEM_OBTAINED:String = "red gem";
	public static inline var GREEN_GEM_OBTAINED:String = "green gem";
	public static inline var PURPLE_GEM_OBTAINED:String = "purple gem";
	public static inline var GAME_END:String = "end";
	public static inline var GAME_START:String = "start";
	public static inline var GAME_CANCELED:String = "cancel";
	
	public static inline var CRATE_SMASHED:String = "smash";	// Crate destroyed in any way
	
	public static inline var CRATE_EXTERNALLY_DESTROYED:String = "external";	// Crate destroyed "externally" (explosion, etc.)
	public static inline var CRATE_CLICK_DESTROYED:String = "click";	// Crate destroyed by clickin
	public static inline var CRATE_CHAIN_DESTROYED:String = "chain";	// Crate destroyed as part of chain
	public static inline var CHAIN_LENGTH:String = "length";			// Length of just executed chain, seds as Int
	
	public static inline var ACHIEVEMENT_UNLOCKED:String = "achievement";
}