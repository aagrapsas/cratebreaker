package engine.assets;
import engine.render.TileRenderer;
import nme.Assets;
import nme.display.Bitmap;
import nme.Lib;

/**
 * ...
 * @author A.A. Grapsas
 */

class AssetManager
{
	public static var PTR:AssetManager;
	
	public var tileRenderers:Hash<TileRenderer>;
	public var tileRenderersOrdered:Array<TileRenderer>;
	
	private var manifest:Hash<String>;
	
	private var bitmaps:Hash<GameBitmap>;
	
	public function new() 
	{
		PTR = this;
		
		manifest = new Hash<String>();
		tileRenderers = new Hash<TileRenderer>();
		tileRenderersOrdered = new Array<TileRenderer>();
	}
	
	public function setupManifest( spritesheets:Array<SpriteSheet> ):Void
	{
		bitmaps = new Hash<GameBitmap>();
		
		// Load all spritesheets
		for ( spritesheet in spritesheets )
		{
			spritesheet.generate();
			
			var sheetMaps:Hash<GameBitmap> = spritesheet.mapped;
			
			// Iterate all cut up bitmaps and add them to the asset manager
			for ( gameBitmap in sheetMaps )
			{
				bitmaps.set( gameBitmap.name, gameBitmap );
			}
			
			if ( spritesheet.tileRenderer != null )
			{
				tileRenderers.set( spritesheet.sheetID, spritesheet.tileRenderer );
				tileRenderersOrdered.push( spritesheet.tileRenderer );
			}
		}
	}
	
	public function getBitmap( name:String ):Bitmap
	{
		var gameBitmap:GameBitmap = bitmaps.get( name );
		
		return gameBitmap.getNewBitmap();
	}
	
	public function getGameBitmap( name:String ):GameBitmap
	{
		return bitmaps.get( name );
	}
}