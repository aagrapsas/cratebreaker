package engine.assets;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.render.ResolutionConfig;
import engine.render.TileRenderer;
import engine.render.tilestorage.ITileStorage;
import haxe.Json;
import haxe.xml.Fast;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.BlendMode;
import nme.display.Graphics;
import nme.display.PixelSnapping;
import nme.display.Tilesheet;
import nme.geom.Matrix;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
 * ...
 * @author A.A. Grapsas
 */

class SpriteSheet 
{
	private var json:String;
	private var data:BitmapData;
	private var isScaledUniformly:Bool;
	private var isScaled:Bool;
	private var isTileSheet:Bool;
	private var tileStorage:ITileStorage;
	
	public var mapped:Hash<GameBitmap>;
	
	public var tileRenderer:TileRenderer;
	
	public var sheetID:String;
	
	public function new( sheetID:String, json:String, data:BitmapData, isScaled:Bool, isScaledUniformly:Bool, isTileSheet:Bool, tileStorage:ITileStorage ) 
	{
		this.isTileSheet = isTileSheet;
		this.sheetID = sheetID;
		this.json = json;
		this.data = data;
		this.isScaled = isScaled;
		this.isScaledUniformly = isScaledUniformly;
		this.tileStorage = tileStorage;
	}
	
	public function generate():Void
	{
		mapped = new Hash<GameBitmap>();
		
		var bitmapData:BitmapData = data;
		
		var scaleX:Float = 1;
		var scaleY:Float = 1;
		
		if ( ResolutionConfig.PTR.uniformScaleFactor != 1 )
		{
			if ( isScaled )
			{
				if ( isScaledUniformly )
				{
					scaleX = 1 / ResolutionConfig.PTR.uniformScaleFactor;
					scaleY = 1 / ResolutionConfig.PTR.uniformScaleFactor;
				}
				else
				{
					scaleX = 1 / ResolutionConfig.PTR.nonuniformScaleX;
					scaleY = 1 / ResolutionConfig.PTR.nonuniformScaleY;
				}
			}
			
			// Scale across the sheet
			bitmapData = new BitmapData( Std.int( data.width * scaleX ), Std.int( data.height * scaleY ) );
			
			var transform:Matrix = new Matrix();
			transform.scale( scaleX, scaleY );
			
			var bitmap:Bitmap = new Bitmap( data, PixelSnapping.AUTO, true );
			
			bitmapData.fillRect( bitmapData.rect, 0 );
			
			// Slow path draw
			bitmapData.draw( bitmap, transform, bitmap.transform.colorTransform, null, null, true );
		}
		
		var jsonObj:Dynamic = Json.parse( this.json );
		
		var point:Point = new Point( 0, 0 );
		
		var frames:Array<Dynamic> = cast jsonObj.frames;
		
		if ( isTileSheet )
		{
			tileRenderer = new TileRenderer( bitmapData, tileStorage );
			tileRenderer.id = sheetID;
		}
		
		for ( spriteObj in frames )
		{
			var rect:Rectangle = new Rectangle();
			
			var name:String = spriteObj.filename;
			name = name.substr( 0, name.length - 4 );
			var offsetX:Int = Std.parseInt( spriteObj.frame.x );
			var offsetY:Int = Std.parseInt( spriteObj.frame.y );
			var width:Int = Std.parseInt( spriteObj.frame.w );
			var height:Int = Std.parseInt( spriteObj.frame.h );

			rect.x = offsetX * scaleX;
			rect.y = offsetY * scaleY;
			rect.width = width * scaleX;
			rect.height = height * scaleY;
			
			var spriteData:BitmapData = new BitmapData( Std.int( rect.width ), Std.int( rect.height ) );
		
			spriteData.lock();
			
			spriteData.fillRect( spriteData.rect, 0 );
			
			spriteData.copyPixels( bitmapData, rect, point, null, null, true );
			
			spriteData.unlock();
			
			var gameBitmap:GameBitmap = new GameBitmap();
			gameBitmap.setup( spriteData, width, height, rect.width, rect.height );
			gameBitmap.name = name;
			
			gameBitmap.isUniformlyScaled = isScaledUniformly;
			
			// @TODO: possibly do not slice up images if they are tiles (to reduce load times!)
			if ( isTileSheet )
			{
				var index:Float = tileRenderer.addRenderable( rect );
				gameBitmap.isTile = true;
				gameBitmap.tileRenderer = tileRenderer;
				gameBitmap.tileID = index;
				
				// Debug.log( DebugChannels.DEBUG, "Assigning id " + index + " to " + name );
			}
			
			mapped.set( name, gameBitmap );
		}
		
		// Release data so it can, eventually, be garbage collected
		this.data = null;
	}
}