package engine.assets;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.render.GameTile;
import engine.render.TileRenderer;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.PixelSnapping;

/**
 * ...
 * @author A.A. Grapsas
 */

class GameBitmap 
{
	public var bitmapData:BitmapData;
	public var originalWidth:Float = 0;
	public var originalHeight:Float = 0;
	public var name:String;
	public var isUniformlyScaled:Bool = true;
	
	public var scaledWidth:Float = 0;
	public var scaledHeight:Float = 0;
	
	public var isTile:Bool = false;
	public var tileID:Float = 0;
	public var tileRenderer:TileRenderer;
	
	public function new() 
	{
		
	}
	
	public function setup( bitmapData:BitmapData, originalWidth:Float, originalHeight:Float, scaledWidth:Float, scaledHeight:Float ):Void
	{
		this.bitmapData = bitmapData;
		this.originalHeight = originalHeight;
		this.originalWidth = originalWidth;
		this.scaledWidth = scaledWidth;
		this.scaledHeight = scaledHeight;
	}
	
	public function getNewBitmap():Bitmap
	{
		return new Bitmap( bitmapData.clone(), PixelSnapping.AUTO, true );
	}
	
	public function getTile():GameTile
	{		
		// Debug.log( DebugChannels.DEBUG, "Creating tile with id " + tileID + " on sheet " + tileRenderer.id );
		var tile:GameTile = tileRenderer.getNewTile( tileID );
		
		tile.asset = name;
		
		#if flash
		tile.bitmap = getNewBitmap();
		#end
		
		return tile;
	}
}