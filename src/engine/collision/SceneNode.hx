package engine.collision;
import engine.misc.GenericPool;
import engine.misc.IPoolable;
import engine.scene.Actor;

/**
 * ...
 * @author A.A. Grapsas
 */

class SceneNode implements IPoolable
{
	public var collisionNode:QuadTreeNode;
	public var collisionComponent:RectCollider;
	public var internalObject:Actor;
	public var pool:GenericPool;
	
	public function new()
	{
		
	}
	
	public function remove():Void
	{
		collisionNode.remove( this );
	}
	
	public function syncPosition():Void
	{
		if ( collisionNode != null )
		{
			collisionNode.syncPosition( this );
		}
	}
	
	public function getPool():GenericPool { return pool; }
	public function setPool( value:GenericPool ):Void { pool = value; }
	
	public function suspend():Void
	{
		internalObject = null;
		collisionComponent = null;
		collisionNode = null;
	}
	
	public function resurrect():Void
	{
		
	}
}