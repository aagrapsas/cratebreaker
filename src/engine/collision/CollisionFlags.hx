package engine.collision;

/**
 * ...
 * @author A.A. Grapsas
 */

class CollisionFlags 
{
	public static var WORLD_UI:Int = 1;
    public static var ACTORS:Int = 2;
    public static var NON_COLLIDERS:Int = 4;
    public static var ALL:Int = ACTORS | NON_COLLIDERS | WORLD_UI;
}