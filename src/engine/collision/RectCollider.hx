package engine.collision;
import engine.misc.MathUtility;
import engine.render.ResolutionConfig;
import nme.display.Graphics;

/**
 * ...
 * @author A.A. Grapsas
 */

class RectCollider 
{
	public var x:Float;	// center x
	public var y:Float; // center y
	public var width:Float;
	public var height:Float;
	
	public var left( getLeft, null ):Float;
	public var right( getRight, null ):Float;
	public var top( getTop, null ):Float;
	public var bottom( getBottom, null ):Float;
	
	public var collisionFlag:Int;
	
	public var offsetX:Float = 0;
	public var offsetY:Float = 0;
	public var isLoose:Bool = true;
	
	public function new() 
	{
		
	}
	
	public function doesAABBCollide( collider:RectCollider ):Bool
	{
		if ( ( collider.collisionFlag & this.collisionFlag ) == 0 )
		{
			return false;
		}
		
		if ( collider.bottom < this.top || collider.top > this.bottom || collider.right < this.left || collider.left > this.right )
		{
			return false;
		}
		
		return true;
	}
	
	public function doesAABBFitIn( collider:RectCollider ):Bool
	{
		if ( ( collider.collisionFlag & this.collisionFlag ) == 0 )
		{
			return false;
		}
		
		return ( this.left >= collider.left && this.right <= collider.right && this.top >= collider.top && this.bottom <= collider.bottom );
	}
	
	public function doesAABBContainPoint( x:Float, y:Float ):Bool
	{
		return ( x >= this.left && x <= this.right && y >= this.top && y <= this.bottom );
	}
	
	public function drawDebug( graphics:Graphics, color:Int ):Void
	{
		graphics.lineStyle( 1, color );
		
		// Convert to appropriate screen coordinates
		graphics.drawRect( left, top, width, height );
	}
	
	public function getLineIntersection( x1:Float, y1:Float, x2:Float, y2:Float, collisionFlag:Int ):CollisionInfo
	{
		if ( ( collisionFlag & this.collisionFlag ) == 0 )
		{
			return new CollisionInfo( 0, 0, false );
		}
		
		var topCollision:CollisionInfo = MathUtility.getLineIntersection( x1, y1, x2, y2, left, top, right, top );
		
		if ( topCollision.DidCollide )
		{
			return topCollision;
		}
		
		var bottomCollision:CollisionInfo = MathUtility.getLineIntersection( x1, y1, x2, y2, left, bottom, right, bottom );
		
		if ( bottomCollision.DidCollide )
		{
			return bottomCollision;
		}
		
		var leftCollision:CollisionInfo = MathUtility.getLineIntersection( x1, y1, x2, y2, left, top, left, bottom );
		
		if ( leftCollision.DidCollide )
		{
			return leftCollision;
		}
		
		var rightCollision:CollisionInfo = MathUtility.getLineIntersection( x1, y1, x2, y2, right, top, right, bottom );
		
		return rightCollision;
	}
	
	public function getDiagonalLength():Float
	{
		return Math.sqrt( Math.pow( left - right, 2 ) + Math.pow( top - bottom, 2 ) );
	}
	
	private function getLeft():Float { return x - width / 2; }
	private function getRight():Float { return x + width / 2; }
	private function getTop():Float { return y - height / 2; }
	private function getBottom():Float { return y + height / 2; }
}