package engine.collision;

/**
 * ...
 * @author A.A. Grapsas
 */

class CollisionInfo 
{
	public var X:Float;
	public var Y:Float;
	public var DidCollide:Bool;
	
	public function new( x:Float, y:Float, didCollide:Bool ) 
	{
		X = x;
		Y = y;
		DidCollide = didCollide;
	}   
}