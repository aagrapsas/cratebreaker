package engine.collision;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.GenericPool;
import engine.misc.IPoolable;
import nme.display.Graphics;

/**
 * ...
 * @author A.A. Grapsas
 */

class QuadTreeNode implements IPoolable
{
	// Constants defining behavior
	private static var NORTH_EAST:Int = 0;
	private static var SOUTH_EAST:Int = 1;
	private static var SOUTH_WEST:Int = 2;
	private static var NORTH_WEST:Int = 3;
	private static var MIN_DIMENSIONS:Int = 10;
	private static var MAX_CONTENTS:Int = 1;
	
	// Public data interface
	public var children:Array<QuadTreeNode>;
	public var collisionComponent:RectCollider;
	public var parent:QuadTreeNode;
	public var level:Int;
	
	// Private data
	private var contents:Array<SceneNode>;
	private var nodePool:GenericPool;
	private var canSubdivide:Bool;
	private var isLeaf:Bool;
	
	public function new() 
	{
		
	}
	
	public function firstSetup():Void
	{
		children = new Array<QuadTreeNode>();
		contents = new Array<SceneNode>();
		
		children.push( null );
		children.push( null );
		children.push( null );
		children.push( null );
	}
	
	public function initialize( collision:RectCollider, parent:QuadTreeNode, level:Int, nodePool:GenericPool ):Void
	{
		this.canSubdivide = ( collision.width / 4 ) >= MIN_DIMENSIONS && ( collision.height / 4) >= MIN_DIMENSIONS;
		this.collisionComponent = collision;
		this.parent = parent;
		this.level = level;
		this.nodePool = nodePool;
		this.isLeaf = true;
	}
	
	public function getColliders( node:SceneNode ):Array<SceneNode>
	{
		var colliderSet:Array<SceneNode> = new Array<SceneNode>();
		
		if ( contents.length > 0 )
		{
			for ( potential in contents )
			{
				if ( potential == node )
					continue;
				
				if ( potential.collisionComponent.doesAABBCollide( node.collisionComponent ) )
				{
					colliderSet.push( potential );
				}
			}
		}
		
		for ( child in children )
		{
			if ( child != null && child.collisionComponent.doesAABBCollide( node.collisionComponent ) )
			{
				// @NOTE: recursing through the tree! Could optimize!
				var childColliders:Array<SceneNode> = child.getColliders( node );
				
				for ( childCollider in childColliders )
				{
					colliderSet.push( childCollider );
				}
			}
		}
		
		return colliderSet;
	}
	
	public function getLineColliders( originator:SceneNode, x1:Float, y1:Float, x2:Float, y2:Float, flag:Int ):Array<SceneNode>
	{
		var colliderSet:Array<SceneNode> = new Array<SceneNode>();
		
		if ( contents.length > 0 )
		{
			for ( potential in contents )
			{
				if ( potential == originator )
				{
					continue;
				}
				
				if ( potential.collisionComponent.getLineIntersection( x1, y1, x2, y2, flag ).DidCollide )
				{
					colliderSet.push( potential );
				}
			}
		}
		
		for ( child in children )
		{
			if ( child != null && child.collisionComponent.getLineIntersection( x1, y1, x2, y2, flag ).DidCollide )
			{
				var childColliders:Array<SceneNode> = child.getLineColliders( originator, x1, y1, x2, y2, flag );
				
				for ( childCollider in childColliders )
				{
					colliderSet.push( childCollider );
				}
			}
		}
		
		return colliderSet;
	}
	
	public function getPointColliders( x:Float, y:Float ):Array<SceneNode>
	{
		var colliderSet:Array<SceneNode> = new Array<SceneNode>();
		
		if ( contents.length > 0 )
		{
			for ( potential in contents )
			{				
				if ( potential.collisionComponent.doesAABBContainPoint( x, y ) )
				{
					colliderSet.push( potential );
				}
			}
		}
		
		for ( child in children )
		{
			if ( child != null && child.collisionComponent.doesAABBContainPoint( x, y ) )
			{
				var childColliders:Array<SceneNode> = child.getPointColliders( x, y );
				
				for ( childCollider in childColliders )
				{
					colliderSet.push( childCollider );
				}
			}
		}
		
		return colliderSet;
	}
	
	public function add( node:SceneNode ):Void
	{
		if ( contents.length >= MAX_CONTENTS && canSubdivide )
		{
			divide();
		}
		
		if ( isLeaf )
		{
			contents.push( node );
			node.collisionNode = this;
		}
		else
		{
			var overlapIndex:Int = 0;
			var overlapCount:Int = 0;
			
			for ( index in 0...children.length )
			{
				if ( children[ index ] != null && children[ index ].collisionComponent.doesAABBCollide( node.collisionComponent ) )
				{
					overlapIndex = index;
					overlapCount++;
				}
			}
			
			if ( overlapCount > 1 )
			{
				contents.push( node );
				node.collisionNode = this;
			}
			else if ( overlapCount == 1 )
			{
				children[ overlapIndex ].add( node );
			}
		}
	}
	
	private function divide():Void
	{
		if ( canSubdivide == false || isLeaf == false )
		{
			return;
		}
		
		var quarterWidth:Float = collisionComponent.width / 4;
		var quarterHeight:Float = collisionComponent.height / 4;
		var halfWidth:Float = collisionComponent.width / 2;
		var halfHeight:Float = collisionComponent.height / 2;
		
		children[ NORTH_EAST ] = getNewQuadTreeNode( collisionComponent.x + quarterWidth, collisionComponent.y - quarterHeight, halfWidth, halfHeight, this, level + 1 );
		children[ SOUTH_EAST ] = getNewQuadTreeNode( collisionComponent.x + quarterWidth, collisionComponent.y + quarterHeight, halfWidth, halfHeight, this, level + 1 );
		children[ SOUTH_WEST ] = getNewQuadTreeNode( collisionComponent.x - quarterWidth, collisionComponent.y + quarterHeight, halfWidth, halfHeight, this, level + 1 );
		children[ NORTH_WEST ] = getNewQuadTreeNode( collisionComponent.x - quarterWidth, collisionComponent.y - quarterHeight, halfWidth, halfHeight, this, level + 1 );
		
		isLeaf = false;
	}
	
	private function getNewQuadTreeNode( x:Float, y:Float, width:Float, height:Float, parent:QuadTreeNode, level:Int ):QuadTreeNode
	{
		var newNode:QuadTreeNode = cast( nodePool.pop(), QuadTreeNode );
		
		newNode.collisionComponent.x = x;
		newNode.collisionComponent.y = y;
		newNode.collisionComponent.width = width;
		newNode.collisionComponent.height = height;
		newNode.collisionComponent.collisionFlag = CollisionFlags.ALL;
		newNode.initialize( newNode.collisionComponent, parent, level, nodePool );
		
		return newNode;
	}
	
	public function syncPosition( node:SceneNode ):Void
	{
		contents.remove( node );
		
		var isNodedAdded:Bool = false;
		
		if ( node.collisionComponent.doesAABBFitIn( this.collisionComponent ) )
		{
			add( node );
			isNodedAdded = true;
		}
		else
		{
			var prevQuadNode:QuadTreeNode = this.parent;
			
			while ( prevQuadNode != null )
			{
				if ( node.collisionComponent.doesAABBFitIn( prevQuadNode.collisionComponent ) )
				{
					prevQuadNode.add( node );
					isNodedAdded = true;
					break;
				}
				
				prevQuadNode = prevQuadNode.parent;
			}
			
			if ( isNodedAdded == false )
			{
				//Debug.log( DebugChannels.ERROR, "An actor has moved beyond the maxiumum world coordinates with x: " + node.collisionComponent.x + " / y: " + node.collisionComponent.y );
			}
			
			prevQuadNode = this;
			
			while ( prevQuadNode != null )
			{
				if ( prevQuadNode.getChildrenContentCount() == 0 )
				{
					prevQuadNode.freeChildren();
				}
				else
				{
					break;
				}
				
				prevQuadNode = prevQuadNode.parent;
			}
		}
	}
	
	public function remove( node:SceneNode ):Void
	{
		contents.remove( node );
	}
	
	public function drawDebug( graphics:Graphics, color:Int ):Void
	{
		if ( isLeaf )
		{
			this.collisionComponent.drawDebug( graphics, color );
		}
		
		for ( content in contents )
		{
			content.collisionComponent.drawDebug( graphics, 0xFFFF33 );
		}
		
		for ( child in children )
		{
			if ( child != null )
			{
				child.drawDebug( graphics, color );
			}
		}
	}
	
	public function getContentCount():Int { return contents.length; }
	
	public function getChildrenContentCount():Int
	{
		var count:Int = contents.length;
		
		for ( child in children )
		{
			if ( child != null )
			{
				count += child.getChildrenContentCount();
			}
		}
		
		return count;
	}
	
	public function freeChildren():Void
	{
		isLeaf = true;
		
		for ( i in 0...children.length )
		{
			if ( children[ i ] != null )
			{
				children[ i ].free();
			}
			
			children[ i ] = null;
		}
	}
	
	public function free():Void
	{
		nodePool.free( this );
	}
	
	public function freeRecursive():Void
	{
		for ( i in 0...children.length )
		{
			if ( children[ i ] != null )
			{
				children[ i ].freeRecursive();
			}
			
			children[ i ] = null;
		}
		
		free();
	}
	
	public function resurrect():Void
	{
		
	}
	
	public function suspend():Void
	{
		
	}
	
	public function getPool():GenericPool { return nodePool; }
	public function setPool( value:GenericPool ):Void { nodePool = value; }
	
	public function destroy():Void
	{
		
	}
}