package engine.scene;
import engine.assets.AssetManager;
import engine.collision.CollisionFlags;
import engine.collision.QuadTreeNode;
import engine.collision.RectCollider;
import engine.collision.SceneNode;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.GenericPool;
import engine.misc.IPoolable;
import engine.misc.ITickable;
import engine.misc.ITouchable;
import engine.render.GameTile;
import engine.render.ResolutionConfig;
import engine.render.TileRenderer;
import game.config.GameConfig;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.Lib;

/**
 * ...
 * @author A.A. Grapsas
 */

class SceneManager 
{
	public static var PTR:SceneManager;
	
	public var elapsedTime( default, null ):Float;			// FOR GAMEPLAY
	public var engineElapsedTime( default, null ):Float;	// FOR ENGINE USE -- does not get slowed by slomo
	
	public var display:Sprite;
	public var backgroundLayer:Sprite;
	public var tileLayer:Sprite;
	public var uiLayer:Sprite;
	
	#if flash
	public var renderBuffer:Bitmap;
	#end
	
	public var isPaused:Bool = false;
	public var slomoValue:Float = 1.0;
	public var doesAllowSceneInput:Bool = true;
	
	private var actors:Array<Actor>;
	private var actorsToRemove:Array<Actor>;
	private var actorsToAdd:Array<Actor>;
	
	private var tickables:Array<ITickable>;
	private var tickablesToRemove:Array<ITickable>;
	
	private var uiTickables:Array<ITickable>;
	private var uiTickablesToRemove:Array<ITickable>;
	
	private var collision:QuadTreeNode;	// Root node for quad tree
	
	private var cachedNode:SceneNode;
	
	private var isMouseDown:Bool = false;
	
	private var noClickZones:Array<RectCollider>;
	
	public function new() 
	{		
		PTR = this;
		
		display = new Sprite();
		backgroundLayer = new Sprite();
		tileLayer = new Sprite();
		uiLayer = new Sprite();
		
		display.addChild( backgroundLayer );
		display.addChild( tileLayer );
		display.addChild( uiLayer );
		
		cachedNode = new SceneNode();
		cachedNode.collisionComponent = new RectCollider();
		
		noClickZones = new Array<RectCollider>();
		
		#if flash
		renderBuffer = new Bitmap();
		renderBuffer.bitmapData = new BitmapData( cast ResolutionConfig.PTR.actualScreenWidth, cast ResolutionConfig.PTR.actualScreenHeight );
		tileLayer.addChild( renderBuffer);
		#end
		
		// Actors
		actors = new Array<Actor>();
		actorsToRemove = new Array<Actor>();
		actorsToAdd = new Array<Actor>();
		
		// Tickables
		tickables = new Array<ITickable>();
		tickablesToRemove = new Array<ITickable>();
		uiTickables = new Array<ITickable>();
		uiTickablesToRemove = new Array<ITickable>();
		
		elapsedTime = 0;
		
		display.addEventListener( Event.ADDED_TO_STAGE, onAdded );
		
		Lib.current.stage.addEventListener( MouseEvent.MOUSE_DOWN, onDown );
		Lib.current.stage.addEventListener( MouseEvent.MOUSE_UP, onUp );
	}
	
	private function onDown( e:MouseEvent ):Void
	{
		if ( isPaused || doesAllowSceneInput == false )
		{
			return;
		}
		
		isMouseDown = true;
		
		var mouseX:Float = ResolutionConfig.PTR.convertScreenXToWorld( e.stageX );
		var mouseY:Float = ResolutionConfig.PTR.convertScreenYToWorld( e.stageY );
		
		for ( noClick in noClickZones )
		{
			if ( noClick.doesAABBContainPoint( mouseX, mouseY ) )
			{
				isMouseDown = false;
				return;
			}
		}
		
		var colliders:Array<SceneNode> = collision.getPointColliders( mouseX, mouseY );
		
		for ( collider in colliders )
		{
			if ( Std.is( collider.internalObject, ITouchable ) )
			{
				var touchable:ITouchable = cast collider.internalObject;
				
				touchable.handleTap();
			}
		}
	}
	
	private function onUp( e:MouseEvent ):Void
	{
		isMouseDown = false;
	}
	
	private function onAdded( e:Event ):Void
	{
		display.removeEventListener( Event.ADDED_TO_STAGE, onAdded );
		display.addEventListener( Event.ENTER_FRAME, update );
	}
	
	public function initializeCollision( screenWidth:Int, screenHeight:Int, poolSize:Int ):Void
	{
		if ( collision != null )
		{
			collision.freeRecursive();
		}
		
		var collider:RectCollider = new RectCollider();
		collider.x = ResolutionConfig.PTR.expectedScreenWidth / 2;	// Center screen is center of collision area
		collider.y = ResolutionConfig.PTR.expectedScreenHeight / 2;
		collider.width = ResolutionConfig.PTR.expectedScreenWidth;
		collider.height = ResolutionConfig.PTR.expectedScreenHeight;
		collider.collisionFlag = CollisionFlags.ALL;
		
		var pool:GenericPool = new GenericPool( "quads", createQuadNode, 200, poolSize );
		
		collision = new QuadTreeNode();
		collision.firstSetup();
		collision.initialize( collider, null, 0, pool );
	}
	
	private function createQuadNode():IPoolable
	{
		var node:QuadTreeNode = new QuadTreeNode();
		node.collisionComponent = new RectCollider();
		node.firstSetup();
		
		return node;
	}
	
	public function addTile( tile:GameTile ):Void
	{
		var renderer:TileRenderer = AssetManager.PTR.tileRenderers.get( tile.layer );
		
		renderer.addTile( tile );
	}
	
	public function removeTile( tile:GameTile ):Void
	{
		var renderer:TileRenderer = AssetManager.PTR.tileRenderers.get( tile.layer );
		
		renderer.removeTile( tile );
	}
	
	public function addActor( actor:Actor ):Void
	{
		actorsToAdd.push( actor );
	}
	
	private function addActorInternal( actor:Actor ):Void
	{
		actors.push( actor );
		
		if ( actor.tile != null )
		{
			var renderer:TileRenderer = AssetManager.PTR.tileRenderers.get( actor.tile.layer );
			
			renderer.addTile( actor.tile );
		}
		
		if ( collision != null && actor.sceneNode != null )
		{
			collision.add( actor.sceneNode );
		}
	}
	
	public function removeActor( actor:Actor ):Void
	{
		actorsToRemove.push( actor );
	}
	
	private function removeActorInternal( actor:Actor ):Void
	{		
		if ( actors.remove( actor ) == false )
		{
			return;
		}
		
		if ( actor.tile != null )
		{
			var renderer:TileRenderer = AssetManager.PTR.tileRenderers.get( actor.tile.layer );
			
			renderer.removeTile( actor.tile );
		}
		
		if ( actor.sceneNode != null )
		{
			actor.sceneNode.remove();
		}
	}
	
	public function addTickable( tickable:ITickable, isUI:Bool = false ):Void
	{
		if ( isUI == false )
		{
			tickables.push( tickable );
		}
		else
		{
			uiTickables.push( tickable );
		}
	}
	
	public function removeTickable( tickable:ITickable, isUI:Bool = false ):Void
	{
		if ( isUI == false )
		{
			tickablesToRemove.push( tickable );
		}
		else
		{
			uiTickablesToRemove.push( tickable );
		}
	}
	
	//Takes an (x, y) in screen space, width and height in screen space.
	public function getColliders( x:Float, y:Float, width:Float, height:Float ):Array<SceneNode>
	{
		cachedNode.collisionComponent.x = ResolutionConfig.PTR.convertWorldXToScreen( x );
		cachedNode.collisionComponent.y = ResolutionConfig.PTR.convertWorldYToScreen( y ); 
		cachedNode.collisionComponent.width = width;
		cachedNode.collisionComponent.height = height;
		cachedNode.collisionComponent.collisionFlag = CollisionFlags.ACTORS;
		
		var result:Array<SceneNode> = collision.getColliders( cachedNode );
		
		return result;
	}
	
	//Works entirely in world space.
	public function getCollidersWorldSpace( x:Float, y:Float, width:Float, height:Float ):Array<SceneNode>
	{
		cachedNode.collisionComponent.x = x;
		cachedNode.collisionComponent.y = y; 
		cachedNode.collisionComponent.width = width;
		cachedNode.collisionComponent.height = height;
		cachedNode.collisionComponent.collisionFlag = CollisionFlags.ACTORS;
		
		var result:Array<SceneNode> = collision.getColliders( cachedNode );
		
		return result;
	}
	
	public function getNodeColliders( sceneNode:SceneNode ):Array<SceneNode>
	{
		return collision.getColliders( sceneNode );
	}
	
	public function getLineColliders( x1:Float, y1:Float, x2:Float, y2:Float, originator:SceneNode, flags:Int ):Array<SceneNode>
	{
		// Convert to screen coords (that's what the tree is in)
		return collision.getLineColliders( originator, ResolutionConfig.PTR.convertWorldXToScreen( x1 ),
											ResolutionConfig.PTR.convertWorldYToScreen( y1 ),
											ResolutionConfig.PTR.convertWorldXToScreen( x2 ),
											ResolutionConfig.PTR.convertWorldYToScreen( y2 ), flags );
	}
	
	private function update( e:Event ):Void
	{
		var dtReal:Float = ( 1 / 30 );
		var deltaTimeSeconds:Float = dtReal * slomoValue;
		
		for ( uiTickable in uiTickables )
		{
			uiTickable.tick( dtReal );
		}
		
		engineElapsedTime += dtReal;
		
		if ( uiTickablesToRemove.length > 0 )
		{
			for ( uiTickableToRem in uiTickablesToRemove )
			{
				uiTickables.remove( uiTickableToRem );
			}
			
			uiTickablesToRemove.splice( 0, uiTickablesToRemove.length );
		}
		
		if ( isPaused )
		{
			return;
		}
		
		elapsedTime += deltaTimeSeconds;
		
		var mouseX:Float = Lib.current.stage.mouseX;
		var mouseY:Float = Lib.current.stage.mouseY;
		
		var colliders:Array<SceneNode> = collision.getPointColliders( mouseX, mouseY );
		
		#if !flash
		isMouseDown = true;
		#end
		
		if ( doesAllowSceneInput == false )
		{
			isMouseDown = false;
		}
		
		if ( isMouseDown && noClickZones.length > 0 )
		{
			for ( noClick in noClickZones )
			{
				if ( noClick.doesAABBContainPoint( mouseX, mouseY ) )
				{
					isMouseDown = false;
					break;
				}
			}
		}
		
		// Touch action
		if ( isMouseDown )
		{
			for ( collider in colliders )
			{
				if ( Std.is( collider.internalObject, ITouchable ) )
				{
					var touchable:ITouchable = cast collider.internalObject;
					touchable.handleTouch();
				}
			}
		}
		
		for ( tickable in tickables )
		{
			tickable.tick( deltaTimeSeconds );
		}
		
		if ( tickablesToRemove.length > 0 )
		{
			for ( tickToRemove in tickablesToRemove )
			{
				tickables.remove( tickToRemove );
			}
			
			tickablesToRemove.splice( 0, tickablesToRemove.length );
		}
		
		if ( actorsToAdd.length > 0 )
		{
			for ( actorToAdd in actorsToAdd )
			{
				addActorInternal( actorToAdd );
			}
			
			actorsToAdd.splice( 0, actorsToAdd.length );
		}
		
		if ( actorsToRemove.length > 0 )
		{
			for ( actorToRemove in actorsToRemove )
			{
				removeActorInternal( actorToRemove );
			}
			
			actorsToRemove.splice( 0, actorsToRemove.length );
		}
		
		/*
		for ( layer in layers )
		{
			layer.graphics.clear();
		}
		*/
		
		tileLayer.graphics.clear();
		
		#if flash
		renderBuffer.bitmapData.lock();
		renderBuffer.bitmapData.fillRect( renderBuffer.bitmapData.rect, 0 );
		#end
		
		var renderList:Array<TileRenderer> = AssetManager.PTR.tileRenderersOrdered;
		
		// Render
		for ( i in 0...renderList.length )
		{
			renderList[ i ].render( tileLayer.graphics );
		}
		
		#if flash
		renderBuffer.bitmapData.unlock();
		#end
		
		GameConfig.PTR.poolManager.handleEndOfUpdate();
	}
	
	public function registerNoClickZone( zone:RectCollider ):Void
	{
		noClickZones.push( zone );
	}
	
	public function resetNoClickZones():Void
	{
		noClickZones.splice( 0, noClickZones.length );
	}
	
	public function getTickables():Array<ITickable> { return tickables; }
}