package engine.scene;
import engine.assets.GameBitmap;
import engine.collision.SceneNode;
import engine.misc.IDestroyable;
import engine.misc.MathUtility;
import engine.render.GameTile;
import engine.render.particles.ParticleEmitter;
import engine.render.ResolutionConfig;
import nme.events.Event;
import nme.events.EventDispatcher;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class Actor extends EventDispatcher
{
	public var bitmap:GameBitmap;
	public var tile:GameTile;
	public var sceneNode:SceneNode;
	
	public var x( default, setX ):Float;
	public var y( default, setY ):Float;
	
	public function new() 
	{
		super();
	}
	
	public function rotate( amount:Float ):Void
	{
		tile.rotation += amount;
	}
	
	private function setX( value:Float ):Float 
	{ 		
		x = value;
		
		tile.x = ResolutionConfig.PTR.convertWorldXToScreen( x );
		
		if ( sceneNode != null )
		{
			sceneNode.collisionComponent.x = x;
			sceneNode.syncPosition();
		}
		
		return x;
	}
	
	private function setY( value:Float ):Float
	{ 
		y = value;
		
		tile.y = ResolutionConfig.PTR.convertWorldYToScreen( y );
		
		if ( sceneNode != null )
		{
			sceneNode.collisionComponent.y = y;
			sceneNode.syncPosition();
		}
		
		return y;
	}
}