package engine.render.tilestorage;
import engine.misc.ITickable;
import engine.render.GameTile;

/**
 * ...
 * @author A.A. Grapsas
 */
interface ITileStorage implements ITickable
{
	function addTile( tile:GameTile ):Void;
	function removeTile( tile:GameTile ):Void;
	function getRenderIndecies():Array<Float>;
	
	#if flash
	function getTiles():Array<GameTile>;
	#end
}