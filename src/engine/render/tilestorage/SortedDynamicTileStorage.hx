package engine.render.tilestorage;
import engine.debug.Debug;
import engine.render.GameTile;

/**
 * ...
 * @author A.A. Grapsas
 */
class SortedDynamicTileStorage implements ITileStorage
{
	private var indexData:Array<Float>;
	private var tileLayers:Array<Array<GameTile>>;
	
	#if debug
	private var tileLayersMaxCount:Array<Int>;
	#end
	
	private var runningLength:Int = 0;
	
	private var isDirty:Bool = true;
	
	public function new() 
	{
		indexData = new Array<Float>();
		tileLayers = new Array<Array<GameTile>>();
		
		tileLayers.push( new Array<GameTile>() );
		
		#if debug
		tileLayersMaxCount = new Array<Int>();
		#end
	}
	
	public function addTile( tile:GameTile ):Void
	{
		if ( tile.sort >= tileLayers.length )
		{
			var distance:Int = 1 + ( tile.sort - tileLayers.length );
			
			for ( i in 0...distance )
			{
				tileLayers.push( new Array<GameTile>() );
				
				#if debug
				tileLayersMaxCount.push( 0 );
				#end
			}
		}
		
		tileLayers[ tile.sort ].push( tile );
		
		#if debug
		var prev:Int = tileLayersMaxCount[ tile.sort ];
		
		if ( tileLayers[ tile.sort ].length > prev )
		{
			tileLayersMaxCount[ tile.sort ] = tileLayers[ tile.sort ].length;
			
			Debug.stats.setStat( "Max elms in tile bin " + tile.sort, cast tileLayersMaxCount[ tile.sort ] );
		}
		#end
		
		runningLength++;
		
		if ( ( runningLength * GameTile.TILE_LENGTH ) < indexData.length )
		{
			// Should only ever be by one
			
			for ( j in 0...GameTile.TILE_LENGTH )
			{
				indexData.push( 0 );
			}
		}
		
		Debug.stats.setStat( "Dyn tiles", cast runningLength );
		
		isDirty = true;
	}
	
	public function removeTile( tile:GameTile ):Void
	{
		tileLayers[ tile.sort ].remove( tile );
		
		runningLength--;
		
		Debug.stats.setStat( "Dyn tiles", cast runningLength );
		
		isDirty = true;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( isDirty == false )
		{
			return;
		}
		
		var runningIndex:Int = 0;
		
		Debug.stats.setStat( "Dyn tiles", cast runningLength );
		
		// Overwrite with 0's
		for ( i in 0...indexData.length )
		{
			indexData[ i ] = 0;
		}
		
		// Manually re-sort entire array
		for ( tileLayer in tileLayers )
		{
			for ( tile in tileLayer )
			{
				tile.setup( indexData, runningIndex );
				
				runningIndex += GameTile.TILE_LENGTH;
			}
		}
		
		isDirty = false;
	}
	
	public function getRenderIndecies():Array<Float> { return indexData; }
	
	#if flash
	public function getTiles():Array<GameTile>
	{
		var tiles:Array<GameTile> = new Array<GameTile>();
		
		for ( layer in tileLayers )
		{
			for ( tile in layer )
			{
				tiles.push( tile );
			}
		}
		
		return tiles;
	}
	#end
}