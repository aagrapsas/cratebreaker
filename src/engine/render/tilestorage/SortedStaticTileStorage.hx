package engine.render.tilestorage;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.render.GameTile;

/**
 * ...
 * @author A.A. Grapsas
 */
class SortedStaticTileStorage implements ITileStorage
{
	private var freeIndecies:Array<Array<Int>>;	// layer -> free indecies for that layer
	
	private var indexData:Array<Float>;
	
	#if flash
	private var tileData:Array<GameTile>;
	#end
	
	public function new( binPoolSizes:Array<Int> )
	{
		setup( binPoolSizes );
	}
	
	private function setup( binPoolSizes:Array<Int> ):Void
	{
		freeIndecies = new Array<Array<Int>>();
		indexData = new Array<Float>();
		
		#if flash
		tileData = new Array<GameTile>();
		#end
		
		var index:Int = 0;
		
		for ( i in 0...binPoolSizes.length )
		{		
			freeIndecies[ i ] = new Array<Int>();
			var size:Int = binPoolSizes[ i ];
			
			for ( j in 0...size )
			{
				for ( k in 0...GameTile.TILE_LENGTH )
				{
					indexData.push( 0 );
				}
				
				freeIndecies[ i ].push( index );
				
				index += GameTile.TILE_LENGTH;
			}
		}
	}
	
	public function addTile( tile:GameTile ):Void
	{
		if ( tile.sort >= freeIndecies.length || tile.sort < 0 )
		{
			Debug.log( DebugChannels.ERROR, "Attempted to add tile to layer " + tile.sort + " which is outside bounds!" );
			return;
		}
		
		var free:Array<Int> = freeIndecies[ tile.sort ];
		
		if ( free.length == 0 )
		{
			Debug.log( DebugChannels.ERROR, "Attempted to get tile from layer " + tile.sort + " with pool being empty!" );
			return;
		}
		
		var index:Int = free.pop();
		
		tile.setup( indexData, index );
		
		#if flash
		tileData.push( tile );
		#end
	}
	
	public function removeTile( tile:GameTile ):Void
	{
		var index:Int = tile.index;
		
		freeIndecies[ tile.sort ].push( index );
		
		#if flash
		tileData.remove( tile );
		#end
	}
	
	public function tick( deltaTime:Float ):Void
	{
		
	}
	
	public function getRenderIndecies():Array<Float> { return indexData; }
	
	#if flash
	public function getTiles():Array<GameTile> { return tileData; }
	#end
}