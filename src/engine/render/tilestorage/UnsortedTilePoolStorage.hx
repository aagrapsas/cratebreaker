package engine.render.tilestorage;
import engine.render.GameTile;

/**
 * ...
 * @author A.A. Grapsas
 */
class UnsortedTilePoolStorage implements ITileStorage
{
	public var freeIndices:Array<Int>;		// Indexes of starting points of unused segments
	public var indexData:Array<Float>;		// All data, including unused segments	(UNSORTED)
	public var tiles:Array<GameTile>;
	
	public function new( size:Int ) 
	{
		var index:Int = 0;
		
		for ( i in 0...size )
		{			
			for ( j in 0...GameTile.TILE_LENGTH )
			{
				indexData.push( 0 );
			}
			
			freeIndecies.push( index );
			
			index += GameTile.TILE_LENGTH;
		}
		
		#if flash
		tiles = new Array<GameTile>();
		#end
	}
	
	public function addTile( tile:GameTile ):Void
	{
		if ( freeIndices.length == 0 )
		{
			Debug.log( DebugChannels.ERROR, "Used all tiles!" );
			return;
		}
		
		// Remove an index from the pool of available indecies
		var freeIndex:Int = freeIndecies.pop();
		
		// Assign to tile and have tile fill in the appropriate data
		tile.setup( indexData, freeIndex );
		
		#if flash
		tiles.push( tile );
		#end if
	}
	
	public function removeTile( tile:GameTile ); Void
	{
		var index:Int = tile.index;
		
		freeIndecies.push( index );
		
		#if flash
		tiles.remove( tile );
		#end if
	}
	
	public function tick( deltaTime:Float ):Void
	{
		
	}
	
	public function getRenderIndecies():Array<Float> { return indexData; }
	
	#if flash
	public function getTiles():Array<GameTile> { return tiles; }
	#end if
}