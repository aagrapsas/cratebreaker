package engine.render.particles;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.render.particles.ParticleEmitter;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;
import nme.events.Event;

/**
 * ...
 * @author A.A. Grapsas
 */

class InWorldFXPlayer
{
	public static var PTR:InWorldFXPlayer;
	
	public function new()
	{
		PTR = this;
	}
	
	public function playFX( fx:ParticleEmitter, x:Float, y:Float ):Void
	{
		SceneManager.PTR.addTickable( fx );
		
		fx.x = ResolutionConfig.PTR.convertWorldXToScreen( x );
		fx.y = ResolutionConfig.PTR.convertWorldYToScreen( y );
		
		fx.addEventListener( ParticleEmitter.EMITTER_EMPTY, onEmitterEmpty );
	}
	
	private function onEmitterEmpty( e:Event ):Void
	{
		var emitter:ParticleEmitter = cast e.target;
		
		emitter.removeEventListener( ParticleEmitter.EMITTER_EMPTY, onEmitterEmpty );
		
		SceneManager.PTR.removeTickable( emitter );
	}
}