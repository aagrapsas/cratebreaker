package engine.render.particles;

/**
 * ...
 * @author A.A. Grapsas
 */

interface IParticleFX 
{
	function updateParticle( particle:Particle, deltaTime:Float ):Void;
}