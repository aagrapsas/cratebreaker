package engine.render.particles;
import engine.assets.GameBitmap;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.IDestroyable;
import engine.misc.ITickable;
import engine.misc.MathUtility;
import engine.scene.SceneManager;
import game.config.GameConfig;
import nme.events.Event;
import nme.events.EventDispatcher;

/**
 * ...
 * @author A.A. Grapsas
 */

class ParticleEmitter extends EventDispatcher, implements ITickable, implements IDestroyable
{
	public static var EMITTER_EMPTY:String = "empty";
	
	public var x( default, default ):Float = 0;
	public var y( default, default ):Float = 0;
	
	public var particlesPerSecond:Float = 60;
	public var maxParticles:Int = 200;
	public var maxLifeTime:Float = 0;
	public var timeToSoftStop:Float = 0;
	
	public var initializers:Array<IParticleFX>;
	public var effects:Array<IParticleFX>;
	
	private var particleFractions:Float = 0;
	private var particles:Array<Particle>;
	private var toRemove:Array<Particle>;
	private var lifeTime:Float = 0;
	
	private var isSoftStopped:Bool = false;
	private var isStopped:Bool = false;
	
	public function new() 
	{
		super();
		
		initializers = new Array<IParticleFX>();
		effects = new Array<IParticleFX>();
		
		particles = new Array<Particle>();
		toRemove = new Array<Particle>();
	}

	private function spawnParticle():Void
	{
		var particle:Particle = cast GameConfig.PTR.poolManager.getPool( "particles" ).pop();
		
		// Start at appropriate x, y
		particle.x = x;
		particle.y = y;
		
		particle.velocityX = 0;
		particle.velocityY = 0;
		
		particle.creationStamp = SceneManager.PTR.elapsedTime;
		particle.lifeTime = 0;
		particle.maxLifeTime = 0;
		
		// Initializers can modify x, y
		for ( initializer in initializers )
		{
			initializer.updateParticle( particle, 0 );
		}
		
		// Set to living
		particle.isAlive = true;
		
		particles.push( particle );
		
		// Debug.log( DebugChannels.DEBUG, "Spawning particle at x: " + particle.x + " y: " + particle.y );
	}
	
	public function tick( deltaTime:Float ):Void
	{	
		if ( isStopped )
		{
			return;
		}
		
		// Increment life of emitter (if it has a limit)
		if ( maxLifeTime > 0 || timeToSoftStop > 0 )
		{
			lifeTime += deltaTime;
			
			if ( maxLifeTime > 0 && lifeTime >= maxLifeTime )
			{
				this.dispatchEvent( new Event( EMITTER_EMPTY ) );
				
				destroy();
				return;
			}
			
			if ( timeToSoftStop > 0 && lifeTime >= timeToSoftStop )
			{
				softStop();
			}
		}
		
		if ( particles.length < maxParticles && isSoftStopped == false )
		{			
			// Complex particle spawning logic (accounts for very slow frames as with slomo)
			var particlesPerUpdate:Float = particlesPerSecond * deltaTime;
			
			var particlesToSpawn:Int = Std.int( particlesPerUpdate );
			
			particlesToSpawn = Std.int( Math.min( particlesToSpawn, maxParticles - particles.length ) );
			
			if ( particlesPerUpdate < 1 )
			{
				particleFractions += particlesPerUpdate;
				
				if ( particleFractions > 1 )
				{
					particlesToSpawn++;
					
					particleFractions -= 1.0;
				}
			}
			
			for ( i in 0...particlesToSpawn )
			{
				spawnParticle();
			}
		}
		
		for ( particle in particles )
		{		
			particle.x += particle.velocityX * deltaTime;
			particle.y += particle.velocityY * deltaTime;
			
			for ( effect in effects )
			{
				effect.updateParticle( particle, deltaTime );
			}
			
			// Handle particle lifetime
			if ( particle.isAlive )
			{
				particle.lifeTime += deltaTime;
			
				if ( particle.maxLifeTime > 0 && particle.lifeTime >= particle.maxLifeTime )
				{
					toRemove.push( particle );
				}
			}
			else
			{
				toRemove.push( particle );
			}
		}
		
		for ( particleToRemove in toRemove )
		{
			particles.remove( particleToRemove );
			
			particleToRemove.destroy();	// Frees to pool
		}
		
		if ( toRemove.length > 0 )
		{
			toRemove.splice( 0, toRemove.length );
		}
		
		if ( isSoftStopped && particles.length == 0 )
		{
			this.dispatchEvent( new Event( EMITTER_EMPTY ) );
			destroy();
		}
	}
	
	/**
	 * softStop
	 * 
	 * Will trigger emitter empty and destroy the emitter when
	 * they are finished
	 * 
	 */
	public function softStop():Void
	{
		isSoftStopped = true;
	}
	
	/**
	 * stop
	 * 
	 * Immediately kills emitter. Does not trigger event.
	 * 
	 */
	public function stop():Void
	{
		isStopped = true;
		
		destroy();
	}
	
	private function freeParticles():Void
	{
		for ( particle in particles )
		{
			particle.destroy();
		}
		
		particles.splice( 0, particles.length );
	}
	
	public function destroy():Void
	{
		freeParticles();
	}
}