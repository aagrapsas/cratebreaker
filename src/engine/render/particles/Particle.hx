package engine.render.particles;
import engine.misc.GenericPool;
import engine.misc.IDestroyable;
import engine.misc.IPoolable;
import engine.render.GameTile;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;

/**
 * ...
 * @author A.A. Grapsas
 */

class Particle implements IPoolable, implements IDestroyable
{
	public var tile( default, setTile ):GameTile;
	
	public var x( default, setX ):Float;
	public var y( default, setY ):Float;
	public var rotation( default, setRotation ):Float;
	public var alpha( default, setAlpha ):Float;
	public var scale( default, setScale ):Float;
	
	public var isAlive:Bool = true;
	public var lifeTime:Float = 0;
	public var maxLifeTime:Float = 0;
	
	public var velocityX:Float = 0;
	public var velocityY:Float = 0;
	
	public var creationStamp:Float = 0;
	
	private var pool:GenericPool;
	
	public function new() 
	{
		this.x = 0;
		this.y = 0;
		this.rotation = 0;
		this.alpha = 1;
		this.scale = 1;
	}
	
	private function setTile( value:GameTile ):GameTile
	{
		tile = value;
		
		if ( tile != null )
		{
			tile.x = x;
			tile.y = y;
			tile.rotation = rotation;
			tile.alpha = alpha;
			tile.scale = scale;
		}
		
		return tile;
	}
	
	private function setX( value:Float ):Float
	{ 
		x = value;
		
		if ( tile != null )
		{
			tile.x = value;
		}
		
		return x;
	}
	
	private function setY( value:Float ):Float
	{
		y = value;
		
		if ( tile != null )
		{
			tile.y = value;
		}
		
		return y;
	}
	
	private function setRotation( value:Float ):Float
	{
		rotation = value;
		
		if ( tile != null )
		{
			tile.rotation = value;
		}
		
		return rotation;
	}
	
	private function setAlpha( value:Float ):Float
	{
		alpha = value;
		
		if ( tile != null )
		{
			tile.alpha = value;
		}
		
		return alpha;
	}
	
	private function setScale( value:Float ):Float
	{
		scale = value;
		
		if ( tile != null )
		{
			tile.scale = value;
		}
		
		return scale;
	}
	
	public function resurrect():Void
	{
		lifeTime = 0;
		maxLifeTime = 0;
	}
	
	public function suspend():Void
	{
		isAlive = false;
	}
	
	public function getPool():GenericPool
	{
		return pool;
	}
	
	public function setPool( value:GenericPool ):Void
	{
		pool = value;
	}
	
	public function destroy():Void
	{
		if ( isAlive == false )
		{
			return;
		}
		
		isAlive = false;
		
		if ( tile != null )
		{
			SceneManager.PTR.removeTile( tile );
		}
		
		tile = null;
		
		pool.free( this );
	}
}