package engine.render.particles.fx;
import engine.render.particles.IParticleFX;
import engine.render.particles.Particle;

/**
 * ...
 * @author A.A. Grapsas
 */

class SetLifeTimeFX implements IParticleFX
{
	public var lifeTimeSeconds:Float = 10;
	
	public function new() 
	{
		
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		particle.maxLifeTime = lifeTimeSeconds;
	}
}