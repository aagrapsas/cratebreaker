package engine.render.particles.fx;
import engine.render.particles.IParticleFX;

/**
 * ...
 * @author A.A. Grapsas
 */

class SpawnConeFX implements IParticleFX
{
	public var offsetX:Float = 0;
	public var offsetY:Float = 0;
	public var minRadius:Float = 0;
	public var maxRadius:Float = 0;
	public var rotationDegrees:Float = 0;
	public var spreadDegrees:Float = 0;
	
	public function new() 
	{
		
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		var halfAngle:Float = rotationDegrees / 2;
		var randAngle:Float = Math.floor( Math.random() * ( halfAngle + 1 ) ) * ( Math.random() > 0.5 ? -1 : 1 );
		var finalAngle:Float = ( rotationDegrees + randAngle ) * ( Math.PI / 180 );
		var normalizedX:Float = Math.cos( finalAngle );
		var normalizedY:Float = Math.sin( finalAngle );
		
		var dist:Float = Math.floor( minRadius + ( Math.random() * ( maxRadius - minRadius + 1 ) ) );
		
		particle.x += offsetX + normalizedX * dist;
		particle.y += offsetY + normalizedY * dist;
	}
}