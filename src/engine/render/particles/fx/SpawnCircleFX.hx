package engine.render.particles.fx;
import engine.misc.MathUtility;
import engine.render.particles.IParticleFX;
import engine.render.particles.Particle;

/**
 * ...
 * @author A.A. Grapsas
 */

class SpawnCircleFX implements IParticleFX
{
	public var offsetX:Float = 0;
	public var offsetY:Float = 0;
	public var minRadius:Int = 0;
	public var maxRadius:Int = 0;
	
	public function new() 
	{
		
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		particle.x += offsetX;
		particle.y += offsetY;
		
		var radius:Int = MathUtility.getRandomIntInRange( minRadius, maxRadius );
		
		var randomAngle:Float = MathUtility.getRandomFloatInRange( 0, Math.PI * 2 );
		
		var x:Float = 1;
		var y:Float = 0;
		
		particle.x += x * Math.cos( randomAngle ) * radius;
		particle.y += x * Math.sin( randomAngle ) * radius;
	}
}