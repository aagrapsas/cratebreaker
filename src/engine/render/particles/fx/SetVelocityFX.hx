package engine.render.particles.fx;
import engine.render.particles.IParticleFX;
import engine.render.particles.Particle;

/**
 * ...
 * @author A.A. Grapsas
 */

class SetVelocityFX implements IParticleFX
{
	public var velocityX:Float = 0;
	public var velocityY:Float = 0;
	
	public function new() 
	{
		
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		particle.velocityX = velocityX;
		particle.velocityY = velocityY;
	}
}