package engine.render.particles.fx;
import engine.misc.LerpTypes;
import engine.render.particles.IParticleFX;

/**
 * ...
 * @author A.A. Grapsas
 */

class FadeInOutFX implements IParticleFX
{
	public var startAlpha:Float = 0;
	public var middleAlpha:Float = 1;
	public var endAlpha:Float = 0;
	
	public var startDuration:Float = 1;
	public var middleDuration:Float = 1;
	public var endDuration:Float = 1;
	
	public var startLerp:Int = 0;
	public var endLerp:Int = 0;
	
	public function new() 
	{
		startLerp = LerpTypes.EASE_IN;
		endLerp = LerpTypes.EASE_OUT;
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		var alpha:Float = 0;
		
		if ( particle.lifeTime < startDuration )
		{
			// Beginning
			alpha = Math.min( particle.lifeTime / startDuration, 1.0 );
			
			particle.alpha = LerpTypes.getLerpValue( startLerp, startAlpha, middleAlpha, alpha );
		}
		else if ( particle.lifeTime < ( startDuration + middleDuration ) )
		{
			// Middle
			// Do nothing
		}
		else
		{
			// End
			alpha = Math.min( ( particle.lifeTime - startDuration - middleDuration ) / endDuration, 1.0 );
			
			particle.alpha = LerpTypes.getLerpValue( endLerp, middleAlpha, endAlpha, alpha );
		}
	}
}