package engine.render.particles.fx;
import engine.misc.MathUtility;

/**
 * ...
 * @author A.A. Grapsas
 */

class SetOpposedMagnitudeFX 
{
	public var magnitudeX:Float = 1;
	public var magnitudeY:Float = 1;
	
	public function new() 
	{
		
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		particle.velocityX = MathUtility.getRandomFloatInRange( -magnitudeX, magnitudeX );
		particle.velocityY = MathUtility.getRandomFloatInRange( -magnitudeY, magnitudeY );
	}
}