package engine.render.particles.fx;
import engine.render.particles.IParticleFX;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class ExplodeFX implements IParticleFX
{
	private var cachedPoint:Point;
	
	public var magnitude:Float = 10;
	
	public function new() 
	{
		cachedPoint = new Point();
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		cachedPoint.x = ( Math.random() * 1000 ) * ( Math.random() > 0.5 ? 1 : -1 );
		cachedPoint.y = ( Math.random() * 1000 ) * ( Math.random() > 0.5 ? 1 : -1 );
		cachedPoint.normalize( 1 );
		
		particle.velocityX += cachedPoint.x * magnitude;
		particle.velocityY += cachedPoint.y * magnitude;
	}
}