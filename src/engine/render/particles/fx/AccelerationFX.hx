package engine.render.particles.fx;
import engine.render.particles.IParticleFX;
import engine.render.particles.Particle;

/**
 * ...
 * @author A.A. Grapsas
 */

class AccelerationFX implements IParticleFX
{
	public var accelerationX:Float = 0;
	public var accelerationY:Float = 0;
	
	public function new() 
	{
		
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		particle.velocityX += accelerationX * deltaTime;
		particle.velocityY += accelerationY * deltaTime;
	}
}