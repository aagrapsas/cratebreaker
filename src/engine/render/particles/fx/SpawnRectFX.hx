package engine.render.particles.fx;
import engine.misc.MathUtility;
import engine.render.particles.IParticleFX;
import engine.render.particles.Particle;

/**
 * ...
 * @author A.A. Grapsas
 */

class SpawnRectFX implements IParticleFX
{
	public var offsetX:Float = 0;
	public var offsetY:Float = 0;
	
	public var width( default, setWidth ):Float;
	public var height( default, setHeight ):Float;
	
	private var halfWidth:Float;
	private var halfHeight:Float;
	
	public function new() 
	{
		
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		particle.x += offsetX;
		particle.y += offsetY;
		
		particle.x += MathUtility.getRandomFloatInRange( -halfWidth, halfWidth );
		particle.y += MathUtility.getRandomFloatInRange( -halfHeight, halfHeight );
	}
	
	private function setWidth( value:Float ):Float
	{
		width = value;
		
		halfWidth = width / 2;
		
		return value;
	}
	
	private function setHeight( value:Float ):Float
	{
		height = value;
		
		halfHeight = height / 2;
		
		return value;
	}
}