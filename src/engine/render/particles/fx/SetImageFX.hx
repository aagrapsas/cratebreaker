package engine.render.particles.fx;
import engine.assets.GameBitmap;
import engine.misc.MathUtility;
import engine.render.particles.IParticleFX;
import engine.scene.SceneManager;

/**
 * ...
 * @author A.A. Grapsas
 */

class SetImageFX implements IParticleFX
{
	public var images:Array<GameBitmap>;
	public var sortOrder:Int = 0;
	
	public function new() 
	{
		images = new Array<GameBitmap>();
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		if ( particle.tile != null )
		{
			SceneManager.PTR.removeTile( particle.tile );
		}
		
		var image:GameBitmap = images[ MathUtility.getRandomIntInRange( 0, images.length - 1 ) ];
		
		particle.tile = image.getTile();
		particle.tile.sort = sortOrder;
		
		#if flash
		particle.tile.bitmap = image.getNewBitmap();
		#end
		
		SceneManager.PTR.addTile( particle.tile );
	}
}