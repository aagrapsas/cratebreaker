package engine.render.particles.fx;
import engine.render.particles.IParticleFX;
import engine.render.particles.Particle;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class MoveOverTimeFX implements IParticleFX
{
	public var speed:Float = 100;
	public var acceleration:Float = 10;
	
	public var targetX:Float = 0;
	public var targetY:Float = 0;
	
	private var cachedPoint:Point;
	
	public function new() 
	{
		cachedPoint	= new Point();
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		cachedPoint.x = targetX - particle.x;
		cachedPoint.y = targetY - particle.y;
		cachedPoint.normalize( 1 );
		
		if ( speed > 0 )
		{
			particle.x += cachedPoint.x * speed * deltaTime;
			particle.y += cachedPoint.y * speed * deltaTime;
		}
		else if ( acceleration > 0 )
		{
			particle.velocityX += cachedPoint.x * acceleration * deltaTime;
			particle.velocityY += cachedPoint.y * acceleration * deltaTime;
		}
	}
}