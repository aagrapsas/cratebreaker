package engine.render.particles.fx;
import engine.misc.LerpTypes;
import engine.misc.MathUtility;
import engine.render.particles.IParticleFX;

/**
 * ...
 * @author A.A. Grapsas
 */

class ScaleOverTimeFX implements IParticleFX
{
	public var startScale:Float = 1;
	public var endScale:Float = 0;
	public var duration:Float = 3;
	public var lerpType:Int = 0;
	
	public function new() 
	{
		lerpType = LerpTypes.LINEAR;
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		var alpha:Float = Math.min( particle.lifeTime / duration, 1.0 );
		
		particle.scale = LerpTypes.getLerpValue( lerpType, startScale, endScale, alpha );
	}
}