package engine.render.particles.fx;
import engine.misc.LerpTypes;
import engine.misc.MathUtility;
import engine.render.particles.IParticleFX;
import engine.scene.SceneManager;

/**
 * ...
 * @author A.A. Grapsas
 */

class FadeOverTimeFX implements IParticleFX
{
	public var startAlpha:Float = 1;
	public var endAlpha:Float = 0;
	public var duration:Float = 3;
	public var lerpType:Int = 0;
	
	public function new() 
	{
		lerpType = LerpTypes.LINEAR;
	}
	
	public function updateParticle( particle:Particle, deltaTime:Float ):Void
	{
		var alpha:Float = Math.min( particle.lifeTime / duration, 1 );
		
		particle.alpha = LerpTypes.getLerpValue( lerpType, startAlpha, endAlpha, alpha );
	}
}