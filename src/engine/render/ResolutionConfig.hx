package engine.render;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import nme.system.Capabilities;

/**
 * ...
 * @author A.A. Grapsas
 */

class ResolutionConfig 
{
	public static var PTR:ResolutionConfig;
	
	public var expectedScreenWidth:Float = 0;
	public var expectedScreenHeight:Float = 0;
	public var actualScreenWidth:Float = 0;
	public var actualScreenHeight:Float = 0;
	
	public var uniformScaleFactor:Float = 0;
	
	public var nonuniformScaleX:Float = 0;
	public var nonuniformScaleY:Float = 0;
	
	public function new() 
	{
		PTR = this;
	}
	
	public function setup( expectedWidth:Float, expectedHeight:Float, actualWidth:Float, actualHeight:Float ):Void
	{
		this.expectedScreenHeight = expectedHeight;
		this.expectedScreenWidth = expectedWidth;
		this.actualScreenHeight = actualHeight;
		this.actualScreenWidth = actualWidth;
		
		/**
		 * 
		 * WHAT IF?
		 * 
		 * Scale assets by DPI difference
		 * 
		 * Expected: 306
		 * Actual: 72
		 * 
		 * Scale world coordinates by resolution difference
		 * 
		 */
		
		// DPI is diagonal
		var expectedDiagonalPI:Float = 306;	// Galaxy S3
		var actualDiagonalPI:Float = Capabilities.screenDPI;	// Current device
		
		var expectedDiagonalRes:Float = Math.sqrt( this.expectedScreenWidth * this.expectedScreenWidth + this.expectedScreenHeight * this.expectedScreenHeight );
		var actualDiagonalRes:Float = Math.sqrt( this.actualScreenWidth * this.actualScreenWidth + this.actualScreenHeight * this.actualScreenHeight );
		
		// Expected PPI
		var expectedVerticalPI:Float = ( this.expectedScreenHeight / expectedDiagonalRes ) * expectedDiagonalPI;
		var expectedHorizontalPI:Float = ( this.expectedScreenWidth / expectedDiagonalRes ) * expectedDiagonalPI;
		
		// Actual PPI
		var actualVerticalPI:Float = ( this.actualScreenHeight / actualDiagonalRes ) * actualDiagonalPI;
		var actualHorizontalPI:Float = ( this.actualScreenWidth / actualDiagonalRes ) * actualDiagonalPI;
		
		// Diagonal inches
		var actualDiagonalInches:Float = actualDiagonalRes / actualDiagonalPI;
		var expectedDiagonalInches:Float = expectedDiagonalRes / expectedDiagonalPI;
		
		// Expected inches
		var expectedVerticalInches:Float = ( this.expectedScreenHeight / expectedDiagonalRes ) * expectedDiagonalInches;
		var expectedHorizontalInches:Float = ( this.expectedScreenWidth / expectedDiagonalRes ) * expectedDiagonalInches;
		
		// Actual inches
		var actualVerticalInches:Float = ( this.actualScreenHeight / actualDiagonalRes ) * actualDiagonalInches;
		var actualHorizontalInches:Float = ( this.actualScreenWidth / actualDiagonalRes ) * actualDiagonalInches;
		
		this.actualScreenHeight = actualHeight;
		this.actualScreenWidth = actualWidth;
		
		var physicalScale:Float = expectedVerticalInches / actualVerticalInches;
		var physicalScaleHorizontal:Float = expectedHorizontalInches / actualHorizontalInches;
		
		var dpiScale:Float = expectedDiagonalPI / actualDiagonalPI;
		var dpiScaleHorizontal:Float = expectedHorizontalPI / actualHorizontalPI;
		var dpiScaleVertical:Float = expectedVerticalPI / actualVerticalPI;

		this.uniformScaleFactor = dpiScale * physicalScale;
		
		this.nonuniformScaleY = expectedHeight / actualHeight;
		this.nonuniformScaleX = expectedWidth / actualWidth;
		
		this.actualScreenHeight = this.expectedScreenHeight / this.nonuniformScaleY;
		this.actualScreenWidth = this.expectedScreenWidth / this.nonuniformScaleX;
		
		Debug.log( DebugChannels.DISPLAY, "Native res - w: " + actualWidth + " h: " + actualHeight );
		Debug.log( DebugChannels.DISPLAY, "Native dpi - d: " + actualDiagonalPI + " w: " + actualHorizontalPI + " h: " + actualVerticalPI );
		Debug.log( DebugChannels.DISPLAY, "Native siz - w: " + actualHorizontalInches + " h: " + actualVerticalInches );
		Debug.log( DebugChannels.DISPLAY, "Native sca - w: " + nonuniformScaleX + " h: " + nonuniformScaleY );
		Debug.log( DebugChannels.DISPLAY, "Uniform scale : " + uniformScaleFactor );
	}
	
	public function convertScreenXToWorld( x:Float ):Float
	{
		return x * nonuniformScaleX;
	}
	
	public function convertScreenYToWorld( y:Float ):Float
	{
		return y * nonuniformScaleY;
	}
	
	public function convertWorldXToScreen( x:Float ):Float
	{
		return x / nonuniformScaleX;
	}
	
	public function convertWorldYToScreen( y:Float ):Float
	{
		return y / nonuniformScaleY;
	}
	
	public function convertWorldToScreen( z:Float ):Float
	{
		return z / uniformScaleFactor;
	}
}