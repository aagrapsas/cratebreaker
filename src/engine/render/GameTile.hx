package engine.render;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import nme.display.Bitmap;

/**
 * ...
 * @author A.A. Grapsas
 */

class GameTile 
{
	#if flash
	// Used for blitting
	public var bitmap:Bitmap;
	#end
	
	public var asset:String = null;
	
	public var layer:String = null;
	
	public var index:Int = 0;
	
	public var sort:Int = 0;	// 0 is rendered first, 1 next, etc.
	
	public var id( default, setID ):Float = 0;
	public var x( default, setX ):Float = 0;
	public var y( default, setY ):Float = 0;
	public var scale( default, setScale ):Float = 1;
	public var rotation( default, setRotation ):Float = 0;
	public var alpha( default, setAlpha ):Float = 1;
	
	private var data:Array<Float>;
	
	private var idIndex:Int = 0;
	private var xIndex:Int = 0;
	private var yIndex:Int = 0;
	private var scaleIndex:Int = 0;
	private var rotationIndex:Int = 0;
	private var alphaIndex:Int = 0;
	
	private var isAlive:Bool = false;
	
	public static var TILE_LENGTH:Int = 6;
	
	public function new( id:Float )
	{		
		this.id = id;
		
		// set defaults
		x = 0;
		y = 0;
		scale = 1;
		rotation = 0;
		alpha = 1;
	}
	
	public function setup( data:Array<Float>, index:Int ):Void
	{	
		this.index = index;
		this.data = data;
		
		
		xIndex = index;
		yIndex = index + 1;
		idIndex = index + 2;
		scaleIndex = index + 3;
		rotationIndex = index + 4;
		alphaIndex = index + 5;
		
		// Set values
		data[ idIndex ] = id;
		data[ xIndex ] = x;
		data[ yIndex ] = y;
		data[ scaleIndex ] = scale;
		data[ rotationIndex ] = rotation;
		data[ alphaIndex ] = alpha;
		
		isAlive = true;
	}
	
	public function breakDown():Void
	{
		if ( isAlive == false )
		{
			Debug.log( DebugChannels.ERROR, "Attempted to breakDown already decomposed tile!" );
			return;
		}
		
		// Null out data
		data[ idIndex ] = 0;
		data[ xIndex ] = 0;
		data[ yIndex ] = 0;
		data[ scaleIndex ] = 0;
		data[ rotationIndex ] = 0;
		data[ alphaIndex ] = 0;
		
		isAlive = false;
		
		this.index = -1;
		this.data = null;
	}
	
	private function setID( value:Float ):Float
	{
		if ( isAlive )
		{
			data[ idIndex ] = value;
		}
		
		id = value;
		
		return value;
	}
	
	private function setX( value:Float ):Float
	{
		if ( isAlive )
		{
			data[ xIndex ] = value;
		}
		
		x = value;
		
		return value;
	}
	
	private function setY( value:Float ):Float
	{
		if ( isAlive )
		{
			data[ yIndex ] = value;
		}
		
		y = value;
		
		return value;
	}
	
	private function setScale( value:Float ):Float
	{
		if ( isAlive )
		{
			data[ scaleIndex ] = value;
		}
		
		scale = value;
		
		return value;
	}
	
	private function setRotation( value:Float ):Float
	{
		if ( isAlive )
		{
			data[ rotationIndex ] = value;
		}
		
		rotation = value;
		
		return value;
	}
	
	private function setAlpha( value:Float ):Float
	{
		if ( isAlive )
		{
			data[ alphaIndex ] = value;
		}
		
		alpha = value;
		
		return value;
	}
}