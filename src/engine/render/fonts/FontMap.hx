package engine.render.fonts;
import engine.assets.AssetManager;
import engine.assets.GameBitmap;

/**
 * ...
 * @author A.A. Grapsas
 */

class FontMap 
{
	private var bitmaps:Hash<GameBitmap>;
	
	public function new( font:String, characters:String ) 
	{
		bitmaps = new Hash<GameBitmap>();
		
		setFont( font, characters );
	}
	
	public function setFont( font:String, characters:String ):Void
	{
		for ( i in 0...characters.length )
		{
			var character:String = characters.charAt( i );
			var name:String = font + "_" + character;
			var asset:GameBitmap = AssetManager.PTR.getGameBitmap( name );
			
			bitmaps.set( character, asset );
		}
	}
	
	public function getBitmapData( character:String ):GameBitmap
	{
		return bitmaps.get( character );
	}
}