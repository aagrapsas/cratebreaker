package engine.render.fonts;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.misc.IDestroyable;
import engine.misc.ITickable;
import engine.render.GameTile;
import engine.render.ResolutionConfig;
import engine.scene.SceneManager;

/**
 * ...
 * @author A.A. Grapsas
 */

class DynamicText implements ITickable, implements IDestroyable
{
	public var sortOrder:Int = 0;
	
	public var x( default, setX ):Float = 0;
	public var y( default, setY ):Float = 0;
	public var alpha( default, setAlpha ):Float = 1;
	public var fixedWidth( default, setFixedWidth ):Bool = true; //Use fixed width for numbers?
	
	private var fontMap:FontMap;
	private var spacing:Float = 0;
	private var text:String;
	
	private var isDirty:Bool = false;
	
	private var tiles:Array<GameTile>;
	
	private var _width:Float;
	
	public function new() 
	{
		tiles = new Array<GameTile>();
	}
	
	public function setFont( font:FontMap ):Void
	{
		this.fontMap = font;
		
		isDirty = true;
	}
	
	public function setSpacing( space:Float ):Void
	{
		this.spacing = ( space / ResolutionConfig.PTR.nonuniformScaleX );
		
		isDirty = true;
	}
	
	public function setText( text:String ):Void
	{
		this.text = text;
		
		isDirty = true;
	}
	
	public function setFixedWidth( useFixedWidth:Bool ):Bool
	{
		this.fixedWidth = useFixedWidth;
		
		isDirty = true;
		
		return this.fixedWidth;
	}
	
	// Optimization to lazily
	public function tick( deltaTime:Float ):Void
	{
		if ( isDirty )
		{
			redraw();
		}
	}
	
	private function redraw():Void
	{
		var newLen:Int = text.length;
		var oldLen:Int = tiles.length;
		
		var longestLen:Int = Std.int( Math.max( newLen, oldLen ) );
		
		var fixedNumberWidth:Float = 0;
		if ( fixedWidth )
		{
			fixedNumberWidth = fontMap.getBitmapData( "0" ).scaledWidth;
		}
		
		// For longest length ( old range or new )
		for ( i in 0...longestLen )
		{
			// If within new string
			if ( i < newLen )
			{
				// If still within old range, adjust id
				if ( i < oldLen )
				{
					#if flash
					tiles[ i ].bitmap = fontMap.getBitmapData( text.charAt( i ) ).getNewBitmap();
					#end
					
					tiles[ i ].id = fontMap.getBitmapData( text.charAt( i ) ).tileID;
				}
				else // If longer than old, add new tiles
				{
					var newTile:GameTile = fontMap.getBitmapData( text.charAt( i ) ).getTile();
					tiles.push( newTile );
					newTile.sort = sortOrder;
					
					SceneManager.PTR.addTile( newTile );
				}
			}
			else // If done with new string, remove old unused tiles from scene
			{
				SceneManager.PTR.removeTile( tiles[ i ] );
			}
		}
		
		var toSplice:Int = oldLen - newLen;
		
		// Splice unused tiles from tile list
		if ( toSplice > 0 )
		{
			tiles.splice( newLen, toSplice );
		}
		
		var runningX:Float = 0 + x;
		var runningWidth:Float = 0;
		
		var order:String = "";
		
		// Space tiles appropriately
		for ( i in 0...tiles.length )
		{
			var character:String = text.charAt( i );
			var tile:GameTile = tiles[ i ];
			var tileWidth:Float = fontMap.getBitmapData( character ).scaledWidth;
			
			tile.alpha = this.alpha;
			
			if ( fixedWidth && Std.parseInt( character ) != null )
			{
				tileWidth = fixedNumberWidth;
			}
			
			runningWidth += tileWidth;
			
			tile.x = runningX + ( tileWidth / 2 ); // we want these to be postioned based on left, not center, so, offset by half width
			tile.y = y;
			
			runningX += tileWidth + spacing;
			
			order += Std.string( tile.id ) + "_";
		}
		
		_width = runningWidth + ( tiles.length * spacing ) - spacing;
		
		isDirty = false;
	}
	
	public function getWidth():Float
	{
		if ( isDirty )
		{
			redraw();
		}
		
		return _width;
	}
	
	public function getHeight():Float
	{
		return fontMap.getBitmapData( "0" ).scaledHeight;
	}
	
	public function destroy():Void
	{
		for ( tile in tiles )
		{
			SceneManager.PTR.removeTile( tile );
		}
	}
	
	private function setX( value:Float ):Float
	{
		isDirty = true;
		
		x = value;
		
		return x;
	}
	
	private function setY( value:Float ):Float
	{
		isDirty = true;
		
		y = value;
		
		return y;
	}
	
	private function setAlpha( value:Float ):Float
	{
		for ( tile in tiles )
		{
			tile.alpha = value;
		}
		
		alpha = value;
		
		return alpha;
	}
}