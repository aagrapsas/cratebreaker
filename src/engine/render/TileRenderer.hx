package engine.render;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import engine.render.tilestorage.ITileStorage;
import engine.scene.SceneManager;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Graphics;
import nme.display.Tilesheet;
import nme.geom.ColorTransform;
import nme.geom.Matrix;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
 * ...
 * @author A.A. Grapsas
 */

class TileRenderer 
{
	private var count:Float = -1;
	private var lifeTimeAdded:Int = 0;
	private var lifeTimeRemoved:Int = 0;
	
	private var tilesheet:Tilesheet;
	
	private var tileStorage:ITileStorage;
	
	public var id:String = null;

	public function new( image:BitmapData, tileStorage:ITileStorage ) 
	{
		tilesheet = new Tilesheet( image );
		
		this.tileStorage = tileStorage;
	}
	
	public function addRenderable( rectangle:Rectangle ):Float
	{
		tilesheet.addTileRect( rectangle, new Point( rectangle.width / 2, rectangle.height / 2 ) );
		
		count++;
		
		return count;
	}
	
	public function getNewTile( id:Float ):GameTile
	{
		var tile:GameTile = new GameTile( id );
		
		tile.layer = this.id;
		
		return tile;
	}
	
	public function removeTile( tile:GameTile ):Void
	{		
		tileStorage.removeTile( tile );
		
		tile.breakDown();
		
		lifeTimeRemoved++;
		
		Debug.stats.setStat( "Lifetime removed", cast lifeTimeRemoved );
	}
	
	public function addTile( tile:GameTile ):Void
	{
		tileStorage.addTile( tile );
		
		lifeTimeAdded++;
		
		Debug.stats.setStat( "Lifetime added", cast lifeTimeAdded );
	}
	
	public function render( graphics:Graphics ):Void
	{
		if ( tileStorage == null )
		{
			return;
		}
		
		tileStorage.tick( 0 );
		
		#if false
		// Blit to SceneManager
		var buffer:Bitmap = SceneManager.PTR.renderBuffer;
		
		var matrix:Matrix = new Matrix();
		var colorTransform:ColorTransform = new ColorTransform();
		
		var activeTiles:Array<GameTile> = tileStorage.getTiles();
		
		Debug.stats.setStat( "Tiles", cast activeTiles.length );
		
		for ( tile in activeTiles )
		{
			if ( tile.alpha == 0 || tile.bitmap == null )
			{
				continue;
			}
			
			colorTransform.alphaMultiplier = tile.alpha;
			
			matrix = tile.bitmap.transform.matrix.clone();
			matrix.scale( tile.scale, tile.scale );
			matrix.translate( ( -tile.bitmap.width / 2 ) * tile.scale, ( -tile.bitmap.height / 2 ) * tile.scale );
			matrix.rotate( -tile.rotation ); // Tile Renderer & Blitter use different directions as CW
			matrix.translate( tile.x, tile.y );
			
			tile.bitmap.alpha = tile.alpha;
			
			buffer.bitmapData.draw( tile.bitmap, matrix, tile.bitmap.transform.colorTransform, null, null, true );
		}
		
		#else
		var tileLength:Int = tileStorage.getRenderIndecies().length;
		
		Debug.stats.setStat( "Float count", cast tileLength );
		
		// Draw tiles
		tilesheet.drawTiles( graphics, tileStorage.getRenderIndecies(), false, Tilesheet.TILE_SCALE | Tilesheet.TILE_ROTATION | Tilesheet.TILE_ALPHA );
		#end
	}
}