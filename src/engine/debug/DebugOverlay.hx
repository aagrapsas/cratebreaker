package engine.debug;
import nme.Assets;
import nme.display.Sprite;
import nme.Lib;
import nme.text.Font;
import nme.text.TextField;
import nme.text.TextFormat;

/**
 * ...
 * @author A.A. Grapsas
 */

class DebugOverlay extends Sprite
{
	private var textField:TextField;
	
	public function new() 
	{
		super();
		
		setup();
	}
	
	private function setup():Void
	{
		Lib.current.stage.addChild( this );
		
		textField = new TextField();
		textField.cacheAsBitmap = true;
		
		var font:Font = Assets.getFont( "assets/fonts/anaheim.ttf" );
		var format:TextFormat = new TextFormat( font.fontName, 12, 0xFFFFFF );
		textField.defaultTextFormat = format;
		textField.selectable = false;
		textField.text = "";
		textField.multiline = true;
		textField.wordWrap = true;
		textField.mouseEnabled = false;
		
		this.addChild( textField );
		
		textField.width = Lib.current.stage.stageWidth;
		textField.height = Lib.current.stage.stageHeight / 2;
		
		this.mouseChildren = false;
		this.mouseEnabled = false;
	}
	
	public function log( text:String ):Void
	{
		textField.appendText( "\n" + text );
		textField.scrollV = textField.maxScrollV;
	}
	
	public function hide():Void
	{
		textField.visible = false;
	}
	
	public function show():Void
	{
		textField.visible = true;
	}
}