package engine.debug;
import nme.display.FPS;
import nme.Lib;

/**
 * ...
 * @author A.A. Grapsas
 */

class Debug 
{
	public static var display:DebugOverlay;
	public static var stats:DebugStats;
	public static var fps:FPS;
	
	public static function initialize():Void
	{
		var shouldDebugAll:Bool = false;
		
		#if flash
			shouldDebugAll = true;
		#end
		
		if ( shouldDebugAll )
			display = new DebugOverlay();
		
		stats = new DebugStats();
		
		if ( !shouldDebugAll )
			stats.hide();
		
		if ( shouldDebugAll )
		{
			fps = new FPS();
			
			fps.textColor = 0xFFFFFF;
			
			Lib.current.stage.addChild( fps );
		}
	}
	
	public static function log( channel:String, message:String ):Void
	{
		var output:String = "(" + Lib.getTimer() / 1000 + ") " + channel + ": " + message;
		
		trace( output );
		
		if ( display != null )
		{
			display.log( output );
		}
	}
}