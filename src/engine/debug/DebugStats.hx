package engine.debug;
import engine.misc.ITickable;
import nme.Assets;
import nme.display.Sprite;
import nme.Lib;
import nme.text.Font;
import nme.text.TextField;
import nme.text.TextFormat;

/**
 * ...
 * @author A.A. Grapsas
 */
class DebugStats
{
	private var display:Sprite;
	
	private var textField:TextField;
	
	private var stats:Hash<String>;

	public function new() 
	{
		setup();
	}
	
	private function setup():Void
	{
		stats = new Hash<String>();
		
		display = new Sprite();
		
		Lib.current.stage.addChild( display );
		
		textField = new TextField();
		textField.cacheAsBitmap = true;
		
		var font:Font = Assets.getFont( "assets/fonts/anaheim.ttf" );
		var format:TextFormat = new TextFormat( font.fontName, 20, 0xFFFFFF );
		textField.defaultTextFormat = format;
		textField.selectable = false;
		textField.text = "";
		textField.multiline = true;
		textField.wordWrap = true;
		textField.mouseEnabled = false;
		
		display.addChild( textField );
		
		textField.width = Lib.current.stage.stageWidth;
		textField.height = Lib.current.stage.stageHeight / 2;
		
		display.mouseChildren = false;
		display.mouseEnabled = false;
		
		display.y = Lib.current.stage.stageHeight / 2;
		//display.x = Lib.current.stage.stageWidth / 2;
	}
	
	public function setStat( key:String, value:String ):Void
	{
		// Prevent setting dirty flag for same value
		if ( stats.exists( key ) && stats.get( key ) == value )
		{
			return;
		}
		
		stats.set( key, value );
		
		textField.text = "";
		
		for ( key in stats.keys() )
		{
			textField.appendText( key + ": " + stats.get( key ) + "\n" );
		}
		
		textField.cacheAsBitmap = true;
	}
	
	public function hide():Void
	{
		display.visible = false;
	}
	
	public function show():Void
	{
		display.visible = true;
	}
}