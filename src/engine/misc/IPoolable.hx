package engine.misc;

/**
 * ...
 * @author A.A. Grapsas
 */

interface IPoolable 
{
	function resurrect():Void;
	function suspend():Void;
	function getPool():GenericPool;
	function setPool( value:GenericPool ):Void;
}