package engine.misc;
import engine.assets.GameBitmap;
import engine.render.fonts.FontMap;
import engine.render.ResolutionConfig;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Sprite;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
 * ...
 * @author A.A. Grapsas
 */
class BlitText extends Sprite, implements ITickable
{
	public var fixedWidth( default, setFixedWidth ):Bool = true; //Use fixed width for numbers?
	
	private var bitmap:Bitmap;
	
	private var fontMap:FontMap;
	private var spacing:Float = 0;
	private var text:String;
	
	private var isDirty:Bool = false;
	
	private var _width:Float;
	
	public function new() 
	{
		super();
		
		bitmap = new Bitmap( new BitmapData( 1, 1 ) );
		
		this.addChild( bitmap );
	}
	
	public function setFont( font:FontMap ):Void
	{
		this.fontMap = font;
		
		isDirty = true;
	}
	
	public function setSpacing( space:Float ):Void
	{
		this.spacing = ( space / ResolutionConfig.PTR.nonuniformScaleX );
		
		isDirty = true;
	}
	
	public function setText( text:String ):Void
	{
		this.text = text;
		
		isDirty = true;
	}
	
	public function setFixedWidth( useFixedWidth:Bool ):Bool
	{
		this.fixedWidth = useFixedWidth;
		
		isDirty = true;
		
		return this.fixedWidth;
	}
	
	public function tick( deltaTime:Float ):Void
	{
		if ( isDirty )
		{
			redraw();
		}
	}
	
	private function redraw():Void
	{
		if ( text == null || text == "" )
		{
			return;
		}
		
		bitmap.bitmapData.lock();
		
		var runningWidth:Float = 0;
		
		var fixedNumberWidth:Float = 0;
		if ( fixedWidth )
		{
			fixedNumberWidth = fontMap.getBitmapData( "0" ).scaledWidth;
		}
		
		for ( j in 0...text.length )
		{
			var tempChar:String = text.charAt( j );
			var charBmp:GameBitmap = fontMap.getBitmapData( tempChar );
			if ( charBmp == null )
			{
				//Error, don't have the given font character. Something about skipping it?
				tempChar = "0";
				charBmp = fontMap.getBitmapData( tempChar );
			}
			var tileWidth:Float = charBmp.bitmapData.width;
			if ( fixedWidth && Std.parseInt( tempChar ) != null )
			{
				tileWidth = fixedNumberWidth;
			}
			runningWidth += tileWidth;
			runningWidth += spacing;
		}
		
		var rect:Rectangle = new Rectangle( 0, 0, runningWidth, getHeight() );
		
		if ( bitmap.bitmapData.width < rect.width )
		{
			bitmap.bitmapData = new BitmapData( cast rect.width, cast rect.height );
		}
		
		bitmap.bitmapData.fillRect( rect, 0 );
		
		var point:Point = new Point();
		
		var runningX:Float = 0;
		
		
		for ( i in 0...text.length )
		{
			var char:String = text.charAt( i );
			
			// Pure blit (may be slow on mobile? who knows?)
			var charBmp:GameBitmap = fontMap.getBitmapData( char );
			if ( charBmp == null )
			{
				char = "0";
				charBmp = fontMap.getBitmapData( char );
			}
			
			var data:BitmapData = charBmp.bitmapData;
			
			var charWidth:Float = data.width;
			var fixedWidthOffset:Float = 0;
			if ( fixedWidth && Std.parseInt( char ) != null )
			{
				fixedWidthOffset = (fixedNumberWidth - charWidth) / 2;
				charWidth = fixedNumberWidth;
			}
			
			point.x = runningX + fixedWidthOffset;
			point.y = 0;
			
			runningX += charWidth + spacing;
			
			bitmap.bitmapData.copyPixels( data, data.rect, point );
		}
		
		bitmap.bitmapData.unlock();
		
		_width = runningWidth;
		
		isDirty = false;
	}
	
	public function getWidth():Float
	{
		if ( isDirty )
		{
			redraw();
		}
		
		return _width;
	}
	
	public function getHeight():Float
	{
		return fontMap.getBitmapData( "0" ).scaledHeight;
	}
}