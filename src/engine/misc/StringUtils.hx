package engine.misc;

/**
 * ...
 * @author A.A. Grapsas
 */
class StringUtils
{
	public static function getPadded( value:Int, padding:Int ):String
	{
		var converted:String = Std.string( value );
		
		var len:Int = converted.length;
		
		if ( len >= padding )
		{
				return converted;
		}
		
		var difference:Int = padding - len;
		
		for ( i in 0...difference )
		{
				converted = "0" + converted;
		}
		
		return converted;
	}
}