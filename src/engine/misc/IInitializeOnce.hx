package engine.misc;

/**
 * ...
 * @author A.A. Grapsas
 */

interface IInitializeOnce 
{
	function initialize():Void;
}