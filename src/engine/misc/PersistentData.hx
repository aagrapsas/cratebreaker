package engine.misc;
import engine.debug.Debug;
import engine.debug.DebugChannels;
import nme.net.SharedObject;

/**
 * ...
 * @author A.A. Grapsas
 */

class PersistentData implements IInitializeOnce
{
	public static var PTR:PersistentData;
	
	private var target:String;
	private var sharedObject:SharedObject;
	
	public function new( target:String ) 
	{
		PTR = this;
		
		this.target = target;
		setup();
	}
	
	public function initialize():Void
	{
		
	}
	
	private function setup():Void
	{
		sharedObject = SharedObject.getLocal( target );
	}
	
	public function has( key:String ):Bool
	{
		return Reflect.hasField( sharedObject.data, key );
	}
	
	public function get( key:String ):Dynamic
	{
		return Reflect.field( sharedObject.data, key );
	}
	
	public function set( key:String, value:Dynamic ):Dynamic
	{
		Reflect.setField( sharedObject.data, key, value );
		
		try
		{
			sharedObject.flush();
		}
		catch ( e:Dynamic )
		{
			Debug.log( DebugChannels.ERROR, "Unable to save: " + e );
		}
	}
}