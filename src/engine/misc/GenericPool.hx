package engine.misc;
import engine.debug.Debug;

/**
 * ...
 * @author A.A. Grapsas
 */

class GenericPool
{
	public var name( default, null ):String;
	
	private var _toFree:Array<IPoolable>;
	
	private var _free : Array<IPoolable>;
	private var _inUse : Array<IPoolable>;
	private var _creationFunction : Void -> IPoolable;
	private var _max : Int = 0;
	
	public function new( name:String, creationFunction:Void -> IPoolable, max:Int, startCount:Int = 0 ) 
	{
		this.name = name;
		
		_creationFunction = creationFunction;
		_max = max;
		
		_free = new Array();
		_toFree = new Array();
		_inUse = new Array();
		
		if ( startCount > 0 )
		{
			for ( i in 0...startCount )
			{
				var poolable:IPoolable = creationFunction();
				
				poolable.setPool( this );
				
				_free.push( poolable );
			}
		}
	}
	
	public function pop():IPoolable
	{
		var instance:IPoolable = null;
		
		if ( _free.length == 0 )
		{
			// Create new instance
			instance = _creationFunction();
			
			instance.setPool( this );
		}
		else
		{
			instance = _free.pop();
		}
		
		instance.resurrect();
		
		_inUse.push( instance );
		
		if ( _inUse.length > _max )
		{
			// WARN
		}
		
		return instance;
	}
	
	public function free( instance:IPoolable ):Void
	{
		instance.suspend();
		
		// Move to a queue that waits for the end of the frame (after actors
		// have been released by the SceneManager
		_toFree.push( instance );
		
		_inUse.remove( instance );
	}
	
	public function handleEndOfUpdate():Void
	{
		if ( _toFree.length == 0 )
		{
			return;
		}
		
		// Move from to-Free to the actual free
		for ( instance in _toFree )
		{
			_free.push( instance );
		}
		
		_toFree.splice( 0, _toFree.length );
		
		Debug.stats.setStat( name, _inUse.length + "/" + _free.length + "/" + ( _inUse.length + _free.length ) );
	}
	
	public function getLiveCount():Int { return _inUse.length; }
}