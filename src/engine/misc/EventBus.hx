package engine.misc;

/**
 * ...
 * @author A.A. Grapsas
 */

class EventBus 
{
	private var eventMap:Hash < Array < Dynamic -> Void >> ;
	
	private var isLocked:Bool = false;
	private var isDispLocked:Bool = false;
	
	private var toRemove:Array<Dynamic>;
	private var toAdd:Array<Dynamic>;
	private var toDispatch:Array<Dynamic>;
	
	public function new() 
	{
		toRemove = new Array<Dynamic>();
		toAdd = new Array<Dynamic>();
		toDispatch = new Array<Dynamic>();
		eventMap = new Hash<Array<Dynamic -> Void>>();
	}
	
	public function register( event:String, func:Dynamic -> Void )
	{
		if ( isLocked )
		{
			toAdd.push( { "event" : event, "func" : func } );
			return;
		}
		
		if ( eventMap.exists( event ) == false )
		{
			eventMap.set( event, new Array<Dynamic -> Void> () );
		}
		
		eventMap.get( event ).push( func );
	}
	
	public function unregister( event:String, func:Dynamic -> Void )
	{
		if ( isLocked )
		{
			toRemove.push( { "event" : event, "func" : func } );
			return;
		}
		
		var callbacks:Array<Dynamic -> Void> = eventMap.get( event );

		if ( callbacks == null || callbacks.length == 0 )
		{
			return;
		}
		
		callbacks.remove( func );
	}
	
	public function dispatch( event:String, data:Dynamic ):Void
	{
		if ( isLocked )
		{
			toDispatch.push( { "event" : event, "data" : data } );
			return;
		}
		
		var callbacks:Array<Dynamic -> Void> = eventMap.get( event );

		if ( callbacks == null || callbacks.length == 0 )
		{
			return;
		}
		
		// Lock, we do not want to change the callbacks array while iterating it
		isLocked = true;
		for ( func in callbacks )
		{
			func( data );
		}
		isLocked = false;
		
		// Add anything that we locked out
		if ( toAdd.length > 0 )
		{
			for ( add in toAdd )
			{
				register( add.event, add.func );
			}
			
			toAdd.splice( 0, toAdd.length );
		}
		
		// Remove anything that we locked out
		if ( toRemove.length > 0 )
		{
			for ( rem in toRemove )
			{
				unregister( rem.event, rem.func );
			}
			
			toRemove.splice( 0, toRemove.length );
		}
		
		// Dispatch any remainders
		if ( isDispLocked == false )
		{
			isDispLocked = true;
			
			if ( toDispatch.length > 0 )
			{
				for ( disp in toDispatch )
				{
					dispatch( disp.event, disp.data );
				}
				
				toDispatch.splice( 0, toDispatch.length );
			}
			
			isDispLocked = false;
		}
	}
}