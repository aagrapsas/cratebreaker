package engine.misc;

/**
 * ...
 * @author A.A. Grapsas
 */

interface ITouchable 
{
	function handleTouch():Void;
	function handleTap():Void;
}