package engine.misc;

/**
 * ...
 * @author A.A. Grapsas
 */

class LerpTypes 
{
	public static var LINEAR:Int = 0;
	public static var EASE_IN:Int = 1;
	public static var EASE_OUT:Int = 2;
	public static var EASE_IN_OUT:Int = 3;
	
	public static function getLerpValue( type:Int, start:Float, end:Float, alpha:Float ):Float
	{
		switch ( type )
		{
			case EASE_IN:
				return MathUtility.easeIn( start, end, alpha );
			case EASE_OUT:
				return MathUtility.easeOut( start, end, alpha );
			case EASE_IN_OUT:
				return MathUtility.lerp( start, end, MathUtility.getEaseInOutAlpha( alpha ) );
		}
		
		return MathUtility.lerp( start, end, alpha );
	}
}