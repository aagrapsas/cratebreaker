package engine.misc;
import engine.collision.CollisionInfo;
import nme.geom.Point;

/**
 * ...
 * @author A.A. Grapsas
 */

class MathUtility 
{
	public static function getAngle( x1:Float, y1:Float, x2:Float, y2:Float ):Float
	{
		var first:Float = Math.atan2( x1, y1 );
		var second:Float = Math.atan2( x2, y2 );
		var angle:Float = first - second;
		var angle2:Float = 0;
		
		if ( angle > Math.PI )
		{
			angle2 = angle - Math.PI * 2;
		}
		else if ( angle < Math.PI )
		{
			angle2 = angle + Math.PI * 2;
		}
		
		if ( Math.abs( angle ) > Math.abs( angle2 ) )
		{
			angle = angle2;
		}
		
		angle *= 180 / Math.PI;
		
		return angle;
	}
	
	public static function lerp( start:Float, end:Float, alpha:Float ):Float
	{
		return start + alpha * ( end - start );
	}
	
	public static function easeIn( start:Float, end:Float, alpha:Float ):Float
	{
		return lerp( start, end, getEaseInAlpha( alpha ) );
	}
	
	public static function easeOut( start:Float, end:Float, alpha:Float ):Float
	{
		return lerp( start, end, getEaseOutAlpha( alpha ) );
	}
	
	public static function getEaseInOutAlpha( alpha:Float ):Float
	{
		return ( 1 - Math.sin( Math.PI / 2 + alpha * Math.PI ) ) / 2;
	}
	
	public static function getEaseInAlpha( alpha:Float ):Float
	{
		return Math.sin( Math.PI * alpha / 2 );
	}
	
	public static function getEaseOutAlpha( alpha:Float ):Float
	{
		return 1 - Math.cos( Math.PI * alpha / 2 );
	}
	
	 public static function getLineIntersection( s1X:Float, s1Y:Float, e1X:Float, e1Y:Float, s2X:Float, s2Y:Float, e2X:Float, e2Y:Float ):CollisionInfo
	{
		var dSE2X:Float = e2X - s2X;
		var dSE2Y:Float = e2Y - s2Y;
		var dSE1X:Float = e1X - s1X;
		var dSE1Y:Float = e1Y - s1Y;
		var dSSX:Float = s1Y - s2Y;
		var dSSY:Float = s1X - s2X;
	
		var D:Float = dSE2Y * dSE1X - dSE2X * dSE1Y;
		var X:Float = ( dSE2X * dSSX - dSE2Y * dSSY ) / D;
		var Y:Float = ( dSE1X * dSSX - dSE1Y * dSSY ) / D;
	
		if ( X >= 0 && X <= 1 && Y >= 0 && Y <= 1 )
		{
			var intX:Float = s1X + X * dSE1X;
			var intY:Float = s1Y + X * dSE1Y;
			
			return new CollisionInfo( intX, intY, true );
		}
				
		return new CollisionInfo( 0, 0, false );
	}
	
	 /**
	 * Returns a clock-wise perpendicular with passed in values
	 * @param       x
	 * @param       y
	 */
	public static function getPerpendicular( x:Float, y:Float ):Point
	{
		return new Point( y, -x );
	}
	
	public static function getRandomIntInRange( low:Int, high:Int ):Int
	{
		return Math.floor( Math.random() * ( high - low + 1 ) ) + low;
	}
	
	public static function getRandomFloatInRange( low:Float, high:Float ):Float
	{
		return Math.random() * ( high - low + 1 ) + low;
	}
}