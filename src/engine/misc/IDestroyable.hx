package engine.misc;

/**
 * ...
 * @author A.A. Grapsas
 */

interface IDestroyable 
{
	function destroy():Void;
}