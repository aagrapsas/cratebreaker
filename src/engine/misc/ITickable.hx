package engine.misc;

/**
 * ...
 * @author A.A. Grapsas
 */

interface ITickable 
{
	function tick( deltaTime:Float ):Void;
}