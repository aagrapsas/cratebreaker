package engine.misc;
import engine.render.ResolutionConfig;
import nme.Assets;
import nme.text.Font;
import nme.text.TextField;
import nme.text.TextFormat;
import nme.text.TextFormatAlign;

/**
 * ...
 * @author A.A. Grapsas
 */

class TextFieldHelper 
{
	public static function getTF( text:String, font:String, size:Int, color:Int, isBold:Bool = false ):TextField
	{
		var field:TextField = new TextField();
		var font:Font = Assets.getFont( "assets/fonts/" + font );
		var format:TextFormat = new TextFormat( font.fontName, size / ResolutionConfig.PTR.uniformScaleFactor, color, isBold );
		format.align = TextFormatAlign.LEFT;
		field.defaultTextFormat = format;
		field.selectable = false;
		field.mouseEnabled = false;
		field.text = text;
		field.width = field.textWidth + 5 / ResolutionConfig.PTR.nonuniformScaleX;
		field.height = field.textHeight;
		field.cacheAsBitmap = true;
		
		return field;
	}
}