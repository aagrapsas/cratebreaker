package engine.ui;
import engine.misc.ITickable;
import nme.display.Sprite;
import nme.events.Event;

/**
 * ...
 * @author A.A. Grapsas
 */

class UIElement extends Sprite, implements ITickable
{	
	public var layerName:String = "default";
	
	public function new() 
	{
		super();
		
		this.addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
	}
	
	private function onAddedToStage( e:Event ):Void
	{
		this.removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		
		setup();
	}
	
	public function setup():Void
	{
		
	}
	
	public function tick( deltaTime:Float ):Void
	{
		
	}
	
	public function destroy():Void
	{
		
	}
}