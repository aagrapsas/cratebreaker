package engine.ui;
import engine.misc.ITickable;
import nme.display.Sprite;

/**
 * ...
 * @author A.A. Grapsas
 */

class UIManager implements ITickable
{
	public var mainDisplay:Sprite;
	
	public static var PTR:UIManager;
	
	private var elements:Array<UIElement>;
	
	private var layers:Hash<Sprite>;
	
	public static var LAYER_DEFAULT:String = "default";
	
	private var elementsToAdd:Array<UIElement>;
	private var elementsToRemove:Array<UIElement>;
	
	public function new() 
	{
		PTR = this;
		
		mainDisplay = new Sprite();
		
		elements = new Array<UIElement>();
		
		layers = new Hash<Sprite>();
		elementsToAdd = new Array<UIElement>();
		elementsToRemove = new Array<UIElement>();
		
		setupLayer( LAYER_DEFAULT );
	}
	
	private function setupLayer( layerName:String ):Void
	{
		var layer:Sprite = new Sprite();
		
		layers.set( layerName, layer );
		
		mainDisplay.addChild( layer );
	}
	
	public function addElement( element:UIElement ):Void
	{
		var layer:Sprite = layers.get( element.layerName );
		
		layer.addChild( element );
		
		elementsToAdd.push( element );
	}
	
	public function removeElement( element:UIElement ):Void
	{
		var layer:Sprite = layers.get( element.layerName );
		
		layer.removeChild( element );
		
		elementsToRemove.remove( element );
	}
	
	public function clearElements():Void
	{
		for ( element in elements )
		{
			removeElement( element );
		}
	}
	
	public function tick( deltaTime:Float ):Void
	{
		for ( element in elements )
		{
			element.tick( deltaTime );
		}
		
		for ( elmToAdd in elementsToAdd )
		{
			elements.push( elmToAdd );
		}
		
		if ( elementsToAdd.length > 0 )
		{
			elementsToAdd.splice( 0, elementsToAdd.length );
		}
		
		for ( elmToRem in elementsToRemove )
		{
			elements.remove( elmToRem );
		}
		
		if ( elementsToRemove.length > 0 )
		{
			elementsToRemove.splice( 0, elementsToRemove.length );
		}
	}
}